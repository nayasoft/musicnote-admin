var userName;
var emailId ;
var noteName ;
var date ;
var Description;
var reportLevel ;
var listId ;
var userId;
var noteId;
var compliantId;
var commentId;
var createListForVote;
var fileNameAttach='';
var exactComment='';
var listType='';
var userInitial ='';
var tagFilter='';
var listOwnerFlagForAddBook =false;
var publicNoteCopyFlag =false;
var userDetails=[];
var activeUserDeatilsMap={};
var friendsPhotoMap={};
var activeUserObjectMap={};
var listAccessMap={};
var crowdTagMap={};
var selectedFiles='';
var allNoteCreateBasedOn;
var userNameForDiv ;
var userInitialForDiv;
var dueDateData='';
var role="";
var userDetailsJsonObj;
var tagFilter=new Array();
var month=new Array();
var today = new Date();
var attacmentUserId;

var yesterday=new Date();
yesterday.setDate(yesterday.getDate()-1);

var tomor=new Date();
tomor.setDate(tomor.getDate()+1);
month[0]="Jan";
month[1]="Feb";
month[2]="Mar";
month[3]="Apr";
month[4]="May";
month[5]="Jun";
month[6]="Jul";
month[7]="Aug";
month[8]="Sep";
month[9]="Oct";
month[10]="Nov";
month[11]="Dec";

var monthMap = {};
monthMap["Jan"] = "1";
monthMap["Feb"] = "2";
monthMap["Mar"] = "3";
monthMap["Apr"] = "4";
monthMap["May"] = "5";
monthMap["Jun"] = "6";
monthMap["Jul"] = "7";
monthMap["Aug"] = "8";
monthMap["Sep"] = "9";
monthMap["Oct"] = "10";
monthMap["Nov"] = "11";
monthMap["Dec"] = "12";
var cHour="";
var cMinutes="";


var userFulName ='';
var  noteNameFromList;
var listIdFromList;
var noteIdFromList;
var listOwnerFlag = false;
var sharedNoteFlag = false;
var sharedHideDelete = false;
var listOwnerFullName ;
var listOwnerFirstName;
var parentDivName='';
var vote =new Array();
var actionFrom='';
var noteDeleteAccess='';

$(document).ready(function(){
	$('#userComplaintNote').on('click','#complaintDiv', function(event){
		$('.close').attr('style','display:block;');
		createMoreActionModal(userId,commentId,reportLevel);
	});
	$('#userComplaintNote').on('mouseover','.noteDiv',function(){
		$('.noteDiv').attr('style','background-color: rgb(164, 164, 164);height:55px;');
		
	});
	$('#userComplaintNote').on('mouseout','.noteDiv',function(){
		$('.noteDiv').attr('style','background-color: rgb(243, 243, 243);');
	});
	$('#noteComplaintNote').on('click','#complaintDiv', function(event){
		createMoreActionModal(userId,commentId,reportLevel);
	});
	$('#noteComplaintNote').on('mouseover','.noteDiv',function(){
		$('.noteDiv').attr('style','background-color: rgb(164, 164, 164);height:55px;');
		
	});
	$('#noteComplaintNote').on('mouseout','.noteDiv',function(){
		$('.noteDiv').attr('style','background-color: rgb(243, 243, 243);');
	});
	
	$('#moreActions').on('click','.js-delete-cmt',function(){
		cIdFromDiv= $(this).parent().attr('id');
		cmtsclickId=$(this);
		$('#moreActions').modal('hide');
		$('#adminCommentDialog').attr('style','display:none');
		$('#adminComentComment').val("");
		$('#commentsDeletModal').modal("show");
		
	});
	
	$('#moreActions').on('click','.file-download',function(){
		var tempClass = $(this).attr('class');
		var array = tempClass.split("~");
		var temp=$(this).attr('style');
		
		getFileFor($(this).attr('id'),array[1],temp);
	});
	$('#moreActions').on('click','.playRecordedMoreActions',function(){
		var tempClass = $(this).attr('id');
		var array = tempClass.split("~");
		var lessonName =array[0];
		var fileOwnerId=array[1];
		var splitExtension=lessonName.split(".");
		var extensions=splitExtension[1];
		if(extensions=='mov' || extensions=='mp4' || extensions=='avi' || extensions=='wmv' || extensions=='wma'){
			$('#moreActions').children('.modal-body').children('#content').children('.clearfix').children('.list-actions1').children('.more-player').remove();
			$(this).parent().after('<div class="more-player"><li><embed width="260" height="282" src="'+uploadUrl+lessonName+'" showstatusbar="true" showgotobar="true" showdisplay="true" pluginspage="http://www.microsoft.com/Windows/MediaPlayer/" type="application/x-mplayer2" name="rTuner" AUTOSTART="false"/></li></div>');
			//$(this).parent().after('<div class="more-player"><li><video id="adminUploadedVideo" width="530" height="240" controls style="margin-left:40px;"><source src='+uploadUrl+lessonName+' type="video/mp4"></video>');
		}else if(extensions=='mp3' || extensions=='wav' || extensions=='amr')
		{
			$('#moreActions').children('.modal-body').children('#content').children('.clearfix').children('.list-actions1').children('.more-player').remove();
				
			$(this).parent().after('<div class="more-player"><li><embed  height="50" width="180" name="plugin" src="'+uploadUrl+lessonName+'" type="audio/x-wav" controls AUTOSTART="false"/> </li></div>');
		}
	


	});

	// open pdf for chorme browser only
	$('#moreActions').on('click','.openPdf',function()
		{
		var tempClass = $(this).attr('id');
		var array = tempClass.split("~");
		var lessonName =array[0];
		var fileOwnerId=array[1];
		
		$('#moreActions').children('.modal-body').children('#content').children('.clearfix').children('.list-actions1').children('.more-player').remove();
		/*$(this).parent().after('<div class="more-player"><li><iframe src="'+uploadUrl+lessonName+'" style="width: 10px; height: 10px;" type="application/x-google-chrome-print-preview-pdf" frameborder="0" scrolling="no" name="extensions" extensionspage="https://chrome.google.com/webstore/detail/oemmndcbldboiebfnladdacbdfmadadm/">'
			+' </iframe><p>To view PDFs please ensure your browser has a PDF viewer (PDF.js) installed.</p></li></div>');
		*/
		
		if(is_chrome){
			$(this).parent().after('<div class="more-player"><li><iframe src="'+uploadUrl+lessonName+'" style="width: 100%; height: 200px;" frameborder="0" scrolling="no">'
		    +'<p>It appears your web browser doesn\'t support iframes.</p>'
		    +' <p>It appears because you don\'t have pdf viewer or PDF support in this web browser.Please add (pdf.js) pdf Viewer to your browser </p> </iframe>');	
		}else{
			$(this).parent().after('<div class="more-player"><li><object data="'+uploadUrl+lessonName+'" type="application/pdf" width="100%" height="200px">'
		   +'<embed src="'+uploadUrl+lessonName+'" type="application/pdf" width="100%" height="200px" /><p>To view PDFs please ensure your browser has a PDF viewer (PDF.js) installed.</p>'
			+'</object>');
		}
		
		
	});
$('#moreActions').on('click','.ext',function(){
		
	 	var fileName = $(this).parent().attr('id');
	 	var fileName1=fileName.substring(fileName.lastIndexOf('.')+1);
	 	var fileName2=fileName1.split('~');
	 	var finalExtName=fileName2[0];
	 	
	 	var array = fileName.split("~");
		var lessonName =array[0];
		var fileOwnerId=array[1];
	 	
	 	if(finalExtName=='jpg' || finalExtName=='jpeg' || finalExtName=='gif' || finalExtName=='png')
	 	{
	 		$('#moreActions').children('.modal-body').children('#content').children('.clearfix').children('.list-actions1').children('.more-player').remove();
			$(this).parent().parent().after('<div class="more-player"><li><img height="400" src="'+uploadUrl+lessonName+'" style="border-right: 1px solid; border-top: 1px solid; border-left: 1px solid; border-bottom: 1px solid" width="400" id="'+lessonName+'" alt="'+lessonName+'" />');
	 	}
	});
	//********* dialog message for delet comment *********//
	$('.js-admin-action-comment').on('click',function()
	{
		var adminComment=$("#adminComentComment").val().trim();
		adminComment = adminComment.replace( /[\s\n\r]+/g, ' ' );
		 while(adminComment.indexOf("\"") != -1){
			 adminComment = adminComment.replace("\"", "`*`");
			}
		var action=$(this).val();
		$('#adminCommentDialog').empty();
		if(action=='Cancel')
		{
			$('#moreActions').modal('toggle');
			$('#commentsDeletModal').modal("toggle");
		}
		else if(adminComment!="")
		{
			var url = urlForServer+"admin/actionCompliantComments";
			var datastr = '{"listId":"'+listId+'","cIdFromDiv":"'+cIdFromDiv+'","noteId":"'+noteId+'","adminComment":"'+escape(adminComment)+'","action":"'+action+'","compliantId":"'+compliantId+'"}';
			$.ajax({
				headers: { "Mn-Callers" : musicnote,
			    	"Mn-time" :musicnoteIn	
			    	},
				type: 'POST',
				url : url,
				data:datastr,
				success : function(response){
					
					if(action=="Delete")
						{
				
				var count =$(cmtsclickId).parent().parent().parent().parent().parent().parent().children('.badges').children('.cmts').find('.badge-text').text();
				$(cmtsclickId).parent().parent().parent().parent().parent().parent().children('.badges').children('.cmts').find('.badge-text').remove();
				$(cmtsclickId).parent().parent().parent().parent().parent().parent().children('.badges').children('.cmts').append('<span class="badge-text">&nbsp;&nbsp;'+(count-1)+'</span>');
				$(cmtsclickId).parent().parent().parent().parent().parent().parent().children('.badges').children('.cmts').attr("title",'This card has '+(count-1)+' comments(s).');
				// show in note
				var listId;
				if(listType!='crowd'){
					listId=$('#notebookId').val();
					if(listId =='note'){
						listId =listIdFromList;
					}
				}else{
					listId =listIdFromList;
				}
				if((count-1) > 0 ){
				$(cmtsclickId).parent().parent().parent().parent().parent().parent().parent().parent().parent().children('#'+listId+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').find('.cmts').find("span").remove();
				$(cmtsclickId).parent().parent().parent().parent().parent().parent().parent().parent().parent().children('#'+listId+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').find('.cmts').removeAttr("title");
				$(cmtsclickId).parent().parent().parent().parent().parent().parent().parent().parent().parent().children('#'+listId+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').find('.cmts').attr("title",'This card has '+(count-1)+' comments(s).');
				$(cmtsclickId).parent().parent().parent().parent().parent().parent().parent().parent().parent().children('#'+listId+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').find('.cmts').append(
							'<span class="icon-comment"></span>'
							+'<span class="badge-text">&nbsp;&nbsp;'+(count-1)+'</span>');     
				}else{
					$(cmtsclickId).parent().parent().parent().parent().parent().parent().parent().parent().parent().children('#'+listId+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').find('.cmts').remove();
				}
					// end show in note
					
				$(cmtsclickId).parent().parent().parent().remove();
						}
				$('#moreActions').modal('toggle');
				$('#commentsDeletModal').modal("toggle");
			},
			error: function(e) {
				alert("Please try again later");
			}
			
		});
		}
		else
			{
			 $("#adminCommentDialog").attr('style','display : block;color:red;margin-top:-4px;');	
		     	$('#adminCommentDialog').empty();
			 $("#adminCommentDialog").text("Field cannot be blank");
			}
	});
	
	$('#complaintReportOk').click(function(){
		$('#viewCompaintModal').modal('hide');
	});
	$('#moreActions').on('click','.js-delete-report-note',function(){
		$('#moreActions').modal('hide');
		$('#adminNoteCommentDialog').attr('style','display : none');
		$('#adminNoteComment').val('');
		actionFrom="Note";
		$('#deleteReportNoteModal').modal('show');
	});
	
	
	$('#moreActions').on('click','.js-resolve-report-note',function(){
		$('#moreActions').modal('hide');
		$('#adminResolveCommentDialog').attr('style','display : none');
		$("#emptyTemplatespan").attr('style','display : none');
		$("#adminResolveOption").empty();
		$("#adminResolveComment").val("");
		$("#adminResolveComment").attr('disabled','disabled');
		$('#reportResolveComment').removeAttr("disabled","disabled");
		$('#reportStatusHold').removeAttr("disabled","disabled");
		$('#reportStatusCancel').removeAttr("disabled","disabled");
		$('#adminResolveComment').removeAttr("disabled","disabled");
		$('#adminResolveOption').removeAttr("disabled","disabled");
		$('#adminResolveModal').modal('show');
		if($('#complaintStatus').val()=="Resolved"||$('#complaintStatus').val()=="Rejected")
		{
			$('#reportResolveComment').attr("disabled","disabled");
			$('#reportStatusHold').attr("disabled","disabled");
			$('#reportStatusCancel').attr("disabled","disabled");
			$('#adminResolveComment').attr("disabled","disabled");
			$('#adminResolveOption').attr("disabled","disabled");
		}
		else if($("#deleteNoteFlag").val()=="True")
		{
			$('#reportStatusHold').attr("disabled","disabled");
			$('#reportStatusCancel').attr("disabled","disabled");
			$("#deleteNoteFlag").val("");
		}
		var url = urlForServer + "admin/fetchMailTemplates";
		var datastr = '{"userId":"'+userId+'"}';
		var params = encodeURIComponent(datastr);
		$.ajax({ headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn	
		},
			
					type : 'POST',
					url : url,
					data : params,
					success : function(responseText) 
					{
						if(responseText!=''){
							data1 = jQuery.parseJSON(responseText);
							
							$("#adminResolveOption").append('<option id="select" value="select">Select</option>');
							var data;
							for(var i=0;i<data1.length;i++){
								if($('#mailTemplateId').val()!=data1[i].templateId)
								{
									data='<option id="'+data1[i].templateId+'" value="'+data1[i].templateText+'">'+data1[i].templateName+'</option>';
								}
								else
								{
									data='<option id="'+data1[i].templateId+'" value="'+$('#adminComment').val()+'">'+data1[i].templateName+'</option>';
								}
								$("#adminResolveOption").append(data);
							}
							var content;
							if($('#mailTemplateId').val()!='-')
							{
								$("#adminResolveOption").val($('#adminComment').val());
								content=$("#adminResolveOption option:selected").val();
								content=content.replace(/\<br>/g, "\n");
							}
							if($('#adminResolveOption').val()!='select')
							{
								$("#adminResolveComment").val(content);
							}
							else
							{
								$("#adminResolveComment").attr('disabled','disabled');
								$("#adminResolveComment").val('');
							}
						}else{
							$("#emptyTemplatespan").attr('style','display:block;font-family: Helvetica Neue; font-size: 14px; color: rgb(255, 0, 0); font-weight: bold;');
						}
					}

		});
	});
	
	
	$('#adminResolveOption').change(function(){
		var adminResolveOption=$("#adminResolveOption").val();
		if(adminResolveOption=='select'){
			$("#adminResolveComment").attr('disabled','disabled');
			$("#adminResolveComment").val('');
		}else{
			$("#adminResolveComment").removeAttr('disabled');
		}
		$( "#adminResolveCommentDialog" ).attr('style','display : none;');
		var content=$("#adminResolveOption option:selected").val();
		content=content.replace(/\<br>/g, "\n");
		
		//var content=$(this).attr('value');
		if(adminResolveOption!='select'){
		$("#adminResolveComment").val(content);
		}
		//fetchResolveTemplates();
	});
	
	
	// delete the reported note level
	$('.js-admin-action').click(function()
		{
		if($(this).val()=="Delete")
		$("#deleteNoteFlag").val("True");
		var adminComment=$("#adminNoteComment").val().trim();
		adminComment = adminComment.replace( /[\s\n\r]+/g, ' ' );
		 while(adminComment.indexOf("\"") != -1){
			 adminComment = adminComment.replace("\"", "`*`");
			}
		 
		var action=$(this).val();
		$('#adminNoteCommentDialog').empty();
		if(action=='Cancel')
			{
			$('#deleteReportNoteModal').modal('hide');
			$('#moreActions').modal('show');
			}
		else if(adminComment!="")
		{
		$('#adminNoteCommentDialog').empty();
		var url = urlForServer + "admin/actionComplaintNote";
		var datastr = '{"listId":"'+listId+'","userId":"'+userId+'","noteName":"'+noteName+'","noteId":"'+noteId+'","adminComment":"'+escape(adminComment)+'","action":"'+action+'","actionFrom":"'+actionFrom+'","compliantId":"'+compliantId+'"}';
	  
	 $.ajax({
		  headers: { 
			  "Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn		
	    	},
         url: url,
         data: datastr,
         //dataType: "json",
         type: "POST",
         success: function(data){
        			if(data!=null && data=='success')
    				{
        			$('#deleteReportNoteModal').modal('hide');
        			if(actionFrom=='Note'){
        				//$('.js-delete-report-note').attr('disabled','disabled');
        				$('.js-delete-report-note').attr('style','display:none;');
        				$('.close').attr('style','display:none;');
        				$('#moreActions').modal('show');
        			}else{
        				$('.js-delete-report-note').attr('style','display:block;');
        				$('.close').attr('style','display:block;');
        			}
        			
        			}
        	}
	 });
		}
		else
			{

			 $( "#adminNoteCommentDialog" ).attr('style','display : block;color:red;margin-top:-4px;');	
		     	$('#adminNoteCommentDialog').empty();
			 $("#adminNoteCommentDialog").text("Field cannot be blank");
		 
			}
	});
	
$('.js-status-report').click(function(){
		var adminComment=$("#adminResolveComment").val().trim();
		var adminResolveOption=$("#adminResolveOption").val();
		var action=$(this).val();
		$('#adminNoteCommentDialog').empty();
		if(adminResolveOption!="select")
		{
			if((adminResolveOption!="select")&&(adminComment=="")){
				$( "#adminResolveCommentDialog" ).attr('style','display : block;color:red;margin-top:-4px;');	
		     	$('#adminResolveCommentDialog').empty();
		     	$("#adminResolveCommentDialog").text("Field cannot be blank");
		 
			}else{
				adminComment=adminComment.replace(/\<br \/\>/g, "\n");
				adminComment=adminComment.replace(/\n\r?/g, '<br />');
				$('#adminNoteCommentDialog').empty();
				var url = urlForServer + "admin/adminStatusReport";
				var datastr = '{"listId":"'+listId+'","userId":"'+userId+'","noteName":"'+noteName+'","noteId":"'+noteId+'","adminComment":"'+escape(adminComment)+'","action":"'+action+'","compliantId":"'+compliantId+'","mailTemplateId":"'+$("#adminResolveOption option:selected").attr("id")+'"}';
				$.ajax({
					headers: { 
						"Mn-Callers" : musicnote,
				    	"Mn-time" :musicnoteIn		
					},
					url: url,
					data: datastr,
					//dataType: "json",
					type: "POST",
					success: function(data){
						if(data!=null && data=='success')
						{
							$('#adminResolveModal').modal('hide');
							//$('#moreActions').modal('show');
							fetchComplaintDetailsForAdmin();
						}
					}
				});
			}
			}
			else
			{
				$( "#adminResolveCommentDialog" ).attr('style','display : block;color:red;margin-top:-4px;');	
		     	$('#adminResolveCommentDialog').empty();
		     	$("#adminResolveCommentDialog").text("Please select any one template");
			}
});
	
	
	
	
	
	// for attachment delete-Admin modal panel
	$('#moreActions').on('click','.delete-Attach-report',function(){
		fileNameAttach=this.id;
		$('#moreActions').modal('hide');
		$("#adminattachementComment").val("");
		$('#adminAttachementCommentDialog').attr('style','display:none;');
		$('#deleteReportAttachmentModal').modal("show");
	});
	
	
	$('#reportAttachDeleteCancel').on('click',function(){
		$('#deleteReportAttachmentModal').modal("hide");
		$('#moreActions').modal('show');
	});
	
	$('#attachCloseModalPanel').on('click',function(){
		$('#moveCopyWarning').modal("hide");
	});
	
	
	// attachemnt delete button click action
	$('.js-delete-Attach-report').on('click',function(){
		
		var adminComment=$("#adminattachementComment").val().trim();
		adminComment = adminComment.replace( /[\s\n\r]+/g, ' ' );
		 while(adminComment.indexOf("\"") != -1){
			 adminComment = adminComment.replace("\"", "`*`");
			}
		 
		var optionSelect='crowdOnly';
		if(adminComment==''){

			 $("#adminAttachementCommentDialog" ).attr('style','display : block;color:red;margin-top:-4px;');	
		     	$('#adminAttachementCommentDialog').empty();
			 $("#adminAttachementCommentDialog").text("Field cannot be blank");
		}
		 
		/*else if(optionSelect=='bothNoteCrowd'){
			var fileName = fileNameAttach;
			var url = urlForServer+"admin/deleteAttachmentInBothNoteCrowd/"+fileName+"/"+userId;
			var adminComment='attachment deleted both note and crowd by admin';var action='Delete';
			var params='{"listId":"'+listId+'","noteId":"'+noteId+'","noteName":"'+noteName+'","adminComment":"'+adminComment+'","action":"'+action+'"}';
			params = encodeURIComponent(params);
			$.ajax({
				headers: { 
		    	"Ajax-Call" : userTokens,
		    	"Ajax-Time" :userLoginTime				
		    	},
				type : 'POST',
				url : url,
				data: params,
				success : function(responseText) {

					$('#deleteReportAttachmentModal').modal('hide');
			        if(responseText=='success'){
			        	$("#moveCopyWarning").modal('show');
						$("#move-modal-message1").text(" File Deleted successfully");
					}
				
				},
				error : function() {
					console.log("<-------error returned deleting file-------> ");
				}
			});	
		}*/
		else if(optionSelect=='crowdOnly'){
			var fileName=fileNameAttach;
			var url = urlForServer+"admin/deleteAttachmentInCrowd/"+fileName+"/"+attacmentUserId;
			var action='Delete';
			var params='{"listId":"'+listId+'","noteId":"'+noteId+'","noteName":"'+noteName+'","adminComment":"'+adminComment+'","action":"'+action+'","compliantId":"'+compliantId+'"}';
			params = encodeURIComponent(params);
			$.ajax({
				headers: { 
					"Mn-Callers" : musicnote,
			    	"Mn-time" :musicnoteIn				
		    	},
				type : 'POST',
				url : url,
				data: params,
				success : function(responseText) 
				{
					$('#deleteReportAttachmentModal').modal('hide');
			        if(responseText=='success'){
			        	//$("#moveCopyWarning").modal('show');
						//$("#move-modal-message1").text("File Deleted successfully");
					}

				},
				error : function() {
					console.log("<-------error returned deleting file-------> ");
				}
			});	
			
		}
		/*else if(optionSelect=='noteOnly'){
			
			$("#warningMsgs").attr('style','display:none;');
		var fileName = fileNameAttach;
		var url = urlForServer+"Lesson/deleteUploadFile/"+fileName+"/"+userId;
		var params='{"listId":"'+listId+'","noteId":"'+noteId+'"}';
		params = encodeURIComponent(params);
		$.ajax({
			headers: { 
	    	"Ajax-Call" : userTokens,
	    	"Ajax-Time" :userLoginTime				
	    	},
			type : 'POST',
			url : url,
			data: params,
			success : function(responseText) {
				var data=jQuery.parseJSON(responseText);
				$('#deleteReportAttachmentModal').modal('hide');
		        if(data[0].status=='success'){
		        	$("#moveCopyWarning").modal('show');
					$("#move-modal-message1").text("  File Deleted successfully");

					
					if(selectedFiles.indexOf(fileName) != -1){
						selectedFiles.splice(selectedFiles.indexOf(fileName),1); 
					}
					
					$("#notes").children('#'+listIdFromList+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').children('.attach-badge').remove();
					if(selectedFiles.length > 0 && selectedFiles[0] != null){
						attach = '<div class="badge attach-badge icon-sm"  title="This note has '+selectedFiles.length+' attachment(s).">'
									+'<span class="icon-download-alt"></span>'
									+'<span class="badge-text">&nbsp;&nbsp;'+selectedFiles.length+'</span>'     
									+'</div>';
						var descId = $("#notes").children('#'+listIdFromList+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').find('.decs');
						if($(descId).attr('class') == undefined){
							var cmtLen = $("#notes").children('#'+listIdFromList+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').children('.cmts').find('.badge-text').text();
							if(vote.length > 0 ){
								if(cmtLen > 0){
									$("#notes").children('#'+listIdFromList+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').find('.cmts').after(attach);
								}else{
									$("#notes").children('#'+listIdFromList+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').find('.vote-badge').after(attach);
								}
							}else{
								if(cmtLen > 0){
									$("#notes").children('#'+listIdFromList+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').find('.cmts').after(attach);
								}else{
									$("#notes").children('#'+listIdFromList+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').prepend(attach);	
								}
							}
						}else{
							$(descId).after(attach);
						}
					}
					setTimeout(function(){ 
						$('#moveCopyWarning').modal('hide');
						//checkOwnerOfList();
					},1000);
		        }else if(data[0].status=='error'){
					$("#moveCopyWarning").modal('show');
					$("#move-modal-message1").text("  Unable to delete File Attached");
					//setTimeout(function(){ $('#moveCopyWarning').modal('hide');},4000);
					
		        }
		        else if(data[0].status=='otherNote'){
					$("#moveCopyWarning").modal('show');
					$("#move-msg-modal-body1").attr('style','width:330px;');
					$("#move-modal-message1").text("  File Attached with some other notes in same List");
					setTimeout(function(){ $('#moveCopyWarning').modal('hide');
						$("#move-msg-modal-body1").attr('style','');
					},2500);
				}else if(data[0].status=='otherList'){
					$("#moveCopyWarning").modal('show');
					$("#move-msg-modal-body1").attr('style','width:330px;');
					$("#move-modal-message1").text("  File Attached with some other List");
					setTimeout(function(){ $('#moveCopyWarning').modal('hide');
						$("#move-msg-modal-body1").attr('style','');
						checkOwnerOfList();
					},2500);
				}

			},
			error : function() {
				console.log("<-------error returned deleting file-------> ");
			}
		});	
		}*/
	});	
	
	// description delete 
	
	$('#moreActions').on('click','.js-note-delet-desc',function(){
		fileNameAttach=this.id;
		$('#moreActions').modal('hide');
		actionFrom="Description";
		$("#adminNoteComment").val("");
		$('#adminNoteCommentDialog').attr('style','display:none;');
		$('#deleteReportNoteModal').modal("show");
	});
	
	
	
	//
	$('#userComplaintNote').on('click','.noteDiv ',function(e){
		 if (e.target !== this) return;
			
		   moreActionBasedId=$(this).children('.todo_descriptions');
		   listIdFromList=$(this).parent().parent().attr('id');
		   noteIdFromList=$(this).attr('id');
		
		   noteNameFromList =$(this).children('.todo_descriptions').find("p").text();
		   parentDivName= "crowd";
		   //checkOwnerOfList();
	});
});
/////////////////for crowd//////////////////////////////
	function fetchComplaintDetailsForAdmin(){
		$('#userComplaintGrid').empty();
		var notesDetailsJsonObj="";
		var userComplaint=$("#userComplaint").val();
		var url = urlForServer + "admin/fetchComplaintDetailsForAdmin";
		var datastr = '{"userComplaint":"'+userComplaint+'"}';
		$('#userComplaintGrid').append('<table id="taskTables" ></table><div id="tablePages"></div>');
		var params = encodeURIComponent(datastr);
		$.ajax({
			 headers: { 
				 "Mn-Callers" : musicnote,
			    	"Mn-time" :musicnoteIn	
			 },
					type : 'POST',
					url : url,
					data : params,
					success : function(responseText) 
					{
						if(responseText==''){
							if(userComplaint=='select'){
								$('#noRecord').attr('style','display:none;');
							}
							else{
								$('#noRecord').attr('style','display:block;color:red;margin-left:170px;margin-top:60px;');
							}
						}else{
						var data1 = "";
						var dataLength="";
						data1 = jQuery.parseJSON(responseText);
						if(data1!=null && data1!="")
						{
							for(var i=0;i<data1.length;i++){
							}
						dataLength= data1.length ;
						if(dataLength>=11)
							{
								 height= 450;
							}
							else
							{
								height=  dataLength * 25+50;
							}
						$('#taskTables').jqGrid({
							data:data1,
		   	        		datatype: "local",
		   	        		sortable: true,       		
		   	        		height: 200,
			                width:950,
			                colNames:['Name','Mail Id','Note Name','Date','Complaint','Complaint Status','Report For','adminComment','mailTemplateId','userId','noteId','listId','commentId','Description','Complaint Status','Note ID'],
		   	        	   	 colModel:[
		   	        	   	    {name:'userName',index:'Id', width:110},
		   	        	   		{name:'emailId',index:'Id', width:200,editable:true},
		   	        	   		{name:'noteName',index:'level', width:140},
		   	        	   		{name:'date',width:220},
		   	        	   
		   	        	   		{name:"view",width:120,index:'Description', formatter: 'showlink', align:"center",cellattr: function (cellValue, options, rowdata)
							     {  
							         return "><a id='link' title="+cellValue+" onclick='javascript:load("+cellValue+");'><font color='blue'><u>View</u></font></a";
							     }
		   	        	   		},
		   	        	   		{name:'status',hidden:true,width:180,cellattr: function(rowId, val, rawObject) {
				   	        	    if (val==' Unresolved') {
				   	        	        return " class='ui-state-error-text ui-state-error'";
				   	        	    }else if(val==' Resolved'){
				   	        	    	
				   	        	    	return "class=''";
				   	        	    }
				   	        	}},
		   	        	   		{name:'reportLevel',width:290},
		   	        	   		{name:'adminComment',width:50,hidden:true},
		   	        	   		{name:'mailTemplateId',width:50,hidden:true},
		   	        	   		{name:'userId',width:50,hidden:true},
		   	        	   		{name:'listId',width:50,hidden:true},
		   	        	   		{name:'noteId',width:50,hidden:true},
		   	        	   		{name:'commentId',width:50,hidden:true},
		   	        	   		{name:'Description',width:100,hidden:true},
		   	        	   		{name:'adminAction',width:200, align:"center",cellattr: function(rowId, val, rawObject) {
				   	        	    if (val==' Open ') {
				   	        	        return " class='ui-state-error-text ui-state-error'";
				   	        	    }else if(val=='Resolved'){
				   	        	    	return "class='ui-state-default'";
				   	        	    }else if(val=='Hold'){
				   	        	    	return "";
				   	        	    }else if(val=='Rejected'){
				   	        	    	return "class='ui-state-disabled'";
				   	        	    }
				   	        	    
				   	        	}},
		   	        	   		{name:'complaintId',width:130},
		   	        	   ],
		   	        	   
		   	        	pager: '#tablePages',
		   	        	   viewrecords: true,
		   	        	   rownumbers:true, 
		   	        	   sortorder: "desc",
		   	        	   rowNum:9,
		   	        	   pginput: "false"
		   	        	 });
						$("#taskTables").jqGrid('navGrid',"#tablePages",{edit:false,add:false,del:false,search:false});
						//columnName click disable
						var $grid2 = $("#taskTables"), columnName2 = 'emailId',
					    $th = $("#" + $.jgrid.jqID($grid2[0].id) + "_" + columnName2);
						$th.unbind("click");
						var $grid3 = $("#taskTables"), columnName3 = 'noteName',
					    $th = $("#" + $.jgrid.jqID($grid3[0].id) + "_" + columnName3);
						$th.unbind("click");
						var $grid4 = $("#taskTables"), columnName4 = 'date',
					    $th = $("#" + $.jgrid.jqID($grid4[0].id) + "_" + columnName4);
						$th.unbind("click");
						var $grid5 = $("#taskTables"), columnName5 = 'view',
					    $th = $("#" + $.jgrid.jqID($grid5[0].id) + "_" + columnName5);
						$th.unbind("click");
						var $grid6 = $("#taskTables"), columnName6 = 'reportLevel',
					    $th = $("#" + $.jgrid.jqID($grid6[0].id) + "_" + columnName6);
						$th.unbind("click");
						var $grid7 = $("#taskTables"), columnName7 = 'adminAction',
					    $th = $("#" + $.jgrid.jqID($grid7[0].id) + "_" + columnName7);
						$th.unbind("click");
						var $grid8 = $("#taskTables"), columnName8 = 'complaintId',
					    $th = $("#" + $.jgrid.jqID($grid8[0].id) + "_" + columnName8);
						$th.unbind("click");
						$('#userComplaintGrid').append('<br>&nbsp;&nbsp;&nbsp;<a id="viewComplaintNote" class="btn btn-success fontStyle">View</a>');
						
						
						$("#viewComplaintNote").click(function(){ 
							$('.close').attr('style','display:block;');
							 var selRowId = $("#taskTables").jqGrid ('getGridParam', 'selrow');
							$('#complaintStatus').val($("#taskTables").jqGrid ('getCell', selRowId, 'adminAction'));
							$('#mailTemplateId').val($("#taskTables").jqGrid ('getCell', selRowId, 'mailTemplateId'));
							$('#adminComment').val($("#taskTables").jqGrid ('getCell', selRowId, 'adminComment'));
							if(selRowId!=null && selRowId!='undefined'){
								$("#errormsg").attr('style','display:none;');
							 var retriveData = jQuery("#taskTables").jqGrid('getRowData',selRowId);   //get the selected row
									userName = retriveData.userName;  //get the data from selected row by column name
									emailId = retriveData.emailId;
									noteName = retriveData.noteName;
									date = retriveData.date;
									Description = retriveData.Description;
									reportLevel = retriveData.reportLevel;
									listId = retriveData.listId;
									userId = retriveData.userId;
									noteId = retriveData.noteId;
									commentId = retriveData.commentId;
									compliantId=retriveData.complaintId;
									$('#userComplaintNote').empty();
									var url = urlForServer + "admin/getComplaintNote";
									var datastr = '{"listId":"'+listId+'","userId":"'+userId+'","noteId":"'+noteId+'"}';
								  
								  $.ajax({
									  headers: { 
										  "Mn-Callers" : musicnote,
									    	"Mn-time" :musicnoteIn	
									  },
							         url: url,
							         data: datastr,
							         dataType: "json",
							         type: "POST",
							         success: function(data){
							        		if(data!=null && data!='')
							    			{
							        		for ( var i = 0; i < data.length; i++) {
							        			notesDetailsJsonObj=data[i];
											var tagArray = new Array();
											var tagNames="";
											var noteTags="";
											var noteName='';
											var noteId='';
											var access='';
											var publicUser='';
											var userId='';
											var noteData='';
											var comments='';
											var listId='';
											var tagIds='';
											var status='';
											var notes=data[i];
											if(notesDetailsJsonObj['tagIds']!=null && notesDetailsJsonObj['tagIds']!=''){
												var tagWithParams= notesDetailsJsonObj['tagIds'];
												if(tagWithParams!= undefined && tagWithParams.indexOf("[") != -1){
													tagWithParams = tagWithParams.substring(tagWithParams.indexOf("[")+1,tagWithParams.length);
												}
												if(tagWithParams!= undefined && tagWithParams.indexOf("]") != -1){
													tagWithParams = tagWithParams.substring(0,tagWithParams.indexOf("]"));
												}
												if(tagWithParams!= undefined && tagWithParams.indexOf(",") != -1){
													tagArray = tagWithParams.split(',');
												}else{
													if(tagWithParams!= undefined && tagWithParams.trim() != ''){
														tagArray = tagWithParams.split(',');
													}
												}
												loadTagsForCrowd();
												for(var siz=0;siz<tagArray.length;siz++){
													var tagId=tagArray[siz].trim();
													if (tagId in crowdTagMap){
														tagNames+=" "+crowdTagMap[tagId]+" ,";
													}
												}
												if(tagNames!=null && tagNames!=''){
													var lastIndex  = tagNames.lastIndexOf(",");
													var tagNames = tagNames.substring(0, lastIndex);	
												noteTags='<div class="badge icon-sm decs2" style="white-space: normal;display:inline;background-color:#FFF5BB"" title="'+tagNames+'"><span class="icon-tags"></span><span class="badge-text">'+tagNames+'</span></div>';
												}
											}
											datedesc='<div class="badge icon-sm decs2" style="white-space: normal;display:inline;" title=" Posted by '+notesDetailsJsonObj['publicUser']+' on '+notesDetailsJsonObj['publicDate']+'"><span class="icon-user"></span><span class="badge-text"> Posted by '+notesDetailsJsonObj['publicUser']+' on '+notesDetailsJsonObj['publicDate']+'</span></div>';
											noteData=noteData+'<div class=row><div></div><div class=span2></div><div id='+notesDetailsJsonObj['listId']+' class="listDivs span10" style="background-color: rgb(243, 243, 243)"><div class="listBodyAll" id="complaintDiv" style="margin-left:15px;height:70px;"><div id='+notesDetailsJsonObj['noteId']+' class="noteDiv" ><div class="todo_descriptions fontStyle15"  ><p id="'+data[i].userId+'~'+data[i].listId+'~'+data[i].noteId+' ">'+ notesDetailsJsonObj['noteName']+'</p></div><div class="badges">'+datedesc+noteTags+'</div></div></div></div></div>';
										
										}
										
										var data='<div class="crowd" id="complaintCrowdNote">'+noteData+'</div>';
										$('#userComplaintNote').attr('style','background-color: rgb(243, 243, 243); width: 550px; height: 70px; padding: 15px 15px 15px 15px;');
										$('#userComplaintNote').append(data);
										}
							          
							          }
							       });
									
							 }else{
								 $('#userComplaintNote').hide();
								 $("#errormsg").attr('style','display:block;color:red;');
							 }
								
								});
						
					
						}
						}
				},
		error:function(){
			console.log("<-------Error returned while welcome user mail-------> ");
		}
		});
	}

	/////////////////for note and memo//////////////////////////////
	function fetchNoteComplaintDetailsForAdmin(){
		$('#noteComplaintGrid').empty();
		var notesDetailsJsonObj="";
		var noteComplaint=$("#noteComplaint").val();
		var url = urlForServer + "admin/fetchNoteComplaintDetailsForAdmin";
		var datastr = '{"noteComplaint":"'+noteComplaint+'"}';
		$('#noteComplaintGrid').append('<table id="taskTabless" ></table><div id="tablePagess"></div>');
		var params = encodeURIComponent(datastr);
		$.ajax({
			 headers: { 
				 "Mn-Callers" : musicnote,
			    	"Mn-time" :musicnoteIn	
			 },
					type : 'POST',
					url : url,
					data : params,
					success : function(responseText) 
					{
						if(responseText==''){
							if(noteComplaint=='select'){
								$('#noRecordexist').attr('style','display:none;');
							}
							else{
								$('#noRecordexist').attr('style','display:block;color:red;margin-left:170px;margin-top:60px;');
							}
						}else{
						var data1 = "";
						var dataLength="";
						data1 = jQuery.parseJSON(responseText);
						if(data1!=null && data1!="")
						{
							for(var i=0;i<data1.length;i++){
							}
						dataLength= data1.length ;
						if(dataLength>=11)
							{
								 height= 450;
							}
							else
							{
								height=  dataLength * 25+50;
							}
						$('#taskTabless').jqGrid({
							data:data1,
		   	        		datatype: "local",
		   	        		sortable: true,       		
		   	        		height: 200,
			                width:900,
			                multiselect:false,
			               
			                colNames:['Name','Mail Id','Note Name','Date','Complaint','Complaint Status','Report For','userId','noteId','listId','commentId','Description','Admin Action','Complaint ID'],
		   	        	   	 colModel:[
		   	        	   	    {name:'userName',index:'Id', width:110},
		   	        	   		{name:'emailId',index:'Id', width:180,editable:true},
		   	        	   		{name:'noteName',index:'level', width:140},
		   	        	   		{name:'date',width:200},
		   	        	   
		   	        	   		{name:"view",width:120,index:'Description', formatter: 'showlink', align:"center",cellattr: function (cellValue, options, rowdata)
							     {  
							         return "><a id='link' title="+cellValue+" onclick='javascript:loadNote("+cellValue+");'><font color='blue'><u>View</u></font></a";
							     }
		   	        	   		},
		   	        	   		{name:'status',hidden:true,width:180,cellattr: function(rowId, val, rawObject) {
				   	        	    if (val==' Unresolved') {
				   	        	        return " class='ui-state-error-text ui-state-error'";
				   	        	    }else if(val==' Resolved'){
				   	        	    	return "class='ui-state-active'";
				   	        	    }else if(val==' Pending'){
				   	        	    	return "class='welcome'";
				   	        	    }
				   	        	}},
		   	        	   		{name:'reportLevel',width:130},
		   	        	   		{name:'userId',width:50,hidden:true},
		   	        	   		{name:'listId',width:50,hidden:true},
		   	        	   		{name:'noteId',width:50,hidden:true},
		   	        	   		{name:'commentId',width:50,hidden:true},
		   	        	   		{name:'Description',width:100,hidden:true},
		   	        	   		{name:'adminAction',width:130},
		   	        	   		{name:'complaintId',width:130},
		   	        	   ],
		   	        	   
		   	        	   pager: '#tablePagess',
		   	        	   viewrecords: true,
		   	        	   rownumbers:true, 
		   	        	   sortorder: "desc",
		   	        	   rowNum:9,
		   	        	   pginput:false
		   	        	 });
						$("#taskTabless").jqGrid('navGrid',"#tablePagess",{edit:false,add:false,del:false,search:false});
						
						$('#noteComplaintGrid').append('<br>&nbsp;&nbsp;&nbsp;<a id="viewNoteComplaintNote" class="btn btn-success fontStyle">View</a>');
						jQuery("#viewNoteComplaintNote").click(function(){ 
							 var selRowId = jQuery("#taskTabless").jqGrid ('getGridParam', 'selrow');
							if(selRowId!=null && selRowId!='undefined'){
								$("#errormsgs").attr('style','display:none;');
							 var retriveData = jQuery("#taskTabless").jqGrid('getRowData',selRowId);   //get the selected row
									userName = retriveData.userName;  //get the data from selected row by column name
									emailId = retriveData.emailId;
									noteName = retriveData.noteName;
									date = retriveData.date;
									Description = retriveData.Description;
									reportLevel = retriveData.reportLevel;
									listId = retriveData.listId;
									userId = retriveData.userId;
									noteId = retriveData.noteId;
									commentId = retriveData.commentId;
									compliantId=retriveData.complaintId;
									
									$('#noteComplaintNote').empty();
									var url = urlForServer + "admin/getComplaintNote";
									var datastr = '{"listId":"'+listId+'","userId":"'+userId+'","noteId":"'+noteId+'"}';
								  
								  $.ajax({
									  headers: { 
										  "Mn-Callers" : musicnote,
									    	"Mn-time" :musicnoteIn	
									  },
							         url: url,
							         data: datastr,
							         dataType: "json",
							         type: "POST",
							         success: function(data){
							        		if(data!=null && data!='')
							    			{
							        		for ( var i = 0; i < data.length; i++) {
							        			notesDetailsJsonObj=data[i];
											var tagArray = new Array();
											var tagNames="";
											var noteTags="";
											var noteName='';
											var noteId='';
											var access='';
											var publicUser='';
											var userId='';
											var noteData='';
											var comments='';
											var listId='';
											var tagIds='';
											var status='';
											var notes=data[i];
											if(notesDetailsJsonObj['tagIds']!=null && notesDetailsJsonObj['tagIds']!=''){
												var tagWithParams= notesDetailsJsonObj['tagIds'];
												if(tagWithParams!= undefined && tagWithParams.indexOf("[") != -1){
													tagWithParams = tagWithParams.substring(tagWithParams.indexOf("[")+1,tagWithParams.length);
												}
												if(tagWithParams!= undefined && tagWithParams.indexOf("]") != -1){
													tagWithParams = tagWithParams.substring(0,tagWithParams.indexOf("]"));
												}
												if(tagWithParams!= undefined && tagWithParams.indexOf(",") != -1){
													tagArray = tagWithParams.split(',');
												}else{
													if(tagWithParams!= undefined && tagWithParams.trim() != ''){
														tagArray = tagWithParams.split(',');
													}
												}
												for(var siz=0;siz<tagArray.length;siz++){
													var tagId=tagArray[siz].trim();
													if (tagId in crowdTagMap){
														tagNames+=" "+crowdTagMap[tagId]+" ,";
													}
												}
												if(tagNames!=null && tagNames!=''){
													var lastIndex  = tagNames.lastIndexOf(",");
													var tagNames = tagNames.substring(0, lastIndex);	
												noteTags='<div class="badge icon-sm decs2" style="white-space: normal;display:inline;background-color:#FFF5BB"" title="'+tagNames+'"><span class="icon-tags"></span><span class="badge-text">'+tagNames+'</span></div>';
												}
											}
											
											/*if(notesDetailsJsonObj['ownerId'] != userId){
												var usnameId = notesDetailsJsonObj['ownerId'];
												alert(usnameId);
												var usname = activeUserDeatilsMap[usnameId];
												alert(usname);
												datedesc='<div class="badge icon-sm decs2" title=" Shared by '+usname+'"><span class="icon-user"></span><span class="badge-text"> Shared by '+usname+'</span></div>';
												 
											}else{*/
											if((notesDetailsJsonObj['publicUser'] != '') && (notesDetailsJsonObj['publicUser'] != null) && (notesDetailsJsonObj['publicUser'] != 'undefined')){
												datedesc='<div class="badge icon-sm decs2" style="white-space: normal;display:inline;" title=" Shared by '+notesDetailsJsonObj['publicUser']+'"><span class="icon-user"></span><span class="badge-text"> Shared by '+notesDetailsJsonObj['publicUser']+'</span></div>';
											}else{
												//activeUserDeatilsMap[notesDetailsJsonObj['userId']];
												datedesc='<div class="badge icon-sm decs2" style="white-space: normal;display:inline;" title=" Shared by '+userDetailsJsonObj['userFirstName']+' '+userDetailsJsonObj['userLastName']+'"><span class="icon-user"></span><span class="badge-text"> Shared by '+userDetailsJsonObj['userFirstName']+' '+userDetailsJsonObj['userLastName']+'</span></div>';
											}
											
											// for due date
											//dueDateData=loadDueDateContent(notesDetailsJsonObj['dueDate'],notesDetailsJsonObj['dueTime']);
											
											//var cmts='<div class="badge voted icon-sm cmts" title="This note has '+comments.length +' comment(s)."><span class="icon-comment"></span><span class="badge-text">&nbsp;&nbsp;'+comments.length+'</span></div>';
											
											
											noteData=noteData+'<div class=row><div></div><div class=span2></div><div id='+notesDetailsJsonObj['listId']+' class="listDivs span10" style="background-color: rgb(243, 243, 243)"><div class="listBodyAll" id="complaintDiv" style="margin-left:15px;height:70px;"><div id='+notesDetailsJsonObj['noteId']+' class="noteDiv" ><div class="todo_descriptions fontStyle15"  ><p id="'+data[i].userId+'~'+data[i].listId+'~'+data[i].noteId+' ">'+ notesDetailsJsonObj['noteName']+'</p></div><div class="badges">'+datedesc+noteTags+'</div></div></div></div></div>';

											/*noteData=noteData
											+ '<div class="todo_description fontStyle15">'
											+'<p><b>'
											+ noteName
											+ '</b>'
											+tempDescAfterNote
											+'</p></div>'
											// displaythe cmt, vote, desc, due date
											+'<div class="badges fontStyle">'
											+sharedBy
											+votes
											+cmts
											+desc
											+attach
											+tagss
											+dueDateData
											+datedesc
											+reminderDivContent
											+'</div>'
											+ '</div>';*/
										
										}
										
										var data='<div class="notes" id="complaintCrowdNote">'+noteData+'</div>';
										$('#noteComplaintNote').attr('style','background-color: rgb(243, 243, 243); width: 550px; height: 70px; padding: 15px 15px 15px 15px;');
										$('#noteComplaintNote').append(data);
										}
							          
							          }
							       });
									
							 }else{
								 $('#noteComplaintNote').hide();
								 $("#errormsgs").attr('style','display:block;color:red;');
							 }
								
								});
						
					
						}
						}
				},
		error:function(){
			console.log("<-------Error returned while welcome user mail-------> ");
		}
		});
	}
	 function load(cellValue)
     {
      	var celValue =$("#taskTables").jqGrid ('getCell', cellValue, 'Description');
		 $('#userComplaintView').val(celValue);
		 $('#viewCompaintModal').modal('show');
       }
	 function loadNote(cellValue)
     {
      	var celValue =$("#taskTabless").jqGrid ('getCell', cellValue, 'Description');
		 $('#userComplaintView').val(celValue);
		 $('#viewCompaintModal').modal('show');
       }
	function createMoreActionModal(userId,commentId,reportLevel){
		
		//delete button hide for crowd compalint
		var url = urlForServer + "admin/crowdComplaintDeleteButtonBlock/"+compliantId;
		$.ajax({
			headers: { 
				"Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn				
	    	},
			type : 'POST',
			url : url,
			cache : false,
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			success : function(responseText) {
					//find crowd delete note
					noteDeleteAccess=responseText[0].adminCompalintAction;
			}
		});
		
		$('#content').empty();
		$('#tagFilterUl li').not('.tagit-new').remove();
		var tagsArray=new Array();
		var userDetailsJsonObj;
		noteId=noteId.trim();
		listId=listId.trim();
		userId=userId.trim();
		
		var url = urlForServer+"note/getNote/"+listId+"/"+noteId+"/"+userId;
		
		$.ajax({
			 headers: {
				 "Mn-Callers" : musicnote,
			    	"Mn-time" :musicnoteIn	
			 },
				type: 'POST',
				url : url,
				cache: false,
				success : function(responseText){
					var data = jQuery.parseJSON(responseText);
					var desc='';
					var commentsDiv='';
					var attachDiv='';
					var description='';
					var tagss="";
					var tagNames="";
					var votesWithParam;
					var cmtWithParams;
					var attachWithParams; 
					var noteSharedAllContact;
					var noteOwnerId;
					var noteLevel;
					
					var noteSharedAllContactMembers;
					var noteSharedAllContactMembersIds =new Array();
					var comments = new Array();
					var commentSize='0';
					var remainder = new Array();
					var attachArray = new Array();
					var tagArray = new Array();
					vote =new Array();
					var viteWithUser='vote';
					var voteDivClass = 'js-vote';
					var dueDate = '';
					var voteArchieve = '';
					var location='';
					
					if(data!=null && data!=''){
						
						//description=data['description'];
						votesWithParam = data['vote'];
						$('#tagFilterUl').remove();
						$('#tagFilterDiv').append('<ul id="tagFilterUl" class="span"></ul>');
						/*This code used to separate notes comments and crowd comments. by Venu*/
						/*if(listType=='crowd')
							cmtWithParams = data['pcomments'];
						else
							cmtWithParams = data['comments'];*/
						if(listType=='crowd')
							cmtWithParams = data['pcomments'];
						else
							cmtWithParams = data['comments'];
						
						if(listType=='crowd'){
							if(data['publicDescription']==undefined){
								description = data['description'];
							}else{
								description = data['publicDescription'];
							}
						}
						else{
							description = data['description'];
						}
						if(listType=='crowd'){
							attachWithParams =data['privateAttachFilePath'];
						}else{
							attachWithParams =data['attachFilePath'];
						}
						
						
						if(listType=="schedule"){
							noteSharedAllContact=data['eventSharedAllContact'];
							location=data['location'];
						}
						else
							noteSharedAllContact=data['noteSharedAllContact'];
						
						if(listType!="schedule")
						{
						if(data['shared'] =='shared')
							sharedHideDelete=true;
						else
							sharedHideDelete=false;
						}
						
						noteOwnerId=data['userId'];
						noteLevel=data['noteLevel'];
						fromCrowd=data['fromCrowd'];
						
						if(data['userId'] != userId){
							if(listType=="schedule")
								noteSharedAllContactMembers=data['eventSharedAllContactMembers'];
							else
								noteSharedAllContactMembers=data['noteSharedAllContactMembers'];
							
							if(noteSharedAllContactMembers.indexOf("[") != -1){
								noteSharedAllContactMembers = noteSharedAllContactMembers.substring(noteSharedAllContactMembers.indexOf("[")+1,noteSharedAllContactMembers.length);
							}
							if(noteSharedAllContactMembers.indexOf("]") != -1){
								noteSharedAllContactMembers = noteSharedAllContactMembers.substring(0,noteSharedAllContactMembers.indexOf("]"));
							}
							if(noteSharedAllContactMembers.indexOf(",") != -1){
								noteSharedAllContactMembersIds = noteSharedAllContactMembers.split(',');
							}else{
								if(noteSharedAllContactMembers.trim() != ''){
									noteSharedAllContactMembersIds = noteSharedAllContactMembers.split(',');
								}
							}
						}
						
						if(votesWithParam.indexOf("[") != -1){
							votesWithParam = votesWithParam.substring(votesWithParam.indexOf("[")+1,votesWithParam.length);
						}
						if(votesWithParam.indexOf("]") != -1){
							votesWithParam = votesWithParam.substring(0,votesWithParam.indexOf("]"));
						}
						if(votesWithParam.indexOf(",") != -1){
							vote = votesWithParam.split(',');
						}else{
							if(votesWithParam.trim() != ''){
								vote = votesWithParam.split(',');
							}
						}
						
						//Added by veera for tag name display with notes
						if(listType!='crowd'){
							
							
						if(data['tag']!=null && data['tag'] !=""){
							var tagWithParams= data['tag'];
							if(tagWithParams!= undefined && tagWithParams.indexOf("[") != -1){
								tagWithParams = tagWithParams.substring(tagWithParams.indexOf("[")+1,tagWithParams.length);
							}
							if(tagWithParams!= undefined && tagWithParams.indexOf("]") != -1){
								tagWithParams = tagWithParams.substring(0,tagWithParams.indexOf("]"));
							}
							if(tagWithParams!= undefined && tagWithParams.indexOf(",") != -1){
								tagArray = tagWithParams.split(',');
							}else{
								if(tagWithParams!= undefined && tagWithParams.trim() != ''){
									tagArray = tagWithParams.split(',');
								}
							}
							
							for(var siz=0;siz<tagArray.length;siz++){
								var tagId=tagArray[siz].trim();
								/*if (tagId in notesTagMap){
									tagNames+="["+notesTagMap[tagId]+"]";
									var selectedTag='<li class="tagit-choice"><div class="tagit-label">'+notesTagMap[tagId]+'</div></li>';
									tagsArray.push(notesTagMap[tagId]);
								}*/
							}
							
							
						}
						
						if(listType!="schedule" && (listOwnerFlag || noteLevel == 'edit'))
							{
								tagss ='<div class="badge icon-sm tagss" title="'+tagNames+'">'
									+'<span class="icon-tags"></span><span class="badge-text">&nbsp;&nbsp;'+tagNames+'</span></div>';
							}
						}
						
						if(cmtWithParams.indexOf("[") != -1){
							cmtWithParams = cmtWithParams.substring(cmtWithParams.indexOf("[")+1,cmtWithParams.length);
						}
						if(cmtWithParams.indexOf("]") != -1){
							cmtWithParams = cmtWithParams.substring(0,cmtWithParams.indexOf("]"));
						}
						if(cmtWithParams.indexOf(",") != -1){
							comments = cmtWithParams.split(',');
						}else{
							if(cmtWithParams.trim() != ''){
								comments = cmtWithParams.split(',');
							}
						}
												
						
						if(attachWithParams.indexOf("[") != -1){
								attachWithParams = attachWithParams.substring(attachWithParams.indexOf("[")+1,attachWithParams.length);
						}
						if(attachWithParams.indexOf("]") != -1){
							attachWithParams = attachWithParams.substring(0,attachWithParams.indexOf("]"));
						}
						if(attachWithParams.indexOf(",") != -1){
								attachArray = attachWithParams.split(',');
							
						}else{
								if(attachWithParams.trim() != ''){
									attachArray = attachWithParams.split(',');
								}
						}
						attachDiv = '<div class="badge attach-badge icon-sm"  title="This note has '+attachArray.length+' attachment(s).">'
										+'<span class="icon-download-alt"></span>'
										+'<span class="badge-text">&nbsp;&nbsp;'+attachArray.length+'</span>'     
									+'</div>';
						if(attachArray.length > 0 && attachArray[0]!=null ){
							/*This code used to attachements. by kishore*/
							url= urlForServer+"note/getAttachedByFileId/"+userId;
							
							params = encodeURIComponent(attachArray);
							var attObj;
							$.ajax({	
								 headers: { 
									 "Mn-Callers" : musicnote,
								    	"Mn-time" :musicnoteIn	
								 },
								type : 'POST',
								url : url,
								data : params,
								success : function(response) {
									var attachData = jQuery.parseJSON(response);
									if(attachData != null && attachData!= ''){
										var attachDiv1 =	'<div class="window-module clearfix" style="margin-bottom: 0;">'
											+'<div class="window-module-title no-divider">'
												+'<span class="window-module-title-icon icon-tasks"></span>'
												+'<h4 >Attachments</h4>'
											+'</div>';
										
										attachDiv1 = attachDiv1 +'<div class="list-actions1 " style="top: -20px; position: relative;">';
							
										for ( var i = 0; i < attachData.length; i++) {
										
											attObj = attachData[i];
											var ext;
											if(attObj.fileName!= -1){
												attacmentUserId=attObj.userId;
												ext = attObj.fileName.substring(attObj.fileName.lastIndexOf('.')+1,attObj.fileName.length);
											}
										var playclass='';
										var playIcon='';
										var link='javascript:void(0);';
										var target='';
										
										if(ext=="mov" || ext=="mp4" || ext=="avi" || ext=="wmv"  || ext=="wma"){
											playclass="playRecordedMoreActions";
											playIcon='<i class="icon-play" title="Click to play" />';
										}
										else if(ext=="mp3" || ext=="wav" || ext=="amr"){
											playclass="playRecordedMoreActions";
											playIcon='<i class="icon-play" title="Click to play" />';
										}else if(ext=="jpg" || ext=="png" || ext=="jpeg"){
											playclass="viewAdminImageMoreActions";
										}
										else if(ext=="pdf"){
											playclass="openPdf"	;
																						
										}
											attachDiv1 = attachDiv1  +'<div class="phenom clearfix" style ="border-bottom:none;width:300px;">'
											    +'<a id="'+attObj.fullPath+'~'+attObj.userId+'" href="'+link+'" target="'+target+'" class="member '+playclass+'" title="'+attObj.fileName+'" style="width: 48px; height: 40px; left: -20px;">'												
											    +'<span class="ext">'+ext+'</span>'+playIcon
												+'</a>';
											if(listType!='crowd' && userId == attObj.userId){
												attachDiv1 = attachDiv1 +'<p style="margin: 5px 0 0 39px;position: absolute;width:75%;">';
											}else{
												attachDiv1 = attachDiv1 +'<p style="margin: 5px 0 0 39px;position: absolute;width:75%;">';
											}
											if((attObj.fileName).length>70){
											var fileName1=(attObj.fileName).substring(0, 67);
											attObj.fileUploadName=fileName1+"...";
											}
												attachDiv1 = attachDiv1 +'<span class="title ellip" style="display:block;line-height:110%;word-wrap: break-word;white-space:normal;word-break: break-all;">'
													+'<a id="'+attObj.fileName+'" title="'+attObj.fileUploadName+'" style="'+attObj.fileUploadName+'"  class="file-download ~ '+attObj.userId+'" href="javascript:void(0);">'+attObj.fileUploadName+'</a>'
												+'</span>';
												
												if(listType!='crowd' && userId == attObj.userId ){
													attachDiv1 =attachDiv1 +'<br><span style="position: absolute; top: 26px;font-weight: bolder;" class="ellip" id="'+attObj.fileName+'"><a title="Click to remove" class="remove-file-download" href="javascript:void(0);" style="color: #939393;">Remove</a>&nbsp;&nbsp;&nbsp;&nbsp;<a  class="ellip" href="javascript:void(0);" style="color: #939393;">'+attObj.currentTime+'</a></span>'
														+'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
														//+'<span id="'+attObj.fileName+'" class="ellip" style="position: absolute; top: 23px;font-weight: bolder;">'
														//	+'<a  title="Click to delete" class="delete-file-download" href="javascript:void(0);" style="color: #939393;" >Delete</a>'
													//+'</span>';
												}
												else
												{
													//*if(reportLevel=='attachment'){
													attachDiv1 =attachDiv1 +'<br><span style="position: absolute; top: 26px;font-weight: bolder;" class="ellip" id="'+attObj.fileName+'"><a  class="ellip" href="javascript:void(0);" style="color: #939393;">'+attObj.currentTime+'</a><a id="'+attObj.fileName+'" class="delete-Attach-report icon-remove other" style="margin-left:20px;" title="Click to delete"></a></span>'
													+'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
													//*}else{
													//*	attachDiv1 =attachDiv1 +'<br><span style="position: absolute; top: 26px;font-weight: bolder;" class="ellip" id="'+attObj.fileName+'"><a  class="ellip" href="javascript:void(0);" style="color: #939393;">'+attObj.currentTime+'</a></span>'
													//*	+'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
													//*}
													//+'<span id="'+attObj.fileName+'" class="ellip" style="position: absolute; top: 23px;font-weight: bolder;">'
													//	+'<a  title="Click to delete" class="delete-file-download" href="javascript:void(0);" style="color: #939393;" >Delete</a>'
												//+'</span>';
												}
											attachDiv1= attachDiv1 +'</p>'
											+'</div>';
										}
										getattachedFilesForNotes(listId,noteId,listOwnerFlag,false);
										$("#moreActions").children('.modal-body').children('.span9').children('.hide-on-edit').after(attachDiv1);
									}
									
								},error : function() 
								{
									alert("Please try again later");
									console.log("<-------error returned for new ajax request attach file-------> ");
								}
							});
						}
						
						if(comments.length > 0 && comments[0]!=null){
							/*This code used to separate notes comments and crowd comments. by venu*/
							/*url= urlForServer+"note/getComments/"+listType;*/
							var cmtsLevel;
							var crowdCmdDefaultDiv='';
							if(listType=='crowd')
								cmtsLevel='P';
							else
								cmtsLevel='I';
							url= urlForServer+"note/getComments/"+cmtsLevel+"/"+userId;
							params = encodeURIComponent(comments);
							var comtObj;
							$.ajax({
								 headers: {
									 "Mn-Callers" : musicnote,
								    	"Mn-time" :musicnoteIn	
								 },
								type : 'POST',
								url : url,
								data : params,
								success : function(response) 
								{
									
									comtData = jQuery.parseJSON(response);
									
									var linkArray = new Array();

									if(comtData != null && comtData!= ''){
										commentSize=comtData.length;
										for ( var i = 0; i < comtData.length; i++) {
											comtObj = comtData[i];
											var cmtedUserName;
											var cmtInitials;
											var userFirst;
											var userLast;
											
											if(comtData[i].userId != parseInt(userId)){
												userDetailsJsonObj = activeUserObjectMap[comtData[i].userId] ;
												if((comtData[i].cId==commentId)){
													exactComment='<span class="on" style="background-color:red;float:right;margin-top:-30px;padding:5px;border-radius:9px;"><span class="icon-remove"></span></span>';
												}else{
													exactComment='';
												}
												if(userDetailsJsonObj != undefined && userDetailsJsonObj !=null)
												{
													cmtedUserName = userDetailsJsonObj['userFirstName']+' '+userDetailsJsonObj['userLastName'];
													userFirst=userDetailsJsonObj['userFirstName']; userLast=userDetailsJsonObj['userLastName'];
													cmtInitials=userFirst.substring(0,1).toUpperCase()+userLast.substring(0,1).toUpperCase();
												}
												else{
													//fetchUsersList();
													if(comtData[i].userId != userAdminId){
													cmtedUserName = activeUserDeatilsMap[comtData[i].userId];
													}else{
														cmtInitials=userAdminInitial;
														cmtedUserName=userAdminFullName;
													}
												}
												
											}else{
												
												userDetailsJsonObj = activeUserObjectMap[parseInt(userId)] ;
												if(userDetailsJsonObj != undefined && userDetailsJsonObj !=null)
												{
													if((userDetailsJsonObj['cId']==commentId)){
														$('.action-comment').attr('style','background-color:red');
														exactComment='<span class="on" style="background-color:red;float:right;margin-top:-30px;padding:5px;border-radius:9px;"><span class="icon-remove"></span></span>';
													}else{
														exactComment='';
													}
													cmtedUserName = userDetailsJsonObj['userFirstName']+' '+userDetailsJsonObj['userLastName'];
													userFirst=userDetailsJsonObj['userFirstName']; userLast=userDetailsJsonObj['userLastName'];
													cmtInitials=userFirst.substring(0,1).toUpperCase()+userLast.substring(0,1).toUpperCase();
													//cmtedUserName = userFulName;
													//cmtInitials=userInitial;
												}
												else{
													//fetchUsersList();
													if(comtData[i].userId != userAdminId){
													cmtedUserName = activeUserDeatilsMap[comtData[i].userId];
													}else{
														cmtInitials=userAdminInitial;
														cmtedUserName=userAdminFullName;
													}
												}
											}
											var condate=new Date(comtObj.cDate);
											var convertedDate="";
											convertedDate=month[condate.getMonth()];
											convertedDate=convertedDate+"/"+condate.getDate()+"/"+condate.getFullYear();
											var dateTex=convertedDate.split("/");
											var date=dateTex[0]+" "+dateTex[1]+" "+dateTex[2];
											var timestring=comtObj.cTime.split(":");
											var timee=convertTwentyFourHourToTwelveHourTime(timestring[0]+":"+timestring[1]);
												var editedNotes='';
												var comtsFromObj =comtObj.comments;
												var link="";
												var videoDiv="";
												
												while(comtsFromObj.indexOf("youtube.com/watch?v=") != -1){
													comtsFromObj=comtsFromObj.substring(comtsFromObj.indexOf("youtube.com/watch?v=")+20,comtsFromObj.length);
													link = comtsFromObj.substring(0,comtsFromObj.indexOf("\""));
													comtsFromObj = comtsFromObj.substring(comtsFromObj.indexOf("</a>"),comtsFromObj.length);
													var playLink=comtObj.cId.trim()+link.trim();
													videoDiv = videoDiv + '<div id="'+playLink+'"></div>	';
													link=comtObj.cId.trim()+"~"+link.trim();
													if(linkArray.indexOf(link) == -1){
														linkArray.push(link);
													}
													
												}
														if(comtObj.edited == 1){
															
															var eddate=new Date(comtObj.editDate);
															var editDate="";
															editDate=month[eddate.getMonth()];
															editDate=editDate+"/"+eddate.getDate()+"/"+eddate.getFullYear();
															
															//var editDate = new Date(comtObj.editDate).toLocaleFormat("%b/%d/%Y");
															
															var editDateText=editDate.split("/");
															var editDate=editDateText[0]+" "+editDateText[1]+" "+editDateText[2];
															var edittimestring=comtObj.editTime.split(":");
															var edittimee=convertTwentyFourHourToTwelveHourTime(edittimestring[0]+":"+edittimestring[1]);
															editedNotes = ' - edited ' +editDate +' at '+ edittimee ;
														}
														var cmtsWithBr =  comtObj.comments;
														var arrayCmtsBr = cmtsWithBr.split("<br>");
														if(arrayCmtsBr.length > 0 && arrayCmtsBr[0]!="" ){
															cmtsWithBr = '';
															for(var kj =0; kj< arrayCmtsBr.length;kj++ ){
																cmtsWithBr = cmtsWithBr +  arrayCmtsBr[kj].trim() + "<br>\n";   
															}
														}
														if(cmtsWithBr.lastIndexOf("\n") == (cmtsWithBr.trim().length)){
															cmtsWithBr = cmtsWithBr.substring(0, cmtsWithBr.lastIndexOf("\n")-4);
														}
														var cmdDisplayLevel='';
														
														
													if(listType=='crowd' || listType=='music' && fromCrowd=='True')
													{
														cmdDisplayLevel =  comtObj.commLevel;
														if(cmdDisplayLevel=='P/I')
														{
															crowdCmdDefaultDiv = crowdCmdDefaultDiv + '<div class="phenom clearfix"style="width:300px;"> <div class="creator member js-show-mem-menu">'
															+'<span class="member-initials" title="'+cmtedUserName+'">'+cmtInitials+'</span>'
															+'</div>'
															+'<div class="phenom-desc"> '
																	+'<a class="inline-object js-show-mem-menu" >'+cmtedUserName+'</a> '
																	+'<div class="action-comment" style="width:280px;">'
																	+'<div class="current-comment" style="overflow:auto;">'
																		+'<p>'+cmtsWithBr +'</p>'
																	+'</div>'+exactComment
																	//+'<textarea class="js-text">'+comtObj.comments +'</textarea> ' 
																	+'</div>'
															+'</div>'
															+videoDiv
															+'<p class="phenom-meta quiet">'
																+'<span title="'+date+' at '+timee+'" class="date js-hide-on-sending" dt="'+date+' '+timee+'">'+date+' at '+timee+' '+editedNotes+'</span>'
																+'<span id="'+comtObj.cId+'" class="js-hide-on-sending">';
																	//if(comtObj.userId == userId){
															
																		//*if(comtObj.cId==commentId){ commented for always action enabled
																		crowdCmdDefaultDiv = crowdCmdDefaultDiv +'- <a  class="js-edit-cmt-action" style="color: #939393;font-weight: normal;"></a>'
																		+' <a  class="js-delete-cmt" style="color: #525252;font-weight: normal;">Delete</a>'	;
																		//*}else{
																		//*	crowdCmdDefaultDiv = crowdCmdDefaultDiv +'- <a  class="js-edit-cmt-action" style="color: #939393;font-weight: normal;"></a>'
																		//*	+' <a id="preventDelete" class="js-no-delete-cmt" disabled="disabled" style="color: #DFDFDF;font-weight: normal;">Action</a>'	;
																		//*}
																		
																		//}
																	
																	crowdCmdDefaultDiv = crowdCmdDefaultDiv+' <a  class="js-report-cmt" id="'+comtObj.cId+'" onClick="javascript:reportComment(this.id)" style="color: #939393;font-weight: normal;"></a>' +'</span>'
															+'</p>'
														+'</div>';
															
														}
														else
														{
														commentsDiv = commentsDiv + '<div class="phenom clearfix" style="width:300px;"> <div class="creator member js-show-mem-menu">'
														+'<span class="member-initials" title="'+cmtedUserName+'">'+cmtInitials+'</span>'
														+'</div>'
														+'<div class="phenom-desc"> '
																+'<a class="inline-object js-show-mem-menu" >'+cmtedUserName+'</a> '
																+'<div class="action-comment" style="width:280px;">'
																+'<div class="current-comment" style="overflow:auto;">'
																	+'<p>'+cmtsWithBr +'</p>'
																+'</div>'+exactComment
																//+'<textarea class="js-text">'+comtObj.comments +'</textarea> ' 
																+'</div>'
														+'</div>'
														+videoDiv
														+'<p class="phenom-meta quiet">'
															+'<span title="'+date+' at '+timee+'" class="date js-hide-on-sending" dt="'+date+' '+timee+'">'+date+' at '+timee+' '+editedNotes+'</span>'
															+'<span id="'+comtObj.cId+'" class="js-hide-on-sending">';
																//if(comtObj.userId == userId){
														//*if(comtObj.cId==commentId){
																commentsDiv = commentsDiv +'- <a  class="js-edit-cmt-action" style="color: #939393;font-weight: normal;"></a>'
																	+'<a  class="js-delete-cmt" style="color: #525252;font-weight: normal;">Delete</a>'	;
															//*}else{
															//*	commentsDiv = commentsDiv +'- <a  class="js-edit-cmt-action" style="color: #939393;font-weight: normal;"></a>'
															//*	+'<a id="preventDelete"  class="js-no-delete-cmt" disabled="disabled" style="color: #DFDFDF;font-weight: normal;">Action</a>'	;
															//*}	
																
																commentsDiv = commentsDiv +' <a  class="js-report-cmt" id="'+comtObj.cId+'" onClick="javascript:reportComment(this.id)" style="color: #939393;font-weight: normal;"></a>'  +'</span>'
														+'</p>'
													+'</div>';
														}
													}
													else
													{
														
													commentsDiv = commentsDiv + '<div class="phenom clearfix"style="width:300px;"> <div class="creator member js-show-mem-menu">'
													+'<span class="member-initials" title="'+cmtedUserName+'">'+cmtInitials+'</span>'
													+'</div>'
													+'<div class="phenom-desc"> '
															+'<a class="inline-object js-show-mem-menu" >'+cmtedUserName+'</a> '
															+'<div class="action-comment" style="width:280px;">'
															+'<div class="current-comment" style="overflow:auto;">'
																+'<p>'+cmtsWithBr +'</p>'
															+'</div>'+exactComment
															//+'<textarea class="js-text">'+comtObj.comments +'</textarea> ' 
															+'</div>'
													+'</div>'
													+videoDiv
													+'<p class="phenom-meta quiet">'
														+'<span title="'+date+' at '+timee+'" class="date js-hide-on-sending" dt="'+date+' '+timee+'">'+date+' at '+timee+' '+editedNotes+'</span>'
														+'<span id="'+comtObj.cId+'" class="js-hide-on-sending">';
															//if(comtObj.userId == userId){
													//*if(comtObj.cId==commentId){
															commentsDiv = commentsDiv +'- <a  class="js-edit-cmt-action" style="color: #939393;font-weight: normal;"></a>'
																+' <a  class="js-delete-cmt" style="color: #525252;font-weight: normal;">Delete</a>'	;
															//*}else{
															//*	commentsDiv = commentsDiv +'- <a  class="js-edit-cmt-action" style="color: #939393;font-weight: normal;"></a>'
															//*	+' <a id="preventDelete"  class="js-no-delete-cmt" disabled="disabled" style="color: #DFDFDF;font-weight: normal;">Action</a>'	;
															//*}
															
															commentsDiv = commentsDiv+'<a  class="js-report-cmt" id="'+comtObj.cId+'" onClick="javascript:reportComment(this.id)" style="color: #939393;font-weight: normal;"></a>'  +'</span>'
													+'</p>'
												+'</div>';	
													}
										}
									}
									
									if(listType=='crowd' || listType=='music' && fromCrowd=='True')
									{
										$("#moreActions").children('.modal-body').children('.span9').children('.window-module').children('.list-actions').append(crowdCmdDefaultDiv);
										
										if(crowdCmdDefaultDiv!='')
										{
											$("#moreActions").children('.modal-body').children('.span9').children('.window-module').children('.list-actions').after('<div class="phenom-bold"></div>');
										}
										$("#moreActions").children('.modal-body').children('.span9').children('.window-module').children('.list-actions-crd').append(commentsDiv);
									}
									else
									{
										$("#moreActions").children('.modal-body').children('.span9').children('.window-module').children('.list-actions').append(commentsDiv);
									}
										
									if(linkArray.length > 0 && linkArray[0]!=null){
										for(var i =0; i< linkArray.length;i++ ){
											var playId= linkArray[i].split("~");
											onYouTubeIframeAPIReady(playId[1].trim(),playId[0].trim()+playId[1].trim());
										}
									}
								},
								error : function() 
								{
									alert("Please try again later");
									console.log("<-------error returned for new ajax request note comments -------> ");
								}
							});
						
						}
						//end
						if(listType!='schedule')
						{
						
						if(vote.length > 0 && vote[0] != null){
							viteWithUser=vote.length +' votes';
							if(vote.indexOf(userId) >= 0){
								viteWithUser=viteWithUser +'(with you)';
								voteDivClass = 'js-un-vote';
							}	
						}else{
							viteWithUser='0 votes';
						}
						if(description!=null && description.trim()!=''){
							var array = new Array();
							array = description.split("<br>");
								if(array.length > 0 && array[0]!=null ){
									description ='';
									for(var i =0; i< array.length;i++ ){
										description = description +  array[i] + "<br>\n";   
									}
								} 
							desc='<div class="badge voted icon-sm decs" title="This note has a description."><span class="icon-pencil"></span><span class="badge-text"></span></div>';
						}
						//if(listOwnerFlag){
							if(data['dueDate'] != "")
								dueDate = '';//loadDueDateContent(data['dueDate'],data['dueTime']);
						//}
				
				$("#moreActions").children('.modal-header').find('.js-composer').remove();							
				$("#moreActions").children('.modal-header').children('.js-header').remove();
				$("#moreActions").children('.modal-body').children('.span9').children('.clearfix').remove();
				$("#moreActions").children('.modal-body').children('.span9').children('.hide-on-edit').remove();
				$("#moreActions").children('.modal-body').children('.span9').children('.clearfix').remove();
				$("#moreActions").children('.modal-body').children('.span9').children('.js-scheduleEdit').remove();
				var title='<div class="js-header"><div class="window-header clearfix" ><span class="window-header-icon icon-th-large"></span><h3 id="'+noteOwnerId+'"';
				if(!listOwnerFlag){
					/*
					title= title+' class="" title = "Posted by '+listOwnerFullName+'">'+noteName+'</h3> </div>';
				}else{
					*/
					title= title+' class="js-edit-notes-name" style="cursor: pointer;" title="Click to edit">'+noteName+'</h3> </div>';
				}
					
				title = title +'</div>';	
				$("#moreActions").children('.modal-header').append(title);
				
				var voteView="";
				if(listType=='crowd'){
					voteView='<a class="badge vote-badge clickable voted icon-sm btn dropdown-toggle " data-toggle="dropdown"><span class="icon-thumbs-up"></span><span class="badge-text">&nbsp;&nbsp;'+viteWithUser+'</span></a>';
				}else{
					voteView="";
				}
				
				$("#moreActions").children('.modal-body').children('.span9').append('<div class="badges clearfix btn-group-vote"> '
						/*We didn't separate notes comments and crowd comments use below line other wise we know need to use these lines. by Venu*/
						/*starting */
						+voteView
						+'<div class="badge voted icon-sm cmts" title="This note has '+comments.length +' comment(s)."><span class="icon-comment"></span><span class="badge-text">&nbsp;&nbsp;'+comments.length+'</span></div>'
						+desc
						+attachDiv
						//+tagss
						+dueDate
						/*Ending*/
						
				+'</div>'); 
				
				
				var descEditDivForUserAlone ='';
					if(description!= ''){
						//*if(reportLevel=='note'){
						descEditDivForUserAlone	= '&nbsp;&nbsp;<div class="action-comment" title="Note Description" style="width:280px;"><div class="current-comment" style="overflow:auto;"><p class="p-decs-test-align">' +description  +'</p></div></div>'
							+'<p><span class="span1"></span>&nbsp;&nbsp;<span><a  title="Click to Edit the Description" class="js-note-delet-desc" style="color: #595959;font-weight: normal;">Delete</a></span></p>';
						//*}else{
						//*	descEditDivForUserAlone	= '&nbsp;&nbsp;<div class="action-comment" title="Note Description" style="width:280px;"><div class="current-comment" style="overflow:auto;"><p class="p-decs-test-align">' +description  +'</p></div></div>'
						//*	+'<p><span class="span1"></span>&nbsp;&nbsp;<span><a  title="" class="js-no-delet-descr" style="color: #BCBCBC;font-weight: normal;">Action</a>-<a class="js-note-edit-desc1" style="color: #939393;font-weight: normal;" title="Click to Edit the Description">Edit</a></span></p>';

						//*}
					}else{
						descEditDivForUserAlone ='';
					}
				
				//description 
				$("#moreActions").children('.modal-body').children('.span9').append('<div class="hide-on-edit">'
						+descEditDivForUserAlone
					 +'</div>');
				
				//activity
					var comtDivInActivity = '';
					if(listOwnerFlag || listType =='crowd'|| noteLevel == 'edit' ){
						comtDivInActivity = '<div class="new-comment">'
							+'<div class="member"><span class="member-initials" title="'+userFulName+'">'+userInitial+'</span></div>'
							+'<textarea class="new-comment-input js-cmt-placeholder fontStyle" placeholder="Add comments, links and text "></textarea>'
						+'</div>'
						;
					}
					
					if(listType=='crowd' || listType=='music' && fromCrowd=='True')
					{
						$("#moreActions").children('.modal-body').children('.span9').append('<div class="window-module clearfix" style="">'
								+'<div class="window-module-title no-divider"><span class="window-module-title-icon icon-tasks"></span><h4>Activity</h4></div>'
								+'<div class="list-actions">'
								+'</div>'
								+comtDivInActivity
								+'<div class="list-actions-crd">'
								+'</div>');
					}
					else
					{
						$("#moreActions").children('.modal-body').children('.span9').append('<div class="window-module clearfix" style="">'
								+'<div class="window-module-title no-divider"><span class="window-module-title-icon icon-tasks"></span><h4>Activity</h4></div>'
								+comtDivInActivity
								+'<div class="list-actions">'
								+'</div>'
								+'</div>');
					}
				
					}
						
					}
					
					if(listType!='schedule' && listType!='crowd'){
						$("#tagFilterDiv").show();
						
					}else{
						$("#tagFilterDiv").hide();
					}
				
				if(listType!='schedule'){
					// side bar 
					// members
					$("#moreActions").children('.modal-body').children('.span3').children().remove();
					var sideActionDiv  = '';
					var crowdvalues ='';
					
					//listType='crowd';
					//vote
					//This if Condition used for vote functionality need to hide in notes and Memo page. (by Venu)
					if(listType!=null && listType!='music' && listType!='bill'){
						//blocked delete option condition for deleted note 
						if(noteDeleteAccess=="true")
						{
						}
						else
						{
						/*voteArchieve= voteArchieve+'<div class="clearfix">';
					
						voteArchieve=	voteArchieve+'<div class="voted">'
							+'<a class="'+voteDivClass+' button-link is-on side" style="text-decoration: none;">'
								+'<span class="icon-thumbs-up"></span>'
							+'&nbsp;Vote';
							if(vote.indexOf(userId) >= 0){
								voteArchieve = voteArchieve +'<span class="on">'
									+'<span class="icon-ok"></span>'
								+'</span>';
								}

							voteArchieve = voteArchieve +'</a>'
						+'</div>';*/
							voteArchieve= voteArchieve+'<div class="clearfix">';
							//here report level is note
							//if(commentId=='note'){
							voteArchieve=	voteArchieve+'<a class="button-link js-delete-report-note" title="report" style="text-decoration: none;">'
									+'<span class="icon-trash"></span>'
								+'&nbsp;&nbsp;Delete';
						}
							//}else{
							//	voteArchieve=	voteArchieve+'<a class="deleteButton js-no-delete-report-note" title="report" style="text-decoration: none;">'
							//	+'<font color="#cecece"><span class="icon-trash" style="font-size: 44px;"></span>'
							//+'&nbsp;&nbsp;Action</font>';
							//}
								voteArchieve = voteArchieve +'</a>'
							+'</div>';
								voteArchieve= voteArchieve+'<div class="clearfix">';
								voteArchieve=	voteArchieve+'<a class="button-link js-resolve-report-note" title="report" style="text-decoration: none;">'
										+'<span class="icon-ok-sign"></span>'
									+'&nbsp;&nbsp;Action';
									voteArchieve = voteArchieve +'</a>'
								+'</div>';
					}
							
						if(parentDivName == "notes" ){
							voteArchieve = voteArchieve + '</div>';
						}
					//Actions	
					if(listOwnerFlag ||listType == 'crowd' || noteLevel == 'edit'){
						sideActionDiv = sideActionDiv +'<div class="window-module other-actions clearfix">'
						
						
						
							sideActionDiv = sideActionDiv +'<div class="clearfix">';
							
						$("#moreActions").children('.modal-body').children('.span3').find('.window-module').each(function( index ){
							$(this).remove();
						});
				
						if(listType!='crowd' && (listOwnerFlag || noteLevel == 'edit')){
							//Tag
							var tags=new Array();
							var tag ='';
							var remainderWithParam='';
							if(data!=null && data!=''){
								tag=data['tag'];
								remainderWithParam=data['remainders'];
								if(tag!=null && tag!=''){
									tag=tag.replace("[","");
									tag=tag.replace("]","");
									//var n=tag.indexOf(",");
									
										tags=tag.split(',');
									
								}
								
								
								var url = urlForServer+"note/getTags";
								var params = '{"userId":"'+userId+'"}';
								params = encodeURIComponent(params);
					
								$.ajax({
									 headers: {
										 "Mn-Callers" : musicnote,
									    	"Mn-time" :musicnoteIn	
									 },
									type: 'POST',
									url : url,
									cache: false,
									contentType: "application/json; charset=utf-8",
									data:params, 
									dataType: "json",
									success : function(response){/*
										sideActionDiv=sideActionDiv+'<div class="btn-group" style="min-width:105px;max-width: 250px;display: block;">'
										+'<a class="btn dropdown-toggle button-link" data-toggle="dropdown" title="Tag the note" style="min-width:105px;font-size: 13px;text-align: left;">'
										+'<span class="icon-tags"></span> &nbsp;Tag&nbsp;<span class="caret" style="right: 10%; position: absolute;"></span>'
										+'</a>'
										+'<ul class="dropdown-menu pull-right dropDownStopProgress">';

										if(response!=null && response!=''){
											for ( var i = 0; i < response.length; i++) 
											{
												sideActionDiv=sideActionDiv+'<li><a id="'+response[i].tagId+'">';

												for(var j=0;j<tags.length;j++){
													if(response[i].tagId.trim()==tags[j].trim()){
														sideActionDiv=sideActionDiv+'<i class="icon-ok pull-right"></i>';
													}
												}
												sideActionDiv=sideActionDiv+'<i class="icon-edit tagIconEdit" id="'+response[i].tagId+'" title="edit"></i> &nbsp; <i class="icon-trash tagIconDelete" title="delete" id="'+response[i].tagId+'"></i> &nbsp;<i class="noteToTag" id="'+response[i].tagId+'" title="' + response[i].tagName +'">' + response[i].tagName +'</i></a></li>';
											}
											sideActionDiv=sideActionDiv+'<li class="divider"></li>';
										}
										sideActionDiv=sideActionDiv+'<li><a class="createTag" id="createTag"><i class="icon-plus createTag"></i>&nbsp;&nbsp; Create New Tag</a></li>'
										+'</ul>'
										+'</div>';
										*/
										// remainders
					
										if(remainderWithParam.indexOf("[") != -1){
											remainderWithParam = remainderWithParam.substring(remainderWithParam.indexOf("[")+1,remainderWithParam.length);
										}
										if(remainderWithParam.indexOf("]") != -1){
											remainderWithParam = remainderWithParam.substring(0,remainderWithParam.indexOf("]"));
										}
										if(remainderWithParam.indexOf(",") != -1){
											remainder = remainderWithParam.split(',');
										}else{
											if(remainderWithParam.trim() != ''){
												remainder = remainderWithParam.split(',');
											}
										}
										
										if(remainder.length > 0 && remainder[0]!=''){ 
											var remObj  = remindersMap[parseInt(remainder[0].trim())]; 
											var styleAttr = "";
											var span="";
											if(remObj!= undefined){
												if(remObj.inactiveRemainderUserId!=null){
													var inactiveRemainderUserIds =new Array();
													var inactiveRemainderUserIdsWithParam=remObj.inactiveRemainderUserId;
													if(inactiveRemainderUserIdsWithParam.indexOf("[") != -1){
														inactiveRemainderUserIdsWithParam = inactiveRemainderUserIdsWithParam.substring(inactiveRemainderUserIdsWithParam.indexOf("[")+1,inactiveRemainderUserIdsWithParam.length);
													}
													if(inactiveRemainderUserIdsWithParam.indexOf("]") != -1){
														inactiveRemainderUserIdsWithParam = inactiveRemainderUserIdsWithParam.substring(0,inactiveRemainderUserIdsWithParam.indexOf("]"));
													}
													if(inactiveRemainderUserIdsWithParam.indexOf(",") != -1){
														inactiveRemainderUserIds = inactiveRemainderUserIdsWithParam.split(',');
													}else{
														if(inactiveRemainderUserIdsWithParam.trim() != ''){
															inactiveRemainderUserIds = inactiveRemainderUserIdsWithParam.split(',');
														}
													}
													if(inactiveRemainderUserIds.length > 0 && inactiveRemainderUserIds[0] != ''){
														if(inactiveRemainderUserIds.indexOf(userId) == -1){
															//active
															styleAttr ='text-decoration: none;background:#DFDD0A ;';
																//background: linear-gradient(to bottom, #DFDD0A 100%, #F3F3F3 0%) repeat scroll 0 0 transparent;';
															span='style="color:black;"';
														}else{
															//inactive
															span='style="color:white;"';
															styleAttr ='text-decoration: none;background:#A888A3 ;';
																//background: linear-gradient(to bottom, #A888A3 100%, #F3F3F3 0%) repeat scroll 0 0 transparent;';
														}
													}else{
														//active
														span='style="color:black;"';
														styleAttr ='text-decoration: none;background:#DFDD0A ;';//background: linear-gradient(to bottom, #DFDD0A 100%, #F3F3F3 0%) repeat scroll 0 0 transparent;';
													}
												}	
											}else{
												//inactive
												span='style="color:white;"';
												styleAttr ='text-decoration: none;background:#DFDD0A ;';//background: linear-gradient(to bottom, #A888A3 100%, #F3F3F3 0%) repeat scroll 0 0 transparent;';
											}
											/*sideActionDiv = sideActionDiv 
											+'<a id="'+remainder[0]+'" class="button-link remaindIconEdit" title="Reminder" style="'+styleAttr+'">'
											+'<span class="icon-time" '+span+'></span> &nbsp;Reminder&nbsp;'
											+'</a>'
											+'</div>';*/
										}else{
											/*sideActionDiv = sideActionDiv 
											+'<a class="button-link addRemainders" title="Reminder" style="text-decoration: none;">'
											+'<span class="icon-time"></span> &nbsp;Reminder&nbsp;'
											+'</a>'
											+'</div>';*/
										}
										
										sideActionDiv=sideActionDiv+crowdvalues+voteArchieve;
													
										$("#moreActions").children('.modal-body').children('.span3').append(sideActionDiv);	

									},
									error: function(e) {
										alert("Please try again later");
									}
								});
							}
						}else{
							sideActionDiv=sideActionDiv+'</div>'+crowdvalues+voteArchieve;
							$("#moreActions").children('.modal-body').children('.span3').append(sideActionDiv);
						}
					}	
				}else{
					
					if(listOwnerFlag){
						var shareAllContact='';
						
						if(listOwnerUserId == userId){
							if(noteSharedAllContact==0){
								shareAllContact='<li><a class="js-members-contact" role="button" href="javascript:void(0);" data-toggle="modal" style="text-decoration: none;">With All Contacts</a></li>';
							}else if(noteSharedAllContact==1){
								shareAllContact='<li><a class="js-members-delcontact" role="button" href="javascript:void(0);" data-toggle="modal" style="text-decoration: none;">Shared Contact</a></li>';
							}
						}else{
							if(noteSharedAllContactMembersIds.indexOf(userId) != -1){
								shareAllContact='<li><a class="js-members-delcontact" role="button" href="javascript:void(0);" data-toggle="modal" style="text-decoration: none;">Shared Contact</a></li>';
							}else{
								shareAllContact='<li><a class="js-members-contact" role="button" href="javascript:void(0);" data-toggle="modal" style="text-decoration: none;">With All Contacts</a></li>';
							}
						}
						
						sideActionDiv ='<div class="window-module clearfix">'
							
							+'<div class="card-detail-members hide clearfix"></div>'
							+'<div class="btn-group">'
							+'<a class="btn dropdown-toggle button-link" data-toggle="dropdown" title="Add or remove members of the Book from the Note" style="min-width:105px;">'
							+'<span class="icon-user"></span>Share Event&nbsp;&nbsp;<span class="caret"></span>'
							+'</a>'
							+'<ul class="dropdown-menu pull-right">'
							+'<li><a class="js-members" role="button" href="#" data-toggle="modal" style="text-decoration: none;">With Individual</a></li>'
							+'<li><a class="js-members-group" role="button" href="#" data-toggle="modal" style="text-decoration: none;">With Group</a></li>'
							//+'<li><a class="js-members-contact" role="button" href="javascript:void(0);" data-toggle="modal" style="text-decoration: none;">Share Contacts</a></li>'
							+shareAllContact
							+'</ul>'
							+'</div>'
							
							+'</div>';
						
						
						/*sideActionDiv = sideActionDiv +'<a class="button-link js-archive" title="Delete the note from the book" style="text-decoration: none;">'
						+'<span class="icon-remove"></span>'
						+'&nbsp;Delete'
					+'</a>';*/
						$("#moreActions").children('.modal-body').children('.span3').children('.window-module').remove();
						$("#moreActions").children('.modal-body').children('.span3').append(sideActionDiv);
					}
					else  // event accept ,decline ,later
					{
						if(data['sharingdetails']!=null && data['sharingdetails']!="")
						{
							sideActionDiv='';
							if(data['sharingdetails']=="P")
							{
								sideActionDiv ='<div class="window-module clearfix">'
									+'<h3>Share</h3>'
									+'<div class="card-detail-members hide clearfix"></div>'
									+'<div class="btn-group">'
									+'<a class="btn dropdown-toggle button-link" data-toggle="dropdown" title="Accept or Decline Events." style="min-width:105px;">'
									+'<span class="icon-user"></span> &nbsp;Request&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="caret"></span>'
									+'</a>'
									+'<ul class="dropdown-menu pull-right">'
									+'<li><a class="js-acceptEvent" role="button" href="#" data-toggle="modal" style="text-decoration: none;">Accept</a></li>'
									+'<li><a class="js-declineEvent" role="button" href="#" data-toggle="modal" style="text-decoration: none;">Decline</a></li>'
									//+'<li><a class="js-laterEvent" role="button" href="#" data-toggle="modal" style="text-decoration: none;">Later</a></li>'
									+'</ul>'
									+'</div>'
									
									+'</div>';	
								
							}
							
							/*sideActionDiv = sideActionDiv +'<a class="button-link js-archive" title="Delete the note from the book" style="text-decoration: none;">'
							+'<span class="icon-remove"></span>'
							+'&nbsp;Delete'
						+'</a>';*/
							
//							else if(data['sharingdetails']=="A")
//							{
//								sideActionDiv ='<div class="window-module clearfix">'
//									+'<h3>Share</h3>'
//									+'<div class="card-detail-members hide clearfix"></div>'
//									+'<div class="btn-group">'
//									+'<a class="btn dropdown-toggle button-link" data-toggle="dropdown" title="Accept or Decline Events." style="min-width:105px;">'
//									+'<span class="icon-user"></span> &nbsp;Accepted&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
//									+'</a></div></div>';	
//							}
							
							$("#moreActions").children('.modal-body').children('.span3').children('.window-module').remove();
							$("#moreActions").children('.modal-body').children('.span3').append(sideActionDiv);
						}
						
						
					}
				}
				
				if(listType!='schedule'){
					
					$("#moreActions").attr("style",'top: 5%; overflow: auto;right: 20%;left:20%;width:50%;height:450px; display: block;');
				}else{
					$("#moreActions").attr("style",'top: 10%; min-width: 560px; min-height: 280px; display: block;');
				}
				
				$(".new-comment").attr("style",'top: 10%; min-width: 560px; min-height: 280px; display: none;');
				$("#moreActions").modal('show');	
				if(!listOwnerFlag){
					$("#moreActions").children('.modal-header').children('.window-header').attr("title",'Posted by '+listOwnerFullName);
				}
			},
			error: function(e) {
				alert("Please try again later");
			}

		});
		
		
		/*setTimeout(function(){
			//alert(tagsArray.length);
		$('#tagFilterUl').tagit( {
			tagSource :tagFilter,
			triggerKeys : [ 'comma','enter','tab' ],
			placeholderText: 'Tags',
			select: true,
			initialTags:tagsArray,
		    tagsChanged:function (a, b) {
			 var newTagName=a.trim();
			 var tagId=mapTagNotes[newTagName.toLowerCase()];
		         if(b=='added' ){
		        	 if(tagId==undefined){
		        		 var url = urlForServer+"note/createTag";
			        		if(newTagName!=null && newTagName!=""){
			                var criteria=null;
			        		var params = '{"userId":'+userId+',"tagName":"'+newTagName+'","listId":"'+listIdFromList+'","noteId":"'+noteIdFromList+'","listType":"'+listType+'"}';
			        		params = encodeURIComponent(params);
			        		$.ajax({
			        			 headers: { "Ajax-Call" : userTokens, "Ajax-Time" :userLoginTime },
			        			type: 'POST',
			        			url : url,
			        			cache: false,
			        			contentType: "application/json; charset=utf-8",
			                
			        			data:params, 
			        			dataType: "json",
			        			success: function(transport) {
			        				if(transport['status']=="0"){
			        					if(transport['tag'].contains(',')){
			        						var orignalCount=(newTagName.split(",").length - 1);
			        						var duplicateCount=(transport['tag'].split(",").length - 1);
			        						if(orignalCount == duplicateCount)
			        							$("#errormsg").text("!!! "+transport['tag']+" are already exists").css({"color":"red","font-weight": "bold"});
			        						else
			        							$("#errormsg").text("!!! "+transport['tag']+" are already exists, Other Tags are added successfully.. ").css({"color":"red","font-weight": "bold"});
			        					}else{
			        						var orignalCount=(newTagName.split(",").length - 1);
			        						var duplicateCount=(transport['tag'].split(",").length - 1);
			        						if(orignalCount == duplicateCount)
			        							$("#errormsg").text("!!! "+transport['tag']+" is already exists").css({"color":"red","font-weight": "bold"});
			        						else
			        							$("#errormsg").text("!!! "+transport['tag']+" is already exists, Other Tags are added successfully.. ").css({"color":"red","font-weight": "bold"});
			        						
			        					}
			        					loadTags();
			        					//reLoadList();
			        				}else{
			        					loadTags();
			        					//reLoadList();
			        				}

			                	}
			        		});
			        		if(listType!='schedule'){
			        		loadTagsForCrowd();
			        		}
			        		}else{
			        			$("#tagWarningId").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
			        			$("#tagWarningId").text("Field cannot be blank").css({"color":"red"});
			        		}
			        		return false;
		        	 }else{

		        			var url = urlForServer+"note/noteToTag";
		        			var params = '{"listId":"'+listIdFromList+'","noteId":"'+noteIdFromList+'","userId":"'+userId+'","listType":"'+listType+'","tagId":"'+tagId+'"}';
		        		    params = encodeURIComponent(params);
		        		    var count;
		        		    $.ajax({
		        		    	 headers: { "Ajax-Call" : userTokens, "Ajax-Time" :userLoginTime },
		        		    	type: 'POST',
		        		    	url : url,
		        		    	cache: false,
		        		    	contentType: "application/json; charset=utf-8",
		        		    	data:params, 
		        		    	dataType: "json",
		        		    	success : function(response){
		        		    		if(response['status']=='A'){
		        		    			loadTags();
			        					//reLoadList();
		        		    		}else if(response['status']=='R') {
		        		    			loadTags();
			        					//reLoadList();
		        		    		}	
		        		    	},
		        	            error: function(e) {
		        	                alert("Please try again later");
		        	            }
		        		    });
		        	 }
		        	} else if(b=='popped'){
		    			var url = urlForServer+"note/noteToTag";
		    			var params = '{"listId":"'+listIdFromList+'","noteId":"'+noteIdFromList+'","userId":"'+userId+'","listType":"'+listType+'","tagId":"'+tagId+'"}';
		    		    params = encodeURIComponent(params);
		    		    var count;
		    		    $.ajax({
		    		    	 headers: { "Ajax-Call" : userTokens, "Ajax-Time" :userLoginTime },
		    		    	type: 'POST',
		    		    	url : url,
		    		    	cache: false,
		    		    	contentType: "application/json; charset=utf-8",
		    		    	data:params, 
		    		    	dataType: "json",
		    		    	success : function(response){
		    		    		if(response['status']=='A'){
		    		    			loadTags();
		        					//reLoadList();
		    		    		}else if(response['status']=='R') {
		    		    			loadTags();
		        					//reLoadList();
		    		    		}	
		    		    	},
		    	            error: function(e) {
		    	                alert("Please try again later");
		    	            }
		    		    });
		        	}
		}
		});
		},700);*/
		
		
	}
	function loadTagsForCrowd(){
		
		var url = urlForServer + "note/getAllTags";
		$.ajax({
			headers: { 
				"Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn				
	    	},
			type : 'POST',
			url : url,
			cache : false,
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			success : function(response) {
				if (response != null && response != '') {
					for ( var i = 0; i < response.length; i++) {
						crowdTagMap[response[i].tagId]=response[i].tagName;
					}
				}
			}
		});
	}
	function createListForVote(){
		var userNameForDiv ;
		var userInitialForDiv;
		var userIDForDiv;
		var castButtonClass= 'js-vote btn btn-success';
		var castedVote ='Vote';
		var contentDiv='';//'<li><center><a class="btn btn-success" > Voters</a></center></li><li class="divider"></li>';
			if(vote.length > 0 && vote[0] != null){
				for (i=0;i<vote.length;i++){
					userNameForDiv = '';
					if(userId == vote[i]){
						userNameForDiv = userFulName;
						userInitialForDiv=userInitial;
						userIDForDiv = userId;
						castedVote ='Voted';
						castButtonClass= 'js-un-vote btn btn-danger';
					}else{
						var userObje = activeUserObjectMap[vote[i]];
						userNameForDiv = userObje['userFirstName']+userObje['userLastName'];
						userInitialForDiv=(userObje['userFirstName']).substring(0,1).toUpperCase()+(userObje['userLastName']).substring(0,1).toUpperCase();
						userIDForDiv = userObje['userId'];
					}
					contentDiv = contentDiv +'<li class="voter '+userIDForDiv+'"> '
								+'<div class="member">'
									+'<span class="member-initials" title="'+userNameForDiv+'">'+userInitialForDiv+'</span>'
								+'</div>'
								+'<p class="popover-user-name title">'+userNameForDiv+'</p>'
							+'</li>';
				}
			}
			//This if Condition used for vote functionality need to hide in notes and Memo page. (by Venu)
			if(listType!=null && listType!='music' && listType!='bill'){				
				contentDiv = contentDiv	+'<li class="divider"></li>'
						+'<li class="vote-btns"><center><a class="'+castButtonClass+'">'+castedVote+'</a></center></li>';
			}
		return contentDiv;				
		}
		function convertTwentyFourHourToTwelveHourTime(times)
		{
			var timing=times.split(":");
			if(timing[1].length==1)
				timing[1]="0"+timing[1];
			if(parseInt(timing[0])==12)
			{
				cHour=parseInt(timing[0]);
				cMinutes=timing[1]+" PM";
			}
			else if(parseInt(timing[0])>12)
			{
				cHour=parseInt(timing[0])-12;
				cMinutes=timing[1]+" PM";
			} else if(parseInt(timing[0])==0){
				cHour=parseInt(timing[0])+12;
				cMinutes=timing[1]+" AM";
			}
			else{
				cHour=timing[0];
				cMinutes=timing[1]+" AM";
			}
			return cHour+":"+cMinutes;
		}
			function fetchUsersList(){
			//var userDetailsJsonObj;
			var datastr = '{"userId":"'+userId+'"}';
			var params = encodeURIComponent(datastr);
			var url = urlForServer+"user/getAllActiveUsersListForSearch";
			
			$.ajax({
				 headers: { 
					 "Mn-Callers" : musicnote,
				    	"Mn-time" :musicnoteIn	
				 },
				type : 'POST',
				url : url, data : params,
				success : function(responseText) {
				var data = jQuery.parseJSON(responseText);
				var dataTextObj;
				 if(data!=null && data!='')
					{
					
						dataTextObj="[";
						for ( var i = 0; i < data.length; i++) {
							var image="";
							userDetailsJsonObj = data[i];
							if(userDetailsJsonObj['filePath']!=null && userDetailsJsonObj['filePath']!="")
								image=userDetailsJsonObj['filePath'];
							else
								image="pics/dummy.jpg";
							
							activeUserDeatilsMap[userDetailsJsonObj['userId']]=userDetailsJsonObj['userFirstName']+' '+userDetailsJsonObj['userLastName'];
							activeUserObjectMap[userDetailsJsonObj['userId']] = userDetailsJsonObj;

							//friendsPhotoMap[userDetailsJsonObj['userId']]=userDetailsJsonObj['filePath'];
							dataTextObj = dataTextObj + "{\"userId\" : \"" + userDetailsJsonObj['userId'] + "\","
							+ "\"userName\" : \"" + userDetailsJsonObj['userFirstName']+' '+userDetailsJsonObj['userLastName'] + "\","
							+ "\"uniquserName\" : \"" + userDetailsJsonObj['userName']+"\","
							+ "\"emailId\" : \"" + userDetailsJsonObj['emailId']+"\","
							+ "\"image\" : \""+image+"\"},";
							 
						}
						if(dataTextObj.lastIndexOf(",")!= -1 && dataTextObj.lastIndexOf(",") !=0 ){
							dataTextObj = dataTextObj.substring(0, dataTextObj.lastIndexOf(","));
						}
						dataTextObj = dataTextObj +"]";
							
					}	
				userDetails = jQuery.parseJSON(dataTextObj);
				
			},
			error : function() {
				console.log("<-------error returned for User details -------> ");
				}
			});   
		}
			function getattachedFilesForNotes(listId,noteId,listOwnerFlag,showFlag){
				var url = urlForServer+"note/getAttachFile/"+listId+"/"+noteId;
				var response="";	
						$.ajax({
							headers: { 
								"Mn-Callers" : musicnote,
						    	"Mn-time" :musicnoteIn					
					    	},
							type : 'POST',
							url : url,
							success : function(responseText) 
							{
								response = responseText;
							
								if(response.indexOf("[") != -1){
									response = response.substring(response.indexOf("[")+1,response.length);
								}
								if(response.indexOf("]") != -1){
									response = response.substring(0,response.indexOf("]"));
								}
								
								if(response.trim() !='' ){
									selectedFiles = response.split(',');
								}else{
									selectedFiles = new Array();
								}
								/*if(!listOwnerFlag && selectedFiles.length == 0 ){
									$("#moveCopyWarning").modal('show');
									$("#move-msg-header-span1").attr('class',"label label-info").text("Warning");
									$("#move-modal-message1").attr('class',"label label-info").text("  No Files attached");
									setTimeout(function(){ $('#moveCopyWarning').modal('hide');},4000);
									
								}else{*/
									if(listOwnerFlag){
										recordedLessons(selectedFiles,listOwnerFlag);
										otherCheckFiles(selectedFiles,listOwnerFlag,listId,noteId);
										otherFiles(selectedFiles,listOwnerFlag);
										attachCountId = selectedFiles.length;
										if(showFlag){
											$('#attachFileModel').modal('show');
										}
									}else{
										if(sharedNoteFlag){
											recordedLessons(selectedFiles,true);
											otherCheckFiles(selectedFiles,true,listId,noteId);
											otherFiles(selectedFiles,true);
											attachCountId = selectedFiles.length;
											if(showFlag){
												$('#attachFileModel').modal('show');
											}
										}
									}
								//}
							},
							error : function() 
							{
								alert("Please try again later");
								console.log("<-------error returned for new ajax request attach file-------> ");
							}
						});
						return selectedFiles;
					}
	
function getFileFor(fileName,listOwenrUserId,tempfileName){
				var filename;
				filename=fileName.toString().split(".");
				var	ext = filename[1];
				var href=urlForServer+"Lesson/download/"+listOwenrUserId.trim()+'/'+fileName+'/'+ext+"/"+tempfileName+"/"+musicnote;
			    $.fileDownload(href, {
				   httpMethod: "POST"
			   });  	
			}
