var userDetailsJsonObj;
var userName="";
var userId;
var userFulName="";
var userFullNameMail='';
var userFirstName='';
var userLastName='';
var userInitial='';
var userEmail='';
var timeZoneList='';
var timeZoneSelect='';
var userAdminId='';
var userAdminFullName='';
var userAdminInitial='';
$(document).ready(function(){

	
	var id="";
	//for chrome refresh function
	if(is_chrome || is_safari){
	userId=$.jStorage.get("lkeyA");
	userTokens=$.jStorage.get("tkeyB");
	userLoginTime=$.jStorage.get("ltkeyC");
	musicnote=$.jStorage.get("tkeyB");
	musicnoteIn=$.jStorage.get("ltkeyC");
	fetchUsers();
	fetchUsersList();
	
	  if(userId !=null && userId !="") {
		    $('#loginContainer').attr('style','display:none');
			$('#indexContainer').attr('style','display:block');
			$("#selectEvent").val('0');
	  }
	  else
		  {
		  	$('#loginContainer').attr('style','display:block');
			$('#indexContainer').attr('style','display:none');
	  }
	}
	//for firefox refresh function
	else{
	id=document.cookie;
	var res = id.split("~"); 
	userId=res[0];
	userTokens=res[1];
	userLoginTime=res[2];
	musicnote=res[1];
	musicnoteIn=res[2];
	fetchUsers();
	fetchUsersList();
	  if(id !=null && id !="" && id.indexOf("~") > -1) {
		    $('#loginContainer').attr('style','display:none');
			$('#indexContainer').attr('style','display:block');
			$("#selectEvent").val('0');
	  }
	  else
		  {
		  	$('#loginContainer').attr('style','display:block');
			$('#indexContainer').attr('style','display:none');
	  }
	}
	$('#adminForm').submit(function(e){
		
		e.preventDefault();
		var url=urlForServer +'login/userlogin';
		var userName=$('#adminUserName').val();
		var password=$('#adminPassword').val();
		
		if((userName=="" ||userName==undefined || password=="" || password==undefined)){
			$('#errorMessage1').attr('style','display:none');
			$('#errorMessage').attr('style','display:block;color:red;margin-bottom:10px;margin-left:30px;');
			return false;
		}
		/*else if(mailValidate(userName.trim())==false){
			$('#errorMessage').attr('style','display:none');
			$('#errorMessage1').attr('style','display:block;color:red;margin-bottom:10px;margin-left:30px;');
			return false;
		}*/
		else{
			$('#errorMessage').attr('style','display:none');
			$('#errorMessage1').attr('style','display:none');
		}	
		$.support.cors = true;
		var params='{"loginId":"'+escape(userName)+'","password":"'+escape(password)+'"}';
		var musicnoteAn=$.base64.encode(url);
		
		$.ajax({
				headers: { 
					//"Ajax-Call" : authentications	
					"Mn-Callers" : musicnoteAn			
				},
		    	type: 'POST',
		    	url : url,
		    	cache: false,
		    	data:params,
		    	contentType: "application/json; charset=utf-8",
		    	//dataType: "json",
		    	success : function(response){
					
			 	var res=jQuery.parseJSON(response);
			 	if(res!=null && res!=""){
			 		
				if(res.adminFlag=='true' && res.adminFlag!=null && res.adminFlag!=""){
					
					musicnote=res.token;
					musicnoteIn=res.datess;
			 		userId=res.userId;
			 		userAdminId=res.userId;
			 		$('#adminId').val(userAdminId);
			 		fetchUsersList();
			 		fetchUsers();
					if(is_chrome || is_safari)
					{
						$.jStorage.set("lkeyA", userId);
						$.jStorage.set("tkeyB", musicnote);
						$.jStorage.set("ltkeyC", musicnoteIn);
					}
				else
				    {
				    document.cookie=userId+"~"+musicnote+"~"+musicnoteIn;
				    }
					//setTimeout(mnQurtz,3600000);
					mnQurtz();
					$('#errorMessage').attr('style','display:none');
					$('#errorMessage1').attr('style','display:none');
					$('#loginContainer').attr('style','display:none');
					$('#indexContainer').attr('style','display:block');
					
				}
				else{
					//do nothing
					$('#errorMessage').attr('style','display:block;color:red;margin-bottom:10px;margin-left:30px;');
					return false;
				}
				
			 	}else{
			 		$('#errorMessage').attr('style','display:block;color:red;margin-bottom:10px;margin-left:30px;');
					return false;	
			 	}
		    	},
		    	error: function(e) {
		    		alert("Please try again later");
		    	}
		    	});
		
		});
	
	
	
	
	//To validate mail address
	
	/*function mailValidate(str) {

		var at = "@"
		var dot = "."
		var lat = str.indexOf(at)
		var lstr = str.length
		var ldot = str.indexOf(dot)
		
		
		if (str.indexOf(at) == -1) {
		return false
		}
		if (str.indexOf(at) == -1 || str.indexOf(at) == 0 || str.indexOf(at) == lstr) {
		return false
		}
		if (str.indexOf(dot) == -1 || str.indexOf(dot) == 0 || str.indexOf(dot) == lstr) {
		return false
		}
		if (str.indexOf(at, (lat + 1)) != -1) {
		return false
		}
		if (str.substring(lat - 1, lat) == dot || str.substring(lat + 1, lat + 2) == dot) {
		return false
		}
		if (str.indexOf(dot, (lat + 2)) == -1) {
		return false
		}
		if (str.indexOf(" ") != -1) {
		return false
		}
		return true
	}	*/
	
	$("#adminLogout").click(function(){
		adminLogout();
	});
	function adminLogout(){
		 if(is_chrome || is_safari)
		 {
			 $.jStorage.set("lkeyA", "");
			 $.jStorage.set("tkeyB", "");
			 $.jStorage.set("ltkeyC", "");
		 }
		 
		 else{
		     document.cookie="";    
		 }	 
		 
		 window.location.replace("./login.html");
	}
	function fetchUsers()
	{ 
		    var url = urlForServer+"user/getUserDetails";
			var datastr = '{"userId":"'+userId+'"}';
			var params = encodeURIComponent(datastr);
			var role="";
			var userRoles=new Array();
			userRoles[0]="Teacher";
			userRoles[1]="Student";
			userRoles[2]="Administrator";
			$.ajax({
				headers: { 
					//"Ajax-Call" : userTokens,
			    	//"Ajax-Time" :userLoginTime	
					"Mn-Callers" : musicnote,
			    	"Mn-time" :musicnoteIn					
		    	},
			type : 'POST',
			url : url,
			data : params,
			success : function(responseText) {
			var data = jQuery.parseJSON(responseText);
			  if(data!=null && data!='')
				{
					for ( var i = 0; i < data.length; i++) {
						
						 userDetailsJsonObj = data[i];
						 if(data[i].securityCheck=='true'){
							 $('#securityQuestion').attr('style','display:block');
							 }else{
							 $('#securityQuestion').attr('style','display:none');
							 }

						 userName=userDetailsJsonObj['userName'];
						 displayUserName=userDetailsJsonObj['displayUserName'];
						 
						 if(userDetailsJsonObj['userFirstName']!='null'){
							 $("#userView").text(userDetailsJsonObj['userFirstName']+"  "+userDetailsJsonObj['userLastName']);
							// $("#userView").text(userName);
						 }else if(userDetailsJsonObj['displayUserName']!='null'){
							 $("#userView").text(displayUserName);
						 }else{
							 $("#userView").text(userName);
							//$("#userView").text(userDetailsJsonObj['userFirstName']+"  "+userDetailsJsonObj['userLastName']);
						 }
						 userAdminFullName = userDetailsJsonObj['userFirstName']+" "+userDetailsJsonObj['userLastName'];
						 role=userDetailsJsonObj['userRole'];
						 userId=userDetailsJsonObj['userId'];
						 var noteAccess=userDetailsJsonObj['noteCreateBasedOn'];
						 allNoteCreateBasedOn=noteAccess;
						 userFirstName=userDetailsJsonObj['userFirstName'];
						 userLastName=userDetailsJsonObj['userLastName'];
						 userFullNameMail=userDetailsJsonObj['userFirstName']+"  "+userDetailsJsonObj['userLastName'];
						 userAdminInitial=userFirstName.substring(0, 1).toUpperCase()+userLastName.substring(0, 1).toUpperCase();
						 userEmail=userDetailsJsonObj['emailId'];
						
						 
					}
		            	 
		            	 
		            	
						
							
				}
			  //$('#msgLoadingModal').modal('hide');
			
		},
		error : function() {
			console.log("<-------error returned for User details -------> ");
			}
		});   

	}

	
});