/*
 * Admin page settings
 * Deepak
 */

var PremiumUserList;
var TrialUserList;
var ExpiredUserList;
var allUserList;
var qId;
var QuestionId;
var templateId;
var adminUploadToken;
var operationType;
var addPreferenceUserId;
var userFlag;
var blogId;
var blogArray = new Array();
//var addPreferenceStatus="";
$(document).ready(function(){
	//
	$("#spinner").bind("ajaxSend", function() {
		alert('ajax esnd');
		//$("#spinner").attr('style','display:block;');
        $(this).show();
    }).bind("ajaxStop", function() {
        $(this).hide();
    }).bind("ajaxError", function() {
        $(this).hide();
    }).bind("ajaxComplete", function() {
        $(this).hide();
    });
	
	/*$('.jqte-test').jqte();*/
	//when admin uploading a file
	 $('#fileChoose').on('change', function(){
		 if(is_chrome)
			 {
			 	$("#upload-file-info").html($(this).val().replace(/C:\\fakepath\\/i, ''));
			 }
		 else
			 {
			 	$("#upload-file-info").html($(this).val());
			 }
	        
	    });
	function mailNotification(){
	var url=urlForServer +'admin/updateMailNotification';
	var params='{"status":'+true+',"checkFlag":'+true+'}';
	
	 $.ajax({ headers: { 
		 "Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn	
	 },
	    	type: 'POST',
	    	url : url,
	    	cache: false,
	    	data:params,
	    	contentType: "application/json; charset=utf-8",
	    	//dataType: "json",
	    	success : function(response){
		 if(response!=null ||response!=""){
		 if(response=='true'){
			 $('#on').attr('checked',true);
			 $('.onOff').attr('style','display:block;height:900px;');
		 }else if(response=='false'){
			 $('#off').attr('checked',true);
		 }else if(response=='ConfigurationAbsent'){
			 $('#off').attr('checked',true);
		 }
		 }
	    	},
	    	error: function(e) {
	    		
	    		alert("Please try again later");
	    	}
	    });
	
	}
	

	
	/*function registerTokenGenerator(){
		var url = urlForServer+"user/newToken";
		var musicnoteAn=$.base64.encode(url);
		$.ajax({
			
			headers:{"Mn-Callers1":musicnoteAn},

			type : 'POST',
			url : url,
			//data : params,
			success : function(responseText) {	
			if(responseText!='' && responseText!=null){
			var data=$.parseJSON(responseText);
			regMusicnote=data.token;
			regMusicnoteIn=data.datess;
			}
		},
			error:function(responsetext){
			alert('Please try again later');
		}
		});
		
		}*/
	$('#userLevels').change(function(){
		$('#noRecords').attr('style','display:none;');
		$('#errormsgs').attr('style','display:none;');
		$('#noRecordexist').attr('style','display:none;');
		filterUserDetails();
	});
	$('#userRole').change(function(){
		$('#noRecords').attr('style','display:none;');
		$('#errormsgs').attr('style','display:none;');
		$('#noRecordexist').attr('style','display:none;');
		filterUserDetails();
	});
	//for signUp report and login report
	$('#reportLevel').change(function(){
		if($('#reportLevel').val()=="loginChartReport"){
			$('#reportStartDate').val("");
			$('#reportEndDate').val("");
			viewLoginDetailsInBarChart();
		}
		$('#barchartloginContainer').attr('style','display:none');
		$('#noRecords').attr('style','display:none;');
		$('#errormsgs').attr('style','display:none;');
		$('#noRecordexist').attr('style','display:none;');
		$('#errorMessagesForReport').attr('style','display:none;');
		$('#errorSelectMessagesForReport').attr('style','display:none;');
		$('#errorSelectMessagesForReportLevel').attr('style','display:none;');
		if($('#reportStartDate').val()=="" && $('#reportEndDate').val()!="" && $('#reportLevel').val()!="select"){
			$('#errorSelectMessagesForReport').attr('style','display:block;color:red;margin-left:400px;');
			$('#usageReportGrid').empty();
			return false;
		}else if(($('#reportStartDate').val()=="" && $('#reportEndDate').val()=="")){
			if($('#reportLevel').val()=="signUpReport"){
				$('#reportStartDate').val("");
				$('#reportEndDate').val("");
				filterSignUpUserDetails();
			}else if($('#reportLevel').val()=="loginReport"){
				var date = new Date();
			 	var currentMonth = date.getMonth()+1;
				var currentDate = date.getDate();
				var currentYear = date.getFullYear();
				var mydate;
				if(currentMonth == "12" || currentMonth == "11" || currentMonth == "10" ){
					mydate=currentMonth+"/" +currentDate+"/"+currentYear;
				}else{
					mydate="0"+currentMonth+"/" +currentDate+"/"+currentYear;
				}
		        $("#reportStartDate").datepicker("setDate", mydate);
		        $("#reportEndDate").datepicker("setDate", mydate);
		        $('#reportStartDate').val(mydate);
				$('#reportEndDate').val(mydate);
				filterLoginUserDetails();
			}else if($('#reportLevel').val()=="select"){
				$('#usageReportGrid').empty();
				$('#reportStartDate').val("");
				$('#reportEndDate').val("");
				$('#errorMessagesForReport').attr('style','display:none;');
				$('#errorSelectMessagesForReport').attr('style','display:none;');
			}
		}else if(($('#reportStartDate').val()!="" && $('#reportEndDate').val()=="")||($('#reportStartDate').val()!="" && $('#reportEndDate').val()!="")){
			if($('#reportLevel').val()=="signUpReport"){
				$('#reportStartDate').val("");
				$('#reportEndDate').val("");
					var date = new Date();
				 	var currentMonth = date.getMonth();
					var currentDate = date.getDate();
					var currentYear = date.getFullYear();
					$(".datepicker2" ).datepicker( "option", "maxDate",new Date(currentYear, currentMonth, currentDate));
					$(".datepicker2" ).datepicker( "option", "minDate",null);
				filterSignUpUserDetails();
			}else if($('#reportLevel').val()=="loginReport"){
				var date = new Date();
			 	var currentMonth = date.getMonth()+1;
				var currentDate = date.getDate();
				var currentYear = date.getFullYear();
				var mydate;
				if(currentMonth == "12" || currentMonth == "11" || currentMonth == "10" ){
					mydate=currentMonth+"/" +currentDate+"/"+currentYear;
				}else{
					mydate="0"+currentMonth+"/" +currentDate+"/"+currentYear;
				}
		        $("#reportStartDate").datepicker("setDate", mydate);
		        $("#reportEndDate").datepicker("setDate", mydate);
		        $('#reportStartDate').val(mydate);
				$('#reportEndDate').val(mydate);
				filterLoginUserDetails();
			}else if($('#reportLevel').val()=="select"){
				$('#usageReportGrid').empty();
				$('#reportStartDate').val("");
				$('#reportEndDate').val("");
				$('#errorMessagesForReport').attr('style','display:none;');
				$('#errorSelectMessagesForReport').attr('style','display:none;');
			}
		}
		else{
			$('#usageReportGrid').empty();
			$('#reportStartDate').val("");
			$('#reportEndDate').val("");
			$('#errorMessagesForReport').attr('style','display:none;');
			$('#errorSelectMessagesForReport').attr('style','display:none;');
		}
	});
	$('#reportStartDate').change(function(){
		$('#barchartloginContainer').attr('style','display:none');
		$('#noRecords').attr('style','display:none;');
		$('#errormsgs').attr('style','display:none;');
		$('#noRecordexist').attr('style','display:none;');
		$('#errorMessagesForReport').attr('style','display:none;');
		$('#errorSelectMessagesForReport').attr('style','display:none;');
		if($('#reportLevel').val()=="signUpReport"){
			var date = new Date();
		 	var currentMonth = date.getMonth();
			var currentDate = date.getDate();
			var currentYear = date.getFullYear();
			$(".datepicker2" ).datepicker( "option", "maxDate",new Date(currentYear, currentMonth, currentDate));
			$(".datepicker2" ).datepicker( "option", "minDate",null);
			filterSignUpUserDetails();
		}else if($('#reportLevel').val()=="loginReport"){
			$(".datepicker1").datepicker("option", "onSelect", function (dateText, inst) {
				if($('#reportLevel').val()=="loginReport"){
					$('#noRecords').attr('style','display:none;');
					
		        var date1 = $.datepicker.parseDate(inst.settings.dateFormat || $.datepicker._defaults.dateFormat, dateText, inst.settings);

		      	var date2 = new Date(date1.getTime());
		      		date2.setDate(date2.getDate() + 2);
		      		var myy=new Date();
		      		var previousDay =(myy.getDate()-1);
		      		if(myy.getDate()==date1.getDate()){
		      			if(myy.getMonth()==date1.getMonth()){
		      				if(myy.getFullYear()==date1.getFullYear()){
		      					$(".datepicker2" ).datepicker( "option", "maxDate",date1);
		      				}
		      			}
		      		}else if(previousDay==date1.getDate()){
		      			if(myy.getMonth()==date1.getMonth()){
		      				if(myy.getFullYear()==date1.getFullYear()){
		      					$(".datepicker2" ).datepicker( "option", "minDate",date1);
		      					$(".datepicker2" ).datepicker( "option", "maxDate",myy);
		      				}
		      			}
		      		}
		      		else{
		      			$(".datepicker2" ).datepicker( "option", "maxDate",date2);
		      		}
		      		$(".datepicker2" ).datepicker( "option", "minDate",date1);
		      		var currentMonth = date2.getMonth()+1;
					var currentDate = date2.getDate();
					var currentYear = date2.getFullYear();
					var mydate;
					if(currentDate == "9" || currentDate == "8" || currentDate == "7" || currentDate == "6" || currentDate == "4" || currentDate == "5"|| currentDate == "3" || currentDate == "2" || currentDate == "1" ){
						currentDate="0"+currentDate;
					}else{
						currentDate=currentDate;
					}
					if(currentMonth == "12" || currentMonth == "11" || currentMonth == "10" ){
						mydate=currentMonth+"/" +currentDate+"/"+currentYear;
					}else{
						mydate="0"+currentMonth+"/" +currentDate+"/"+currentYear;
					}
					$('#reportEndDate').val(mydate);
					var today=new Date();
					if(today<date2)
					{
						currentMonth = today.getMonth()+1;
						currentDate = today.getDate();
						currentYear = today.getFullYear();
						var mydate;
						if(currentDate == "9" || currentDate == "8" || currentDate == "7" || currentDate == "6" || currentDate == "4" || currentDate == "5"|| currentDate == "3" || currentDate == "2" || currentDate == "1" ){
							currentDate="0"+currentDate;
						}else{
							currentDate=currentDate;
						}
						if(currentMonth == "12" || currentMonth == "11" || currentMonth == "10" ){
							mydate=currentMonth+"/" +currentDate+"/"+currentYear;
						}else{
							mydate="0"+currentMonth+"/" +currentDate+"/"+currentYear;
						}
						
						$('#reportEndDate').val(mydate);
					}
		        filterLoginUserDetails();
				}
		    });
			var as=$('#reportStartDate').val();
			var date = new Date(as);
		 	var currentMonth = date.getMonth();
			var currentDate = date.getDate();
			var currentYear = date.getFullYear();
			var dates = new Date(date.getTime());
	        dates.setDate(dates.getDate() + 2);
	        var myy=new Date();
      		var previousDay =(myy.getDate()-1);
      		if(myy.getDate()==date.getDate()){
      			if(myy.getMonth()==date.getMonth()){
      				if(myy.getFullYear()==date.getFullYear()){
      					$(".datepicker2" ).datepicker( "option", "maxDate",date);
      				}
      			}
      		}else if(previousDay==date.getDate()){
      			if(myy.getMonth()==date.getMonth()){
      				if(myy.getFullYear()==date.getFullYear()){
      					$(".datepicker2" ).datepicker( "option", "minDate",date);
      					$(".datepicker2" ).datepicker( "option", "maxDate",myy);
      				}
      			}
      		}
      		else{
      			$(".datepicker2" ).datepicker( "option", "maxDate",dates);
      		}
	        $("#reportStartDate").datepicker("setDate", date);
	        $("#reportEndDate").datepicker( "option", "minDate",new Date(currentYear, currentMonth, currentDate));
			filterLoginUserDetails();
		}else if($('#reportLevel').val()=="select"){
			$('#usageReportGrid').empty();
			$('#reportStartDate').val("");
			$('#reportEndDate').val("");
			$('#errorMessagesForReport').attr('style','display:none;');
			$('#errorSelectMessagesForReport').attr('style','display:none;');
			$('#errorSelectMessagesForReportLevel').attr('style','display:block;color:red;margin-left:100px;');
		}
		else if($('#reportLevel').val()=="loginChartReport"){
			viewLoginDetailsInBarChart();
		}
	});
	$('#reportEndDate').change(function(){
		$('#barchartloginContainer').attr('style','display:none');
		$('#noRecords').attr('style','display:none;');
		$('#errormsgs').attr('style','display:none;');
		$('#noRecordexist').attr('style','display:none;');
		$('#errorMessagesForReport').attr('style','display:none;');
		$('#errorSelectMessagesForReport').attr('style','display:none;');
		$('#errorSelectMessagesForReportLevel').attr('style','display:none;');
		if($('#reportLevel').val()!="select")
		{
			if($('#reportStartDate').val()==""){
				$('#errorSelectMessagesForReport').attr('style','display:block;color:red;margin-left:400px;');
				$('#usageReportGrid').empty();
				return false;
			}else{
				$('#errorSelectMessagesForReport').attr('style','display:none;');
			
				if($('#reportLevel').val()=="signUpReport"){
					filterSignUpUserDetails();
				}else if($('#reportLevel').val()=="loginReport"){
					filterLoginUserDetails();
				}
				else if($('#reportLevel').val()=="loginChartReport"){
					viewLoginDetailsInBarChart();
				}else{
					$('#errorSelectMessagesForReportLevel').attr('style','display:block;color:red;margin-left:150px;');
				}
			}
		}
		else
		{
			$('#usageReportGrid').empty();
			$('#reportStartDate').val("");
			$('#reportEndDate').val("");
			$('#errorMessagesForReport').attr('style','display:none;');
			$('#errorSelectMessagesForReport').attr('style','display:none;');
			$('#errorSelectMessagesForReportLevel').attr('style','display:block;color:red;margin-left:100px;');
		}
	});
	$('#userComplaint').change(function(){
		
		$('#errormsg').attr('style','display:none;');
		$('#noRecord').attr('style','display:none;');
		$('#errormsgs').attr('style','display:none;');
		$('#noRecordexist').attr('style','display:none;');
		$('#userComplaintNote').attr('style','display:none;');
		$('#userComplaintNote').empty();
		fetchComplaintDetailsForAdmin();
	});
	$('#noteComplaint').change(function(){
		$('#errormsg').attr('style','display:none;');
		$('#noRecord').attr('style','display:none;');
		$('#errormsgs').attr('style','display:none;');
		$('#noRecordexist').attr('style','display:none;');
		$('#noteComplaintNote').attr('style','display:none;');
		$('#noteComplaintNote').empty();
		fetchNoteComplaintDetailsForAdmin();
	});
	//Add Prefrences
	/*$("#addPrefrencesOption").click(function(){
		$('#errormsg').attr('style','display:none;');
		$('#noRecord').attr('style','display:none;');
		$('#errormsgs').attr('style','display:none;');
		$('#noRecordexist').attr('style','display:none;');
		userAddPrefrences();
	});*/
	function filterUserDetails(){
		$('#usageReportGrid').empty();
		var userLevel=$("#userLevels").val();
		var userRole=$('#userRole').val();
		var url = urlForServer + "admin/fetchUserDetailsBasedLevelRole";
		var datastr = '{"userRole":"'+userRole+'","userLevel":"'+userLevel+'"}';
		
		$('#usageReportGrid').append('<table id="taskTable" ></table><div id="tablePage"></div>');
		var params = encodeURIComponent(datastr);
		$.ajax({ headers: { 
			 "Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn		
		},
			
					type : 'POST',
					url : url,
					data : params,
					success : function(responseText) 
					{
						if(responseText==''){
							if((userLevel=='select')&&(userRole=='select')){
								$('#noRecords').attr('style','display:none;');
							}
							else{
								$('#noRecords').attr('style','display:block;color:red;margin-left:280px;margin-top:115px;');
							}
						}else{
						var data1 = "";
						var dataLength="";
						data1 = jQuery.parseJSON(responseText);
						if(data1!=null && data1!="")
						{
							for(var i=0;i<data1.length;i++){
							}
						dataLength= data1.length ;
						if(dataLength>=11)
							{
								 height= 450;
							}
							else
							{
								height=  dataLength * 25+50;
							}
						$('#taskTable').jqGrid({
							data:data1,
		   	        		datatype: "local",
		   	        		sortable: true,       		
		   	        		height: 200,
			                width:600,
			                multiselect:false,
			               
			                colNames:['Name','MailId','Level','End-Date','Role','Status'],
		   	        	   	 colModel:[
		   	        	   	    {name:'userName',index:'Id', width:80},
		   	        	   		{name:'emailId',index:'Id', width:110,editable:true},
		   	        	   		{name:'level',index:'level', width:170},
		   	        	   		{name:'endDate',width:80},
		   	        	   		{name:'userRole',width:100},
		   	        	   	{name:'status',width:100},
		   	        	   		//{name:'userId',width:50,hidden:true},
		   	        	   		
		   	        	   ],
		   	        	   
		   	        	pager: '#tablePage',
		   	        	   viewrecords: true, 
		   	        	   rowNum:9,
		   	        	   rownumbers:true, 
		   	        	   sortorder: "desc",
		   	        	   pginput: "false"
		   	        	  // grouping:true, 
		   	        	  // groupingView : { groupField : ['userRole']},
		          	      // caption: "userRole"
		   	        	 });
						$("#taskTable").jqGrid('navGrid',"#tablePage",{edit:false,add:false,del:false,search:false});
						//getSelectedRow();  
						}
						}
						
				},
		error:function(){
			console.log("<-------Error returned while welcome user mail-------> ");
		}
		});
		if(userLevel=='select'){
			$('#noRecords').attr('style','display:none;');
		}
	}
	function filterSignUpUserDetails(){
		$('#usageReportGrid').empty();
		var reportLevel=$("#reportLevel").val();
		var reportStartDate=$('#reportStartDate').val();
		var reportEndDate=$('#reportEndDate').val();
		if( (new Date(reportStartDate).getTime() > new Date(reportEndDate).getTime()))
		{
			$('#errorMessagesForReport').attr('style','display:block;color:red;margin-left:400px;');
			return false;
		}else{
			$('#errorMessagesForReport').attr('style','display:none;');
		}
		var url = urlForServer + "admin/fetchUserDetailsBasedOnReportLevel";
		var datastr = '{"reportLevel":"'+reportLevel+'","reportStartDate":"'+reportStartDate+'","reportEndDate":"'+reportEndDate+'"}';
		$('#usageReportGrid').append('<table id="taskTable" ></table><div id="tablePage"></div>');
		var params = encodeURIComponent(datastr);
		$.ajax({ headers: { 
			 "Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn		
		},
			
					type : 'POST',
					url : url,
					data : params,
					success : function(responseText) 
					{
						if(responseText==''){
							if((userLevel=='select')&&(userRole=='select')){
								$('#noRecords').attr('style','display:none;');
							}
							else{
								$('#noRecords').attr('style','display:block;color:red;margin-left:280px;margin-top:115px;');
							}
						}else{
						var data1 = "";
						var dataLength="";
						data1 = jQuery.parseJSON(responseText);
						if(data1!=null && data1!="")
						{
							for(var i=0;i<data1.length;i++){
							}
						dataLength= data1.length ;
						if(dataLength>=11)
							{
								 height= 450;
							}
							else
							{
								height=  dataLength * 25+50;
							}
						$('#taskTable').jqGrid({
							data:data1,
		   	        		datatype: "local",
		   	        		ignoreCase: true,
		   	        		sortable: true,       		
		   	        		height: 335,
			                width:900,
			                multiselect:false,
			               
			                colNames:['Name','Mail Id','Start-Date','Role','Status'],
		   	        	   	 colModel:[
		   	        	   	    {name:'userName',index:'userName', width:150, sorttype:'string',searchoptions:{sopt:['cn']}},
		   	        	   		{name:'emailId',index:'emailId', width:150, sorttype:'string',searchoptions:{sopt:['cn']}},
		   	        	   		{name:'startDate',index:'startDate', width:150, sorttype:'string',searchoptions:{sopt:['cn']}},
		   	        	   		{name:'userRole',index:'userRole', width:150, sorttype:'string',searchoptions:{sopt:['cn']}},
		   	        	   		{name:'status',index:'status', width:150, sorttype:'string',searchoptions:{sopt:['cn']}},
		   	        	   		//{name:'userId',width:50,hidden:true},
		   	        	   ],
		   	        	   
		   	        	   pager: '#tablePage',
		   	        	   viewrecords: true, 
		   	        	   rownumWidth: 50,
		   	        	   gridview: true,
		   	        	   loadonce:true,
			        	   mtype: "GET", 
		   	        	   rowNum:15,
		   	        	   rownumbers:true, 
		   	        	   sortorder: "desc",
		   	        	   pginput: "false"
		   	        	 });
						$("#taskTable").jqGrid('filterToolbar',{
							searchOperators : true,
						});
						if(is_chrome || is_Ie){
						var $grid = $("#taskTable");
						var $th = $($grid[0].grid.hDiv).find(".ui-jqgrid-htable>thead>.ui-search-toolbar>th:first(" +
						        $grid[0].grid.headers.length + ")");
						$th.find(">div").append($("<button id='mySearch' style='margin-top:1px;height:21px;width:33px;margin-right:-6px;'><span class='ui-icon ui-icon-search'></span></button>").button({
						    text: false, icons: {primary: "ui-icon ui-icon-search"}
						}));
						}else{
							var $grid = $("#taskTable");
							var $th = $($grid[0].grid.hDiv).find(".ui-jqgrid-htable>thead>.ui-search-toolbar>th:first(" +
							        $grid[0].grid.headers.length + ")");
							$th.find(">div").append($("<button id='mySearch' style='margin-top:1px;height:21px;width:23px;margin-right:-6px;'><span class='ui-icon ui-icon-search'></span></button>").button({
							    text: false, icons: {primary: "ui-icon ui-icon-search"}
							}));
						}
						$("#taskTable").jqGrid('navGrid',"#tablePage",{edit:false,add:false,del:false,search:false});
						//getSelectedRow();  
						}
						}
						
				},
		error:function(){
			console.log("<-------Error returned while welcome user mail-------> ");
		}
		});
		if(userLevel=='select'){
			$('#noRecords').attr('style','display:none;');
		}
	}
	function filterLoginUserDetails(){
		$('#usageReportGrid').empty();
		var reportLevel=$("#reportLevel").val();
		var reportStartDate=$('#reportStartDate').val();
		var reportEndDate=$('#reportEndDate').val();
		if( (new Date(reportStartDate).getTime() > new Date(reportEndDate).getTime()))
		{
			$('#errorMessagesForReport').attr('style','display:block;color:red;margin-left:400px;');
			return false;
		}else{
			$('#errorMessagesForReport').attr('style','display:none;');
		}
		
		var url = urlForServer + "admin/fetchUserDetailsBasedOnReportLevel";
		var datastr = '{"reportLevel":"'+reportLevel+'","reportStartDate":"'+reportStartDate+'","reportEndDate":"'+reportEndDate+'"}';
		$('#usageReportGrid').append('<table id="taskTable" ></table><div id="tablePage"></div>');
		var params = encodeURIComponent(datastr);
		$.ajax({ headers: { 
			 "Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn		
		},
			
					type : 'POST',
					url : url,
					data : params,
					success : function(responseText) 
					{
						if(responseText==''){
							if((userLevel=='select')&&(userRole=='select')){
								$('#noRecords').attr('style','display:none;');
							}
							else{
								$('#noRecords').attr('style','display:block;color:red;margin-left:280px;margin-top:115px;');
							}
						}else{
						var data1 = "";
						var dataLength="";
						data1 = jQuery.parseJSON(responseText);
						if(data1!=null && data1!="")
						{
							for(var i=0;i<data1.length;i++){
						}
						dataLength= data1.length ;
						if(dataLength>=11)
							{
								 height= 450;
							}
							else
							{
								height=  dataLength * 25+50;
							}
						$('#taskTable').jqGrid({
							data:data1,
		   	        		datatype: "local",
		   	        		ignoreCase: true,
		   	        		sortable: true,       		
		   	        		height: 335,
			                width:900,
			                multiselect:false,
			               
			                colNames:['Name','Mail Id','Login Time','Role','Status','Login via','Browser & Version'],
		   	        	   	colModel:[
		   	        	   	    {name:'userName',index:'userName', width:350, sorttype:'string',searchoptions:{sopt:['cn']}},
		   	        	   		{name:'emailId',index:'emailId', width:450, sorttype:'string',searchoptions:{sopt:['cn']}},
		   	        	   		{name:'loginTime',index:'loginTime',width:450, sorttype:'string',searchoptions:{sopt:['cn']}},
		   	        	   		{name:'userRole',index:'userRole',width:220, sorttype:'string',searchoptions:{sopt:['cn']}},
		   	        	   		{name:'status',index:'status',width:200, sorttype:'string',searchoptions:{sopt:['cn']}},
		   	        	   		{name:'osName',index:'osName', width:300, sorttype:'string',searchoptions:{sopt:['cn']}},
		   	        	   		{name:'BrowserNameVersion',hidden:true,index:'BrowserNameVersion',width:450, sorttype:'string',searchoptions:{sopt:['cn']}},
		   	        	   		
		   	        	   		//{name:'userId',width:50,hidden:true},
		   	        	   		
		   	        	   ],
		   	        	   pager: '#tablePage',
		   	        	   viewrecords: true, 
		   	        	   gridview: true,
		   	        	   rownumWidth: 50,
		   	        	   loadonce:true,
			        	   mtype: "GET", 
		   	        	   rowNum:15,
		   	        	   rownumbers:true, 
		   	        	   sortorder: "desc",
		   	        	   pginput: "false"
		   	        	 });
						$("#taskTable").jqGrid('filterToolbar',{
							searchOperators : true,
						});
						if(is_chrome || is_Ie){
							var $grid = $("#taskTable");
							var $th = $($grid[0].grid.hDiv).find(".ui-jqgrid-htable>thead>.ui-search-toolbar>th:first(" +
							        $grid[0].grid.headers.length + ")");
							$th.find(">div").append($("<button id='mySearch' style='margin-top:1px;height:21px;width:33px;margin-right:-6px;'><span class='ui-icon ui-icon-search'></span></button>").button({
							    text: false, icons: {primary: "ui-icon ui-icon-search"}
							}));
							}else{
								var $grid = $("#taskTable");
								var $th = $($grid[0].grid.hDiv).find(".ui-jqgrid-htable>thead>.ui-search-toolbar>th:first(" +
								        $grid[0].grid.headers.length + ")");
								$th.find(">div").append($("<button id='mySearch' style='margin-top:1px;height:21px;width:23px;margin-right:-6px;'><span class='ui-icon ui-icon-search'></span></button>").button({
								    text: false, icons: {primary: "ui-icon ui-icon-search"}
								}));
							}
						$("#taskTable").jqGrid('navGrid',"#tablePage",{edit:false,add:false,del:false,search:false});
						//getSelectedRow();  
						}
						}
						
				},
		error:function(){
			console.log("<-------Error returned while welcome user mail-------> ");
		}
		});
		if(userLevel=='select'){
			$('#noRecords').attr('style','display:none;');
		}
	}
function viewLoginDetailsInBarChart(){
	
		if($("#reportStartDate").val()=="")
		{
			var date = new Date();
		 	var currentMonth = date.getMonth()+1;
			var currentDate = date.getDate();
			var currentYear = date.getFullYear();
			var mydate;
			if(currentMonth == "12" || currentMonth == "11" || currentMonth == "10" ){
				mydate=currentMonth+"/" +currentDate+"/"+currentYear;
			}else{
				mydate="0"+currentMonth+"/" +currentDate+"/"+currentYear;
			}
			$('#reportStartDate').val(mydate);
			$('#reportEndDate').val(mydate);
		}
		$('#usageReportGrid').empty();
		
		//$('#reportStartDate').val("");
		//$('#reportEndDate').val("");
		var date = new Date();
		var currentMonth = date.getMonth();
		var currentDate = date.getDate();
		var currentYear = date.getFullYear();
		$(".datepicker2" ).datepicker( "option", "maxDate",new Date(currentYear, currentMonth, currentDate));
		$(".datepicker2" ).datepicker( "option", "minDate",null);
		var reportLevel=$("#reportLevel").val();
		var reportStartDate=$('#reportStartDate').val();
		var reportEndDate=$('#reportEndDate').val();
		if( (new Date(reportStartDate).getTime() > new Date(reportEndDate).getTime()))
		{
			$('#errorMessagesForReport').attr('style','display:block;color:red;margin-left:400px;');
			return false;
		}else{
			$('#errorMessagesForReport').attr('style','display:none;');
		}
		
		var url = urlForServer + "admin/fetchLoginDetailsForChartView";
		var datastr = '{"reportLevel":"'+reportLevel+'","reportStartDate":"'+reportStartDate+'","reportEndDate":"'+reportEndDate+'"}';
		var params = encodeURIComponent(datastr);
		$.ajax({ headers: { 
			 "Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn		
		},
			
					type : 'POST',
					url : url,
					data : params,
					success : function(responseText) 
					{
						$('#barchartloginContainer').attr('style','display:block;margin-top:10px;height:600px;');
						//$("#sliders1").show();
						var data1 = "";
						data1 = jQuery.parseJSON(responseText);
						var signUpList=data1.signup;
						var userList=data1.userCount;
						var loginList=data1.LoginCount;
						var signupcount=0;
						var logincount=0;
						var usercount=0;
						
						if(signUpList!=null && signUpList!='undefined')
						{
							signupcount=(signUpList/1)*1;
						}
						else
						{
							signUpList=0;
							signupcount=0;
						}
						if(userList!=null && userList!='undefined')
						{
							logincount=(userList/1)*1;
						}
						else
						{
							userList=0;
							logincount=0;
						}
						if(loginList!=null && loginList!='undefined')
						{
							usercount=(loginList/1)*1;
						}
						else
						{
							loginList=0;
							usercount=0;
						}
					    var chart = new Highcharts.Chart({
					        chart: {
					            renderTo: 'bar_chartlogin',
					            type: 'column',
					            margin: 75,
					            options3d: {
					                enabled: false,
					                alpha: 0,
					                beta: 0,
					                depth: 75,
					                viewDistance: 25
					            }
					        },
					        title: {
					            text: "User's Report"
					        },
					        credits: {
					            enabled: false
					        },
					       /* subtitle: {
					            text: 'Test options by dragging the sliders below'
					        },*/
					        plotOptions: {
					            column: {
					                depth: 25
					            }
					        },
					        xAxis: {
					            categories: ['Signup Count', 'User Count' ,'Login Count'],
					            title: {
					                text: 'Type'
					            }
					        },
					        yAxis: {
					        	allowDecimals: false,
					            title: {
					                text: 'Count'
					            }
					        },
					        series: [{
					        	showInLegend: false,
					        	name: 'Users',
					            data: [{
					                name: 'Signup Count',
					                color: '#0B610B',
					                y: signupcount,
					                name:'Number of Signup: '+signUpList
					            }, {
					                name: 'User Count',
					                color: '#084B8A',
					                y: logincount,
					                name:'Number of User: '+userList
					            },{
					                name: 'Login Count',
					                color: '#8A0808',
					                y: usercount,
					                name:'Number of Login: '+loginList
					            }]
					        }]
					    });

					   
					  /* // Activate the sliders
					    $('#Rr0').on('change', function(){
					        chart.options.chart.options3d.alpha = this.value;
					      //  showValues1();
					        chart.redraw(false);
					    });
					    $('#Rr1').on('change', function(){
					        chart.options.chart.options3d.beta = this.value;
					       //showValues1();
					        chart.redraw(false);
					    });

					    function showValues1() {
					        $('#Rr0-value').html(chart.options.chart.options3d.alpha);
					        $('#Rr1-value').html(chart.options.chart.options3d.beta);
					    }
					    //showValues1();*/					   
					}
		});
		
		
	} 
	
	//event change after selecting appropriate function
	$('#selectEvent').change(function(){
		$('#errorConfig').empty();
		$('#errormsg').attr('style','display:none;');
		$('#errormsgs').attr('style','display:none;');
		$('#noRecords').attr('style','display:none;');
		$('#error_msg').attr('style','display:none;');
		$('#error_msg1').attr('style','display:none;');
		$('#noRecord').attr('style','display:none;');
		$('#noRecordexist').attr('style','display:none;');
		$('#userComplaintNote').attr('style','display:none;');
		$('#noteComplaintNote').attr('style','display:none;');
		$('#blockingUser').attr('style','display:none;');
		$('#videoUploadContainer').attr('style','display:none;');
		$("#selectErrorMsg1").attr('style','display:none;');
		$("#selectErrorMsg2").attr('style','display:none;');
		$('#blogContainer').attr('style','display:none');
		$("#blogTitle").val("");
		$(".note-editable").empty();
		$("#blogdetails").empty();
		if($(this).val()=='mailConfigEvent'){
				
			$('#defaultConfigContainer').attr('style','display:none;height:0px;');
			//$('#off').attr('checked',true);
			mailNotification();
			$('#mailNotification').attr('style','display:block;margin-top:10px;');
			$('#startDate').val("");
			$('#endDate').val("");
			$('#subject').val("");
			$('#userSubject').val("");
			$('#userMessage').val("");
			$('#message').val("");
			$('#amount').val("");
			$('#days').val("");
			$('#usageReportContainer').attr('style','display:none;height:0px;');
			$('#userComplaintContainer').attr('style','display:none;height:0px;');
			$('#NoteComplaintContainer').attr('style','display:none;height:0px;');
			$('#addSecurityQuestionsContainer').attr('style','display:none');
			$('#addMailTemplateContainer').attr('style','display:none');
			$('#errormsg').attr('style','display:none;height:0px;');
			$('#errormsgs').attr('style','display:none;');
			$('#noRecords').attr('style','display:none;');
			$('#noRecord').attr('style','display:none;');
			$("#selectErrorMsg1").attr('style','display:none;');
			$("#selectErrorMsg2").attr('style','display:none;');
			$('#noRecordexist').attr('style','display:none;');
			$('#charts').attr('style','display:none;height:0px;');
			$('#blockingUser').attr('style','display:none;');
			$('#videoUploadContainer').attr('style','display:none;');
			$('#addPraferenceContainer').attr('style','display:none;');
			$('#update').attr('style','display:none;float:left;');
			$('#updateDelete').attr('style','display:none;float:left;margin-left:1%;');
			$('#submit').attr('style','display:block;float:left;');
			$('#blogContainer').attr('style','display:none');
		}else if($(this).val()=='defaultConfigEvent'){
			
			//$('#mailNotification').attr('style','display:none;');
			$('.onOff').attr('style','display:none;height:0px;');
			$('#mailNotification').attr('style','display:none;height:0px;');
			$('#defaultConfigContainer').attr('style','display:block;height: 550px;');
			$('#usageReportContainer').attr('style','display:none;height:0px;');
			$('#userComplaintContainer').attr('style','display:none;height:0px;');
			$('#NoteComplaintContainer').attr('style','display:none;height:0px;');
			$('#addSecurityQuestionsContainer').attr('style','display:none');
			$('#addMailTemplateContainer').attr('style','display:none');
			$('#charts').attr('style','display:none;height:0px;');
			$('#noRecords').attr('style','display:none;');
			$('#noRecord').attr('style','display:none;');
			$('#noRecordexist').attr('style','display:none;');
			$('#errormsg').attr('style','display:none;');
			$('#errormsgs').attr('style','display:none;');
			$('#blockingUser').attr('style','display:none;');
			$('#videoUploadContainer').attr('style','display:none;');
			$('#addPraferenceContainer').attr('style','display:none;');
			$("#selectErrorMsg1").attr('style','display:none;');
			$("#selectErrorMsg2").attr('style','display:none;');
			$('#blogContainer').attr('style','display:none');
		}else if($(this).val()=='usageReport'){
			
			//$('#mailNotification').attr('style','display:none;');
			$('.onOff').attr('style','display:none;height:0px;');
			$('#mailNotification').attr('style','display:none;height:0px;');
			$('#defaultConfigContainer').attr('style','display:none;height:0px;');
			$('#usageReportContainer').attr('style','display:block;height: 600px;');
			$('#userComplaintContainer').attr('style','display:none;height:0px;');
			$('#NoteComplaintContainer').attr('style','display:none;height:0px;');
			$('#addMailTemplateContainer').attr('style','display:none');
			$('#addSecurityQuestionsContainer').attr('style','display:none');
			$('#charts').attr('style','display:none;height:0px;');
			//$("#userLevels").val('select');
			//$("#userRole").val('select');
			$("#reportLevel").val('select');
			$('#reportStartDate').val("");
			$('#reportEndDate').val("");
			$("#usageReportGrid").empty();
			$('#errorMessagesForReport').attr('style','display:none;');
			$('#errorSelectMessagesForReport').attr('style','display:none;');
			$('#errorSelectMessagesForReportLevel').attr('style','display:none;');
			$('#errormsg').attr('style','display:none;');
			$('#errormsgs').attr('style','display:none;');
			$('#noRecords').attr('style','display:none;');
			$('#noRecordexist').attr('style','display:none;');
			$('#noRecord').attr('style','display:none;');
			$('#blockingUser').attr('style','display:none;');
			$('#videoUploadContainer').attr('style','display:none;');
			$('#addPraferenceContainer').attr('style','display:none;');
			$('#barchartloginContainer').attr('style','display:none');
			$("#selectErrorMsg1").attr('style','display:none;');
			$("#selectErrorMsg2").attr('style','display:none;');
			$('#blogContainer').attr('style','display:none');
		}
		else if($(this).val()=='userComplaintReport'){
			
			//$('#mailNotification').attr('style','display:none;');
			$('.onOff').attr('style','display:none;height:0px;');
			$('#mailNotification').attr('style','display:none;height:0px;');
			$('#defaultConfigContainer').attr('style','display:none;height:0px;');
			$('#usageReportContainer').attr('style','display:none;height:0px;');
			$('#NoteComplaintContainer').attr('style','display:none;height:0px;');
			$('#userComplaintContainer').attr('style','display:block;height: 650px;');
			$('#addMailTemplateContainer').attr('style','display:none');
			$('#addSecurityQuestionsContainer').attr('style','display:none');
			$('#charts').attr('style','display:none;height:0px;');
			$('#errormsg').attr('style','display:none');
			$('#errormsgs').attr('style','display:none;');
			$('#noRecords').attr('style','display:none;');
			$('#noRecord').attr('style','display:none;');
			$('#noRecordexist').attr('style','display:none;');
			$('#blockingUser').attr('style','display:none;');
			$("#userComplaint").val('select');
			$("#userComplaintGrid").empty();
			$('#addPraferenceContainer').attr('style','display:none;');
			$('#videoUploadContainer').attr('style','display:none;');
			$("#selectErrorMsg1").attr('style','display:none;');
			$("#selectErrorMsg2").attr('style','display:none;');
			$('#blogContainer').attr('style','display:none');
			listType='crowd';
			//registerTokenGenerator();
			
		}else if($(this).val()=='noteComplaintReport'){
			
			//$('#mailNotification').attr('style','display:none;');
			$('.onOff').attr('style','display:none;height:0px;');
			$('#mailNotification').attr('style','display:none;height:0px;');
			$('#defaultConfigContainer').attr('style','display:none;height:0px;');
			$('#usageReportContainer').attr('style','display:none;height:0px;');
			$('#userComplaintContainer').attr('style','display:none;height:0px;');
			$('#NoteComplaintContainer').attr('style','display:block;height: 650px;');
			$('#addMailTemplateContainer').attr('style','display:none');
			$('#addSecurityQuestionsContainer').attr('style','display:none');
			$('#charts').attr('style','display:none;height:0px;');
			$('#errormsg').attr('style','display:none');
			$('#errormsgs').attr('style','display:none;');
			$('#noRecords').attr('style','display:none;');
			$('#noRecord').attr('style','display:none;');
			$('#noRecordexist').attr('style','display:none;');
			$('#blockingUser').attr('style','display:none;');
			$("#noteComplaint").val('select');
			$("#noteComplaintGrid").empty();
			$('#addPraferenceContainer').attr('style','display:none;');
			$('#videoUploadContainer').attr('style','display:none;');
			$("#selectErrorMsg1").attr('style','display:none;');
			$("#selectErrorMsg2").attr('style','display:none;');
			$('#blogContainer').attr('style','display:none');
			listType='music';
			//registerTokenGenerator();
		}
		else if($(this).val()=='chartReport'){
			
			//$('#mailNotification').attr('style','display:none;');
			$('.onOff').attr('style','display:none');
			$('#mailNotification').attr('style','display:none;');
			$('#defaultConfigContainer').attr('style','display:none;');
			$('#usageReportContainer').attr('style','display:none');
			$('#NoteComplaintContainer').attr('style','display:none;height:0px;');
			$('#userComplaintContainer').attr('style','display:none');
			$('#charts').attr('style','display:block;height: 600px;');
			$('#addMailTemplateContainer').attr('style','display:none');
			$('#addSecurityQuestionsContainer').attr('style','display:none');
			$("#chaneChart").val('select');
			$("#bar_chart").empty();
			$("#sliders").hide();
			$("#line_chart").empty();
			$("#pie_chart").empty();
			//registerTokenGenerator();
			$('#errormsg').attr('style','display:none;');
			$('#errormsgs').attr('style','display:none;');
			$('#noRecords').attr('style','display:none;');
			$('#noRecord').attr('style','display:none;');
			$('#noRecordexist').attr('style','display:none;');
			$('#blockingUser').attr('style','display:none;');
			$('#addPraferenceContainer').attr('style','display:none;');
			$('#videoUploadContainer').attr('style','display:none;');
			$("#selectErrorMsg1").attr('style','display:none;');
			$("#selectErrorMsg2").attr('style','display:none;');
			$('#blogContainer').attr('style','display:none');
			filterUserDetailsForChartView();
			
		}
		else if($(this).val()=='addSecurityQuestions'){
			
			//$('#mailNotification').attr('style','display:none;');
			$('.onOff').attr('style','display:none');
			$('#updatesecurityQuestionSubmit').attr('style','display:none;');
			$('#securityQuestionSubmit').attr('style','display:inline-block;');
			$('#mailNotification').attr('style','display:none;');
			$('#defaultConfigContainer').attr('style','display:none;');
			$('#usageReportContainer').attr('style','display:none');
			$('#NoteComplaintContainer').attr('style','display:none;height:0px;');
			$('#userComplaintContainer').attr('style','display:none');
			$('#charts').attr('style','display:none;height:0px;');
			$('#addMailTemplateContainer').attr('style','display:none');
			$('#addSecurityQuestionsContainer').attr('style','display:block;height: 600px;');
			//registerTokenGenerator();
			$('#errormsg').attr('style','display:none;');
			$('#errormsgs').attr('style','display:none;');
			$('#noRecords').attr('style','display:none;');
			$('#noRecord').attr('style','display:none;');
			$('#noRecordexist').attr('style','display:none;');
			$('#selectErrorMsgs').attr('style','display:none;');
			$('#blockingUser').attr('style','display:none;');
			$('#videoUploadContainer').attr('style','display:none;');
			$('#addPraferenceContainer').attr('style','display:none;');
			$("#selectErrorMsg1").attr('style','display:none;');
			$("#selectErrorMsg2").attr('style','display:none;');
			$('#blogContainer').attr('style','display:none');
			fetchsecurityQuestionView();
		}
		else if($(this).val()=='addMailTemplates'){
			
			//$('#mailNotification').attr('style','display:none;');
			$('.onOff').attr('style','display:none');
			$('#updateTemplateSubmit').attr('style','display:none;');
			$('#addTemplateSubmit').attr('style','display:inline-block;');
			$('#mailNotification').attr('style','display:none;');
			$('#defaultConfigContainer').attr('style','display:none;');
			$('#usageReportContainer').attr('style','display:none');
			$('#NoteComplaintContainer').attr('style','display:none;height:0px;');
			$('#userComplaintContainer').attr('style','display:none');
			$('#addMailTemplateContainer').attr('style','display:block;height: 600px;');
			$('#charts').attr('style','display:none;height:0px;');
			$('#addSecurityQuestionsContainer').attr('style','display:none');
			$('#templateName').val("");
			$('#templateContent').val("");
			$('#viewAddedTemplates').empty();
			//registerTokenGenerator();
			$('#errormsg').attr('style','display:none;');
			$('#errormsgs').attr('style','display:none;');
			$('#noRecords').attr('style','display:none;');
			$('#noRecord').attr('style','display:none;');
			$('#noRecordexist').attr('style','display:none;');
			$('#blockingUser').attr('style','display:none;');
			$('#videoUploadContainer').attr('style','display:none;');
			$('#addPraferenceContainer').attr('style','display:none;');
			$("#selectErrorMsg1").attr('style','display:none;');
			$("#selectErrorMsg2").attr('style','display:none;');
			$('#blogContainer').attr('style','display:none');
		}
		//Blocking user
		else if($(this).val()=='blockingUser')
		{
			$("#noRecordFound").attr('style','display:none');
			$('#selectBlock').val("select");
			$('.onOff').attr('style','display:none');
			$('#mailNotification').attr('style','display:none;height:0px;');
			$('#defaultConfigContainer').attr('style','display:none;');
			$('#usageReportContainer').attr('style','display:none;height:0px;');
			$('#userComplaintContainer').attr('style','display:none;height:0px;');
			$('#NoteComplaintContainer').attr('style','display:none;height:0px;');
			$('#addSecurityQuestionsContainer').attr('style','display:none');
			$('#addMailTemplateContainer').attr('style','display:none');
			$('#addPraferenceContainer').attr('style','display:none;');
			$('#errormsg').attr('style','display:none;height:0px;');
			$('#errormsgs').attr('style','display:none;');
			$('#noRecords').attr('style','display:none;');
			$('#noRecord').attr('style','display:none;');
			$('#noRecordexist').attr('style','display:none;');
			$('#charts').attr('style','display:none;height:0px;');
			$('#videoUploadContainer').attr('style','display:none;');
			$('#blockingUser').attr('style','display:block;height: 600px;');
			$("#selectErrorMsg1").attr('style','display:none;');
			$("#selectErrorMsg2").attr('style','display:none;');
			$('#blogContainer').attr('style','display:none');
			$("#jQGridDiv").empty();
		}
		//Add prefrences
		else if($(this).val()=='addPreferences'){
			$('.onOff').attr('style','display:none');
			$('#updateTemplateSubmit').attr('style','display:none;');
			$('#addTemplateSubmit').attr('style','display:none');
			$('#mailNotification').attr('style','display:none;');
			$('#defaultConfigContainer').attr('style','display:none;');
			$('#usageReportContainer').attr('style','display:none');
			$('#NoteComplaintContainer').attr('style','display:none');
			$('#userComplaintContainer').attr('style','display:none');
			$('#addMailTemplateContainer').attr('style','display:none');
			$('#charts').attr('style','display:none;height:0px;');
			$('#addSecurityQuestionsContainer').attr('style','display:none');			
			//registerTokenGenerator();
			$('#errormsg').attr('style','display:none;');
			$('#errormsgs').attr('style','display:none;');
			$('#noRecords').attr('style','display:none;');
			$('#noRecord').attr('style','display:none;');
			$('#noRecordexist').attr('style','display:none;');
			$('#emailView').attr('style','display:none;');
			$('#securityQuestionView').attr('style','display:none;');
			$('#addPraferenceContainer').attr('style','display:block;height: 600px;');
			$('#blockingusertable').attr('style','display:block;height:600px;');
			$('#videoUploadContainer').attr('style','display:none;');
			$("#selectErrorMsg1").attr('style','display:none;');
			$("#selectErrorMsg2").attr('style','display:none;');
			$('#blogContainer').attr('style','display:none');
			userAddPreferencesGrid();
		}
		//Video upload
		else if($(this).val()=='videoUpload')
		{
			$("#noRecordFound").attr('style','display:none');
			$('#selectBlock').val("select");
			$('.onOff').attr('style','display:none');
			$('#mailNotification').attr('style','display:none;height:0px;');
			$('#defaultConfigContainer').attr('style','display:none;');
			$('#usageReportContainer').attr('style','display:none;height:0px;');
			$('#userComplaintContainer').attr('style','display:none;height:0px;');
			$('#NoteComplaintContainer').attr('style','display:none;height:0px;');
			$('#addSecurityQuestionsContainer').attr('style','display:none');
			$('#addMailTemplateContainer').attr('style','display:none');
			$('#errormsg').attr('style','display:none;height:0px;');
			$('#errormsgs').attr('style','display:none;');
			$('#noRecords').attr('style','display:none;');
			$('#noRecord').attr('style','display:none;');
			$('#noRecordexist').attr('style','display:none;');
			$('#charts').attr('style','display:none;height:0px;');
			$('#blockingUser').attr('style','display:none;');
			$('#addPraferenceContainer').attr('style','display:none;');
			$('#videoUploadContainer').attr('style','display:block;height: 600px;');
			$("#selectErrorMsg1").attr('style','display:none;');
			$("#selectErrorMsg2").attr('style','display:none;');
			$('#blogContainer').attr('style','display:none');
			adminUploadToken=musicnote;
			viewUploadedFile();
		}
		else if($(this).val()=='blogInsert')
		{
			$('.note-editor').remove();
			$('.blogEditor').empty();
			var divContent='<div class="form-group">'+
					'<label><Font color="red">*</Font>Blog Title:</label></div>'+
					'<div id="blogTitlediv" >'+
					'<input type="text" class="form-control" id="blogTitle" style="margin-bottom:20px"></input></div>'+
					'<div class="form-group">'+
					'<label><Font color="red">*</Font>Blog Content:</label></div>'+
					'<div class="summernote" id="blogDescription" style="height:100px;overflow:scroll"></div>'+
					'<div id="blogDescriptionMsgs" style="display:none;">Field cannot be blank</div><br>'+
					'<button id="addBlog" class="btn btn-success" onclick="blog()" >Add</button>'+
					'<button id="updateBlog" class="btn btn-success" style="display:none;" >Update</button>'+
					'<button id="deleteBlog" class="btn btn-danger" style="display:none;" >Delete</button>'+
					'<button id="cancelBlog" class="btn btn-default" onclick="cancelblog()" style="margin-left:10px">Clear</button>';
					$(".blogEditor").append(divContent);
						  $('.summernote').summernote({

							 onImageUpload: function(files, editor, welEditable) {
						        sendFile(files[0], editor, welEditable);
						    }, 
						  height: 230,                 // set editor height
						  maxHeight: 230,             // set maximum height of editor
						  width:550,
						  focus: true                // set focus to editable area after initializing summernote
						});
			$("#noRecordFound").attr('style','display:none');
			$('#selectBlock').val("select");
			$('.onOff').attr('style','display:none');
			$('#mailNotification').attr('style','display:none;height:0px;');
			$('#defaultConfigContainer').attr('style','display:none;');
			$('#usageReportContainer').attr('style','display:none;height:0px;');
			$('#userComplaintContainer').attr('style','display:none;height:0px;');
			$('#NoteComplaintContainer').attr('style','display:none;height:0px;');
			$('#addSecurityQuestionsContainer').attr('style','display:none');
			$('#addMailTemplateContainer').attr('style','display:none');
			$('#noRecords').attr('style','display:none;');
			$('#noRecord').attr('style','display:none;');
			$('#noRecordexist').attr('style','display:none;');
			$('#charts').attr('style','display:none;height:0px;');
			$('#blockingUser').attr('style','display:none;');
			$('#addPraferenceContainer').attr('style','display:none;');
			$('#videoUploadContainer').attr('style','display:none;');
			$("#blogDescriptionMsgs").attr('style','display:none;');
			$('#blogContainer').attr('style','display:block;height: 600px;');
			fetchBlogDetails();
		}
		else{
			
			$('#on').attr('checked',false);
			$('#mailNotification').attr('style','display:none');
			$('#defaultConfigContainer').attr('style','display:none;');
			$('#usageReportContainer').attr('style','display:none');
			$('#userComplaintContainer').attr('style','display:none');
			$('#NoteComplaintContainer').attr('style','display:none;height:0px;');
			$('#charts').attr('style','display:none;');
			$('#addMailTemplateContainer').attr('style','display:none');
			$('#addSecurityQuestionsContainer').attr('style','display:none');
			$('.onOff').attr('style','display:none');
			$('#errormsg').attr('style','display:none;');
			$('#errormsgs').attr('style','display:none;');
			$('#noRecords').attr('style','display:none;');
			$('#noRecord').attr('style','display:none;');
			$('#noRecordexist').attr('style','display:none;');
			$('#addPraferenceContainer').attr('style','display:none;');
			$('#videoUploadContainer').attr('style','display:none;');
			$("#selectErrorMsg1").attr('style','display:none;');
			$("#selectErrorMsg2").attr('style','display:none;');
			$('#blogContainer').attr('style','display:none');
		}
	});
	 $(function() {
		 	var date = new Date();
		 	var currentMonth = date.getMonth();
			var currentDate = date.getDate();
			var currentYear = date.getFullYear();
		 $('.datepicker1').datepicker({
				maxDate: new Date(currentYear, currentMonth, currentDate),
				});
			$('.datepicker2').datepicker({
				maxDate: new Date(currentYear, currentMonth, currentDate),
				});
		/* $(".datepicker1").datepicker("option", "onSelect", function (dateText, inst) {
		        var date1 = $.datepicker.parseDate(inst.settings.dateFormat || $.datepicker._defaults.dateFormat, dateText, inst.settings);

		      	var date2 = new Date(date1.getTime());
		        date2.setDate(date2.getDate() + 2);
		       // $(".datepicker2").datepicker("setDate", date2);
		        $(".datepicker2" ).datepicker( "option", "maxDate",date2);
		        $(".datepicker2" ).datepicker( "option", "minDate",date1);
		      	
		    });*/
		/* $(".datepicker2").datepicker("option", "onSelect", function (dateText, inst) {
		        var date1 = $.datepicker.parseDate(inst.settings.dateFormat || $.datepicker._defaults.dateFormat, dateText, inst.settings);

		      	var date2 = new Date(date1.getTime());
		        date2.setDate(date2.getDate() - 2);
		        $(".datepicker1").datepicker("setDate", date2);

		      	
		    });*/
		 
			//var date = new Date();
	/*var currentMonth = date.getMonth();
	var currentDate = date.getDate();
	var currentYear = date.getFullYear();
	var today=currentMonth+"/" +currentDate+"/"+ currentYear;
	//$('#reportStartDate').attr('value',"'+'+").val(currentMonth+"/" +currentDate+"/"+ currentYear);
	var lastThreedate = new Date();
	lastThreedate.setDate(lastThreedate.getDate()-2);
	var lastThreedateMonth = lastThreedate.getMonth();
	var lastThreedateDate = lastThreedate.getDate();
	var lastThreedateYear = lastThreedate.getFullYear();
	
	$('.datepicker').datepicker({
		//maxDate: new Date(currentYear, currentMonth, currentDate)
	
		});
	
	$('.datepicker1').datepicker({
		//maxDate: new Date(currentYear, currentMonth, currentDate)
		maxDate: new Date(currentYear, currentMonth, currentDate),
		//minDate: new Date(lastThreedateYear, lastThreedateMonth, lastThreedateDate)
		});
	$('.datepicker2').datepicker({
		//maxDate: new Date(currentYear, currentMonth, currentDate)
		maxDate: new Date(currentYear, currentMonth, currentDate),
		//minDate: new Date(lastThreedateYear, lastThreedateMonth, lastThreedateDate)
		});*/
	});
	/*$(function() {
		$(".datepicker").datetimepicker({
			pick12HourFormat: true,
			"format": 'mm/dd/yyyy',
		 	weekStart: 0,
		    todayBtn:  1,
		    defaultDate: "+0d",
		     numberOfMonths: 1,
		    minDate: 0,
			forceParse: 0,
		    showMeridian: 1,
		    todayHighlight: true,
		    autoclose: 1,
			startView: 2,
		    minView:2,
		    maxView:4,
		});
	});*/
	var data="";	
	//adminId is set here

	
	//proceed after confirmation
	$('#configProceed').click(function(){
		addDefaultConfig();
	});
	
	//for default configuration entry
	
	$('#configSubmit').click(function(){
		//e.preventDefault();
		var url=urlForServer +'admin/checkDefaultConfig';
		
		$.ajax({ headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn	
		},
	    	type: 'POST',
	    	url : url,
	    	cache: false,
	    	//data:params,
	    	contentType: "application/json; charset=utf-8",
	    	//dataType: "json",
	    	success : function(response){
			/////////////////////
			if(response=="absent" ||response==""){
				addDefaultConfig();
			}else if(response=="present"){
				//$('#configStatusAlert').show();
				//$('#configStatusAlert').attr('style','margin-top:10px;');
				$('#intervalAlertModal2').modal('show');
				$('#intervalAlertModal2').children('.modal-dialog').children('.modal-content').children('.modal-body').empty();
				$('#intervalAlertModal2').children('.modal-dialog').children('.modal-content').children('.modal-body').append('<button style="" aria-hidden="true" data-dismiss="modal" class="close" type="button">�</button>'); 
				$('#intervalAlertModal2').children('.modal-dialog').children('.modal-content').children('.modal-body').append("<h5>Default Configuration already exist, If you wish to override then click 'Proceed' ");
//				
				
			}
			
			//////////////
		 	
	    	},
	    	error: function(e) {
	    		
	    		alert("Please try again later");
	    	}
	    });
		
		
		
	});
	
	//for default configuration entry
	
	function addDefaultConfig(){
		var payment=[];
		payment.push($('#configAmount').val()+'-'+$('#configDays').val(),$('#configAmount1').val()+'-'+$('#configDays1').val(),$('#configAmount2').val()+'-'+$('#configDays2').val());
		
		if($('#configAmount3').val() !="" && $('#configDays3').val()!="" ){
			payment.push($('#configAmount3').val()+'-'+$('#configDays3').val());
		}
		if($('#configAmount4').val()!="" && $('#configDays4').val()!=""){
			payment.push($('#configAmount4').val()+'-'+$('#configDays4').val());
		}
				
		var uploadLimit=$('#uploadLimit').val();
		
		var params='{"paymentAmountDays":"'+payment+'","uploadLimit":"'+uploadLimit+'","userId":"'+userId+'","status":"'+"A"+'"}';
		var url=urlForServer +'admin/addDefaultConfig';
		
		 $.ajax({ headers: { 
			 "Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn	
		 },
		    	type: 'POST',
		    	url : url,
		    	cache: false,
		    	data:params,
		    	contentType: "application/json; charset=utf-8",
		    	//dataType: "json",
		    	success : function(response){
			 	$('#configAmount').val('');$('#configAmount1').val('');$('#configAmount2').val('');
			 	$('#configAmount3').val('');$('#configAmount4').val('');
			 	$('#configDays').val('');$('#configDays1').val('');$('#configDays2').val('');
			 	$('#configDays3').val('');$('#configDays4').val('');
			 	$('#uploadLimit').val('');
			 	
		    	},
		    	error: function(e) {
		    		
		    		alert("Please try again later");
		    	}
		    });
	}

	$('#emailViewClear').click(function(){
	
	$('#editViewEvents').attr('style','display:none;');
	$('#viewEvents').empty();
	$('#editViewEvents').empty();
	
	});
	
	//To validate fields
	
	function validateConfiguration(){
		var mailNo="0";
		var userLevel=$('#userLevel').val();
		var interval=$('#interval').val();
		var startDate=$('#startDate').val();
		var endDate=$('#endDate').val();
		var subject=$('#subject').val();
		var messageVal=$('#message').val();
		var days=$('#days').val();
		var amount=$('#amount').val();
		var amountDays=amount+'-'+days;
		var message = messageVal.replace(/\r?\n/g, '<br>');
		var userMessage=$('#userMessage').val();
		var userSubject=$('#userSubject').val();
		
		var mailNo="0";

		var startDates=new Date(startDate);
		//var date1=startDates.toLocaleFormat('%d/%m/%Y');
		
		var endDates=new Date(endDate);
		//var date2=endDates.toLocaleFormat('%d/%m/%Y');
		
		
		if(new Date(startDates).getTime()>new Date(endDates).getTime()){
			$('#error_msg1').attr('style','display:block;color:red;margin-bottom:1.5%;');
			return false;	
		}else{
			$('#error_msg1').attr('style','display:none');
			
		}
		
		if(userMessage=="" || userMessage==undefined){
			$('#error_msg').attr('style','display:block;color:red;margin-bottom:1.5%;');
			return false;	
		}else if(startDate=="" || startDate==undefined  || userSubject=="" ||userSubject==undefined ){
			$('#error_msg').attr('style','display:block;color:red;margin-bottom:1.5%;');
			return false;
		}else if(endDate=="" || endDate==undefined){
			$('#error_msg').attr('style','display:block;color:red;margin-bottom:1.5%;');
			return false;
		}else if(subject=="" || subject==undefined){
			$('#error_msg').attr('style','display:block;color:red;margin-bottom:1.5%;');
			return false;
		}else if(message=="" || message==undefined){
			$('#error_msg').attr('style','display:block;color:red;margin-bottom:1.5%;');
			return false;
		}else if(userLevel=="" || userLevel==undefined){
			$('#error_msg').attr('style','display:block;color:red;margin-bottom:1.5%;');
			return false;
		}else if(interval=="" || interval==undefined){
			$('#error_msg').attr('style','display:block;color:red;margin-bottom:1.5%;');
			return false;
		}else if(amount=="" || amount==undefined){
			$('#error_msg').attr('style','display:block;color:red;margin-bottom:1.5%;');
			return false;
		}
		else if(days=="" || amount==undefined){
			$('#error_msg').attr('style','display:block;color:red;margin-bottom:1.5%;');
			return false;
		}
		else{
			$('#error_msg').attr('style','display:none');
			$('#error_msg1').attr('style','display:none');
			return true;
		}
	}

$("#submit").click(function(){
				
				var mailNo="0";
				var userLevel=$('#userLevel').val();
				var interval=$('#interval').val();
				var startDate=$('#startDate').val();
				var endDate=$('#endDate').val();
				var subject=$('#subject').val();
				var messageVal=$('#message').val();
				var days=$('#days').val();
				var amount=$('#amount').val();
				var amountDays=amount+'-'+days;
				var message = messageVal.replace(/\r?\n/g, '<br>');
				var mailNo="0";
				var userMessage=$('#userMessage').val();
				var userSubject=$('#userSubject').val();
				
				var state=validateConfiguration();
				if(state==false){
					return false;
				}
				
				var params='{"amountDays":"'+amountDays+'","userId":"'+userId+'","userLevel":"'+userLevel+'","interval":"'+interval+'","startDate":"'+startDate+'","endDate":"'+endDate+'","mailSubject":"'+subject+'","mailNo":"'+mailNo+'","mailMessage":"'+message+'","userSubject":"'+userSubject+'","userMessage":"'+userMessage+'"}';
				var url=urlForServer+'admin/checkUserLevel';			
				 $.ajax({ headers: { 
					 "Mn-Callers" : musicnote,
				    	"Mn-time" :musicnoteIn	
				 },
				    	type: 'POST',
				    	url : url,
				    	cache: false,
				    	data:params,
				    	contentType: "application/json; charset=utf-8",
				    	//dataType: "json",
				    	success : function(response){
							if(response=="empty"){
								addMailConfiguration();	
							}else{
								$('#intervalAlertModal').modal('show');
								$('#intervalAlertModal').children('.modal-dialog').children('.modal-content').children('.modal-body').empty();
								$('#intervalAlertModal').children('.modal-dialog').children('.modal-content').children('.modal-body').append('<button style="" aria-hidden="true" data-dismiss="modal" class="close" type="button">�</button>'); 
								$('#intervalAlertModal').children('.modal-dialog').children('.modal-content').children('.modal-body').append("<h5>E-mail Configuration already exist for selected User Level : " +'<b>'+userLevel+'<b>'+"</h5> "+ "<br></br>");

							}
				    	},
				    	error: function(e) {
				    		alert("Please try again later");
				    	}
				    });
});
				
				
				$('#intervalAlertModal').on('click','#proceed',function(){
					addMailConfiguration();
				});
				$('#intervalAlertModal1').on('click','#updateProceed',function(){
					updateMailConfiguration();
				});
				

//To add the mail configuration
function addMailConfiguration(){
	
	var days=$('#days').val();
	var amount=$('#amount').val();
	var amountDays=amount+'-'+days;
	var mailNo="0";
	var userLevel=$('#userLevel').val();
	var interval=$('#interval').val();
	var startDate=$('#startDate').val();
	var endDate=$('#endDate').val();
	var subject=$('#subject').val();
	var messageVal=$('#message').val();
	var message = messageVal.replace(/\r?\n/g, '<br>');
	var userMessage=$('#userMessage').val();
	var userSubject=$('#userSubject').val();
	var message1=userMessage.replace(/\r?\n/g, '<br>');
	
	var params='{"amountDays":"'+amountDays+'","userId":"'+userId+'","userLevel":"'+userLevel+'","interval":"'+interval+'","startDate":"'+startDate+'","endDate":"'+endDate+'","mailSubject":"'+subject+'","mailMessage":"'+message+'","mailNo":"'+mailNo+'","userSubject":"'+userSubject+'","userMessage":"'+message1+'"}';
	
	var url=urlForServer+'admin/addMailConfiguration';			
	 $.ajax({ headers: {
		 "Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn	
	 },
	    	type: 'POST',
	    	url : url,
	    	cache: false,
	    	data:params,
	    	contentType: "application/json; charset=utf-8",
	    	//dataType: "json",
	    	success : function(response){
		 	
		 	getMailEventsNames();
	    	},
	    	error: function(e) {
	    		
	    		alert("Please try again later");
	    	}
	    });
	 
	 	$('#userLevel').val("");
		$('#interval').val("");
		$('#startDate').val("");
		$('#endDate').val("");
		$('#subject').val("");
		$('#message').val("");
		$('#days').val("");
		$('#amount').val("");
		$('#userMessage').val("");
		$('#userSubject').val("");
}


$('#emailView').click(function(){
	getMailEventsNames();	
});

//To display the clicked mail configuration
$('#viewEvents').on('click','.emailUpdate',function(){
	$('#editViewEvents').attr('style','display:block;');
	var checkMailId=$(this).attr('id');
	for(var i=0;i<data.length;i++){
		if(data[i].mailNo==checkMailId){
			var mailNo=data[i].mailNo;
			var classs=$('#userLevel').attr('class');
			
			$('#userLevel').removeAttr('class');
			$('#userLevel').val("");
			$('#interval').val("");
			$('#startDate').val("");
			$('#endDate').val("");
			$('#subject').val("");
			$('#message').val("");
			$('#days').val("");
			$('#amount').val("");
			$('#userMessage').val("");
			$('#userSubject').val("");
			$('#update').attr('style','display:block;float:left;');
			$('#updateDelete').attr('style','display:block;float:left;margin-left:1%;');
			$('#submit').attr('style','display:none;');
			
			var split=(data[i].amountDays).split('-');
			var days=split[0];
			var amount=split[1];
			
			$('#userLevel').val(data[i].userLevel);
			$('#userLevel').attr('class','form-control ~'+mailNo+'');
			$('#interval').val(data[i].intervel);
			$('#startDate').val(data[i].startDate);
			$('#endDate').val(data[i].endDate);
			var message1 = data[i].userMessage.replace(/(<br\s*\/?>)+/g, "\n");;
			$('#userMessage').val(message1);
			$('#userSubject').val(data[i].userSubject);
			$('#days').val(days);
			$('#amount').val(amount);
			
			$('#subject').val(data[i].mailSubject);
			var message = data[i].mailMessage.replace(/(<br\s*\/?>)+/g, "\n");;
			//var message=message1.replace('<br>','\n');
			$('#message').val(message);
			
		}
	}
});



//To update the mail configuration 
$('.update').click(function(){
	var userLevelClassName=$('#userLevel').attr('class');
	var split=userLevelClassName.split('~');
	var mailNo=split[1];
	var userLevel=$('#userLevel').val();
	var interval=$('#interval').val();
	var startDate=$('#startDate').val();
	var endDate=$('#endDate').val();
	var subject=$('#subject').val();
	var messageVal=$('#message').val();
	var days=$('#days').val();
	var amount=$('#amount').val();
	var message = messageVal.replace(/\r?\n/g, '<br>');
	var userMessage=$('#userMessage').val();
	var userSubject=$('#userSubject').val();
	var message1=userMessage.replace(/\r?\n/g, '<br>');
	
	//To validate fields
	var state=validateConfiguration();
	if(state==false){
		return false;
	}
	
	var params='{"userId":"'+userId+'","userLevel":"'+userLevel+'","interval":"'+interval+'","mailSubject":"'+subject+'","mailMessage":"'+message+'","startDate":"'+startDate+'","endDate":"'+endDate+'","mailNo":"'+mailNo+'","userSubject":"'+userSubject+'","userMessage":"'+message1+'"}';
	
	var url=urlForServer+'admin/checkUserLevel';			
	 $.ajax({ headers: { 
		 "Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn	
	 },
	    	type: 'POST',
	    	url : url,
	    	cache: false,
	    	data:params,
	    	contentType: "application/json; charset=utf-8",
	    	//dataType: "json",
	    	success : function(response){
				if(response=="empty"){
					updateMailConfiguration();
				}else{
					$('#intervalAlertModal1').modal('show');
					$('#intervalAlertModal1').children('.modal-dialog').children('.modal-content').children('.modal-body').empty();
					$('#intervalAlertModal1').children('.modal-dialog').children('.modal-content').children('.modal-body').append('<button style="" aria-hidden="true" data-dismiss="modal" class="close" type="button">�</button>'); 
					$('#intervalAlertModal1').children('.modal-dialog').children('.modal-content').children('.modal-body').append("<h5>E-mail Configuration already exist for selected User Level : " +'<b>'+userLevel+'<b>'+"</h5> "+ "<br></br>");
				}
	    	},
	    	error: function(e) {
	    		alert("Please try again later");
	    	}
	    });
	
});

function updateMailConfiguration(){
	var userLevelClassName=$('#userLevel').attr('class');
	var split=userLevelClassName.split('~');
	var mailNo=split[1];
	var userLevel=$('#userLevel').val();
	var interval=$('#interval').val();
	var startDate=$('#startDate').val();
	var endDate=$('#endDate').val();
	var subject=$('#subject').val();
	var messageVal=$('#message').val();
	var days=$('#days').val();
	var amount=$('#amount').val();
	var amountDays=amount+'-'+days;
	var message = messageVal.replace(/\r?\n/g, '<br>');
	var userMessage=$('#userMessage').val();
	var userSubject=$('#userSubject').val();
	var message1=userMessage.replace(/\r?\n/g, '<br>');
	
	var params='{"amountDays":"'+amountDays+'","userId":"'+userId+'","userLevel":"'+userLevel+'","interval":"'+interval+'","mailSubject":"'+subject+'","mailMessage":"'+message+'","startDate":"'+startDate+'","endDate":"'+endDate+'","userSubject":"'+userSubject+'","userMessage":"'+message1+'"}';
	var url=urlForServer+'admin/updateMailConfiguration/'+mailNo;			
	 $.ajax({ headers: { 
		 "Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn	
	 },
	    	type: 'POST',
	    	url : url,
	    	cache: false,
	    	data:params,
	    	contentType: "application/json; charset=utf-8",
	    	//dataType: "json",
	    	success : function(response){
				 getMailEventsNames();
	    	},
	    	error: function(e) {
	    		alert("Please try again later");
	    	}
	    });
}

//To clear the fields
$('#cancel').click(function(){
	$('#userLevel').val("all");
	$('#interval').val("daily");
	$('#startDate').val("");
	$('#endDate').val("");
	$('#subject').val("");
	$('#message').val("");
	$('#days').val("");
	$('#userMessage').val("");
	$('#userSubject').val("");
	$('#amount').val("");
});

//To delete the mail configuration
$('#updateDelete').click(function(){
	var userLevelClassName=$('#userLevel').attr('class');
	var split=userLevelClassName.split('~');
	var mailNo=split[1];
	var params='{"mailNo":"'+mailNo+'"}';
	var url=urlForServer+'admin/deleteMailConfiguration';			
	 $.ajax({ headers: { 
		 "Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn	
	 },
	    	type: 'POST',
	    	url : url,
	    	cache: false,
	    	data:params,
	    	contentType: "application/json; charset=utf-8",
	    	//dataType: "json",
	    	success : function(response){
		 	 getMailEventsNames();
	    	},
	    	error: function(e) {
	    	alert("Please try again later");
	    	}
	    });
	
	$('#userLevel').val("all");
	$('#interval').val("daily");
	$('#startDate').val("");
	$('#endDate').val("");
	$('#subject').val("");
	$('#message').val("");
	$('#days').val("");
	$('#amount').val("");
	$('#userMessage').val("");
	$('#userSubject').val("");
});

//To get MailConfiguration subject and userLevel
function getMailEventsNames(){
	var url=urlForServer+'admin/getMailConfiguration';			
	 $.ajax({ headers: { 
		 "Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn	
	 },
	    	type: 'POST',
	    	url : url,
	    	cache: false,
	    	contentType: "application/json; charset=utf-8",
	    	dataType: "json",
	    	success : function(response){
		 		$('#viewEvents').empty();
				data=response;
				for(var i=0;i<data.length;i++){
					var subject=data[i].mailSubject;
					var interval=data[i].intervel;
					var mailId=data[i].mailNo;
					var eventData='<div  id="'+mailId+'" class="emailUpdate" title="click to update your events" style="color:grey;font-size:20px;">&nbsp;&nbsp;&nbsp;&nbsp;'+subject+' - <a>'+interval+'</a></div>';
					$('#viewEvents').append(eventData);
				}
	    	},
	    	error: function(e) {
	    		
	    		alert("Please try again later");
	    	}
	    });
}

//To enable the add functionality
$('#add').click(function(){
	$('#update').attr('style','display:none;float:left;');
	$('#updateDelete').attr('style','display:none;float:left;margin-left:1%;');
	$('#submit').attr('style','display:block;float:left;');
	$('#userLevel').val("all");
	$('#interval').val("daily");
	$('#startDate').val("");
	$('#endDate').val("");
	$('#subject').val("");
	$('#message').val("");
	$('#days').val("");
	$('#amount').val("");
	$('#userMessage').val("");
	$('#userSubject').val("");
});

//To toggle on/off settings
$('#on').click(function(){
	var url=urlForServer +'admin/updateMailNotification';
	var params='{"status":'+true+',"checkFlag":'+false+'}';
	
	 $.ajax({ headers: {
		 "Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn	
	 },
	    	type: 'POST',
	    	url : url,
	    	cache: false,
	    	data:params,
	    	contentType: "application/json; charset=utf-8",
	    	//dataType: "json",
	    	success : function(response){
		 	if(response!=null ||response!=""){
		 	if(response=="ConfigurationAbsent"){
		 		$('#errorConfig').empty();
				$('#errorConfig').append('Please add Default Configuration');
		 		
		 		$('#on').removeAttr('checked');
		 		 $('#off').attr('checked',true);
			 }else{
				 $('#errorConfig').empty();
		 	$('.onOff').attr('style','display:block;height:900px;');
			 }
		 	
		 	}
	    	},
	    	error: function(e) {
	    		
	    		alert("Please try again later");
	    	}
	    });

	
});

$('#off').click(function(){
	var url=urlForServer +'admin/updateMailNotification';
	var params='{"status":'+false+',"checkFlag":'+false+'}';
	
	 $.ajax({ headers: { 
		 "Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn	
	 },
	    	type: 'POST',
	    	url : url,
	    	cache: false,
	    	data:params,
	    	contentType: "application/json; charset=utf-8",
	    	//dataType: "json",
	    	success : function(response){
		 	
		 	 if(response!=null ||response!=""){
			 if(response=="ConfigurationAbsent"){
				$('#errorConfig').empty();
				$('#errorConfig').append('Please add Default Configuration');
			 }else{
				 $('#errorConfig').empty();
				 $('.onOff').attr('style','display:none');
			 }
		 }
	    	},
	    	error: function(e) {
	    		
	    		alert("Please try again later");
	    	}
	    });
	
});

$('#viewStandardSettings').click(function(){
	
	var url=urlForServer +'admin/viewDefaultConfig';
	
	$.ajax({ headers: { 
		"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn	
	},
    	type: 'POST',
    	url : url,
    	cache: false,
    //	data:params,
    	contentType: "application/json; charset=utf-8",
    //	dataType: "json",
    	success : function(response){
		if(response!="" && response!=undefined){
			
		$('#configUpdate').attr('style','display:block;float:left;');
	 	$('#configSubmit').attr('style','display:none');
	 	var resData=jQuery.parseJSON(response);
	 	var uploadLimit=resData.uploadLimit;
	 	var configId=resData.configId;
	 		
	 	var payList=resData.paymentAmount;
	 	var splitConfig=payList[0];
	 	var pay=splitConfig.split("-");
	 	
	 	$('#configAmount').val(pay[0]);$('#configDays').val(pay[1]);
	 	var splitConfig1=payList[1];
	 	var pay1=splitConfig1.split("-");
	 	$('#configAmount1').val(pay1[0]);$('#configDays1').val(pay1[1]);
	 	
	 	var splitConfig2=payList[2];
	 	var pay2=splitConfig2.split("-");
	 	$('#configAmount2').val(pay2[0]);$('#configDays2').val(pay2[1]);
	 	
	 	var splitConfig3=payList[3];
	 	var pay3=splitConfig3.split("-");
	 	$('#configAmount3').val(pay3[0]);$('#configDays3').val(pay3[1]);
	 	
	 	var splitConfig4=payList[4];
	 	var pay4=splitConfig4.split("-");
	 	$('#configAmount4').val(pay4[0]);$('#configDays4').val(pay4[1]);
	 	
	 	
	 	$('#uploadLimit').val(uploadLimit);
	 	$('#uploadLimit').attr('class','form-control ~'+configId+'');
		}
		
    	},
    	error: function(e) {
    		
    		alert("Please try again later");
    	}
    });
	
});

function updateDefaultConfiguration(){
	
	var payment=[];
	var configId;
	var split=$('#uploadLimit').attr('class').split('~');
	configId=split[1];
	payment.push($('#configAmount').val()+'-'+$('#configDays').val(),$('#configAmount1').val()+'-'+$('#configDays1').val(),$('#configAmount2').val()+'-'+$('#configDays2').val());
	
	if($('#configAmount3').val() !="" && $('#configDays3').val()!="" ){
		payment.push($('#configAmount3').val()+'-'+$('#configDays3').val());
	}
	if($('#configAmount4').val()!="" && $('#configDays4').val()!=""){
		payment.push($('#configAmount4').val()+'-'+$('#configDays4').val());
	}
			
	var uploadLimit=$('#uploadLimit').val();
	
	var params='{"paymentAmountDays":"'+payment+'","uploadLimit":"'+uploadLimit+'","userId":"'+userId+'","status":"'+"A"+'"}';
	var url=urlForServer +'admin/updateDefaultConfig/'+configId;
	
	 $.ajax({ headers: { 
		 "Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn	
	 },
	    	type: 'POST',
	    	url : url,
	    	cache: false,
	    	data:params,
	    	contentType: "application/json; charset=utf-8",
	    	//dataType: "json",
	    	success : function(response){
		 	addStandardSetting();
	    	},
	    	error: function(e) {
	    		
	    		alert("Please try again later");
	    	}
	    });
}

$('#configUpdate').click(function(){
	updateDefaultConfiguration();
});

$('#addStandardSettings').click(function(){
	addStandardSetting();
});

function addStandardSetting(){
	$('#configAmount').val('');$('#configAmount1').val('');$('#configAmount2').val('');
 	$('#configAmount3').val('');$('#configAmount4').val('');
 	$('#configDays').val('');$('#configDays1').val('');$('#configDays2').val('');
 	$('#configDays3').val('');$('#configDays4').val('');
 	$('#uploadLimit').val('');
 	$('#configUpdate').attr('style','display:none');
 	$('#configSubmit').attr('style','display:block;float:left;');
}
$('#configCancel').click(function(){
	addStandardSetting();
});


// change event for Chart reports

$('#chaneChart').change(function(){

	$('#errorConfig').empty();
	if($(this).val()=='barchart'){
				
		$('#barchatContainer').attr('style','display:block;margin-top:10px;');
		$("#sliders").show();
		barChart();
		$('#piechatContainer').attr('style','display:none');
		$('#linechatContainer').attr('style','display:none');
	
	}else if($(this).val()=='piechart'){
		
		pieChart();
		$('#barchatContainer').attr('style','display:none');
		$('#piechatContainer').attr('style','display:block;margin-top:10px;');
		$('#linechatContainer').attr('style','display:none');
		
	}
	/*else if($(this).val()=='linechart'){
		
		lineChart();
		$('#barchatContainer').attr('style','display:none');
		$('#piechatContainer').attr('style','display:none');
		$('#linechatContainer').attr('style','display:block;margin-top:10px;');
		
	}*/
	else{
		$('#barchatContainer').attr('style','display:none');
		$('#piechatContainer').attr('style','display:none');
		$('#linechatContainer').attr('style','display:none');
	}
	
	});

    // Set up the chart 3D bar chart
function barChart()
{
	var Premium=0;
	var Expiry=0;
	var Trial=0;
	if(allUserList!=null && allUserList!='undefined')
	{
		allUserList=allUserList;
	}
	else
	{
		allUserList=0;
	}
	if(TrialUserList!=null && TrialUserList!='undefined')
	{
		Trial=(TrialUserList/1)*1;
	}
	else
	{
		TrialUserList=0;
		Trial=0;
	}
	if(PremiumUserList!=null && PremiumUserList!='undefined')
	{
		Premium=(PremiumUserList/1)*1;
	}
	else
	{
		PremiumUserList=0;
		Premium=0;
	}
	if(ExpiredUserList!=null && ExpiredUserList!='undefined')
	{
		Expiry=(ExpiredUserList/1)*1;
	}
	else
	{
		ExpiredUserList=0;
		Expiry=0;
	}
    var chart = new Highcharts.Chart({
        chart: {
            renderTo: 'bar_chart',
            type: 'column',
            margin: 75,
            options3d: {
                enabled: true,
                alpha: 0,
                beta: 0,
                depth: 75,
                viewDistance: 25
            }
        },
        title: {
            text: 'Musicnote Users'
        },
        credits: {
            enabled: false
        },
       /* subtitle: {
            text: 'Test options by dragging the sliders below'
        },*/
        plotOptions: {
            column: {
                depth: 25
            }
        },
        xAxis: {
            categories: ['Premium', 'Trial' ,'Expiry'],
            title: {
                text: 'Package Type'
            }
        },
        yAxis: {
            title: {
                text: 'Number of Users'
            }
        },
        series: [{
        	showInLegend: false,
        	name: 'Users',
            data: [{
                name: 'Premium',
                color: '#0B610B',
                y: Premium,
                name:'Number of premium users: '+PremiumUserList
            }, {
                name: 'Trial',
                color: '#084B8A',
                y: Trial,
                name:'Number of trial users: '+TrialUserList
            },{
                name: 'Expiry',
                color: '#8A0808',
                y: Expiry,
                name:'Number of expired users: '+ExpiredUserList
            }]
        }]
    });


    // Activate the sliders
    $('#R0').on('change', function(){
        chart.options.chart.options3d.alpha = this.value;
        showValues();
        chart.redraw(false);
    });
    $('#R1').on('change', function(){
        chart.options.chart.options3d.beta = this.value;
        showValues();
        chart.redraw(false);
    });
    function showValues() {
        $('#R0-value').html(chart.options.chart.options3d.alpha);
        $('#R1-value').html(chart.options.chart.options3d.beta);
    }
    showValues();
}

function pieChart()
{
	var Premium=0;
	var Expiry=0;
	var Trial=0;
	if(TrialUserList!=null && TrialUserList!='undefined')
	{
		Trial=(TrialUserList/allUserList)*100;
	}
else
	{
	Trial=0;
	}
	if(PremiumUserList!=null && PremiumUserList!='undefined')
	{
		Premium=(PremiumUserList/allUserList)*100;
	}
else
	{
	Premium=0;
	}
	if(ExpiredUserList!=null && ExpiredUserList!='undefined')
	{
		Expiry=(ExpiredUserList/allUserList)*100;
	}
else
	{
	Expiry=0;
	}
         // pie chart
    $('#pie_chart').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false
        },
        title: {
            text: 'Musicnote Users'
        },
        tooltip: {
    	    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
       
        series: [{
            type: 'pie',
            name: 'Music users',
            data: [
                ['Premium',   Premium],
               
                {
                    name: 'Expiry',
                    y: Expiry,
                    sliced: true,
                    selected: true
                },
                ['Trial', Trial]
                
            ]
        }]
    });
}
    
function lineChart()
{
	var Premium=0;
	var Expiry=0;
	var Trial=0;
	if(TrialUserList!=null && TrialUserList!='undefined')
	{
	Trial=(TrialUserList/allUserList)*100;
	}
else
	{
	Trial=0;
	}
	if(PremiumUserList!=null && PremiumUserList!='undefined')
	{
		Premium=(PremiumUserList/allUserList)*100;
	}
else
	{
	Premium=0;
	}
	if(ExpiredUserList!=null && ExpiredUserList!='undefined')
	{
		Expiry=(ExpiredUserList/allUserList)*100;
	}
else
	{
	Expiry=0;
	}
    $('#line_chart').highcharts({
        title: {
            text: 'Musicnote users',
            x: -20 //center
        },
        subtitle: {
            text: 'Source: www.musicnoteapp.com',
            x: -20
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories: ['Premium', 'Trial', 'Expiry']
        },
        yAxis: {
            title: {
                text: 'Percentage'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: '%'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: 'Music users',
            data: [Premium, Trial, Expiry]
        }]
    });
}

function filterUserDetailsForChartView(){
	var userLevel=$("#userLevels").val();
	var userRole=$('#userRole').val();
	var url = urlForServer + "admin/fetchUserDetailsForChartView";
	var datastr = '{"userLevel":"all"}';
	
	var params = encodeURIComponent(datastr);
	$.ajax({ headers: {
		 "Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn		
	},
		
				type : 'POST',
				url : url,
				data : params,
				success : function(responseText) 
				{
					var data1 = "";
					var dataLength="";
					data1 = jQuery.parseJSON(responseText);
					PremiumUserList=data1.PremiumUserList;
					TrialUserList=data1.TrialUserList;
					ExpiredUserList=data1.ExpiredUserList;
					allUserList=data1.allUserList;
				}

	});
}

///////////////////////////////For add mail templates////////////////////////////////////////////
$('#addTemplateSubmit').click(function(){
	addTemplateSubmit();
});
$('#addTemplateCancel').click(function(){
	$('#templateName').val("");
	$('#templateContent').val("");
	$("#selectErrorMsg1").attr('style','display:none;');
	$("#selectErrorMsg2").attr('style','display:none;');
});
$('#addTemplate').click(function(){
	$('#templateName').val("");
	$('#templateContent').val("");
	$('#updateTemplateSubmit').attr('style','display:none;');
	$('#addTemplateSubmit').attr('style','display:inline-block;');
	$("#selectErrorMsg1").attr('style','display:none;');
	$("#selectErrorMsg2").attr('style','display:none;');
});
function addTemplateSubmit(){
	var templateName=$("#templateName").val();
	var templateContent=$("#templateContent").val();
	templateContent=templateContent.replace(/\<br \/\>/g, "\n");
	templateContent=templateContent.replace(/\n\r?/g, '<br />');
	if(templateName!=""){
		if(templateContent!=""){
			var url = urlForServer + "admin/addMailTemplates";
			var datastr = '{"templateName":"'+templateName+'","templateText":"'+templateContent+'"}';
			var params = encodeURIComponent(datastr);
			$.ajax({ headers: {
				"Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn	
			},
				
						type : 'POST',
						url : url,
						data : params,
						success : function(responseText) 
						{
							$("#moveCopyWarning").modal('show');
							$("#move-modal-message1").text("Template Added Successfully");
							$("#templateName").val("");
							$("#templateContent").val("");
							fetchMailTemplates();
						}

			});
		
		}else{
			$("#selectErrorMsg1").attr('style','display:none;');
			$("#selectErrorMsg2").attr('style','display:block;color:red');
		}
	}else{
		$("#selectErrorMsg2").attr('style','display:none;');
		$("#selectErrorMsg1").attr('style','display:block;color:red');
		
	}
	
	
}



function updateTemplateSubmit(templateId){
	var templateName=$("#templateName").val();
	var templateContent=$("#templateContent").val();
	templateContent=templateContent.replace(/\<br \/\>/g, "\n");
	templateContent=templateContent.replace(/\n\r?/g, '<br />');
	if(templateName!=""){
		if(templateContent!=""){
			$("#selectErrorMsg2").attr('style','display:none;');
			$("#selectErrorMsg1").attr('style','display:none;');
			var url = urlForServer + "admin/updateMailTemplates";
			
			var datastr = '{"templateName":"'+templateName+'","templateText":"'+templateContent+'","templateId":"'+templateId+'"}';
			var params = encodeURIComponent(datastr);
			$.ajax({ headers: { 
				"Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn	
			},
				
						type : 'POST',
						url : url,
						data : params,
						success : function(responseText) 
						{
							$("#moveCopyWarning").modal('show');
							$("#move-modal-message1").text("Template Updated Successfully");
							$("#templateName").val("");
							$("#templateContent").val("");
							fetchMailTemplates();
							$('#updateTemplateSubmit').removeAttr('disabled');
						}

			});
		
		}else{
			$('#updateTemplateSubmit').removeAttr('disabled');
			$("#selectErrorMsg1").attr('style','display:none;');
			$("#selectErrorMsg2").attr('style','display:block;color:red');
		}
	}else{
		$('#updateTemplateSubmit').removeAttr('disabled');
		$("#selectErrorMsg2").attr('style','display:none;');
		$("#selectErrorMsg1").attr('style','display:block;color:red');
		
	}
	
	
}


$('#addTemplateView').click(function(){
	fetchMailTemplates();
});

function fetchMailTemplates(){
	$("#viewAddedTemplates").empty();
			var url = urlForServer + "admin/fetchMailTemplates";
			var datastr = '{"userId":"'+userId+'"}';
			var params = encodeURIComponent(datastr);
			$.ajax({ headers: { 
				"Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn	
			},
				
						type : 'POST',
						url : url,
						data : params,
						success : function(responseText) 
						{
							data1 = jQuery.parseJSON(responseText);
							for(var i=0;i<data1.length;i++){
								if(data1[i].templateText.indexOf('<br />')){
									data1[i].templateText=data1[i].templateText.replace(/\<br \/\>/g, "\n");
								}
							var eventData='<div  id="'+data1[i].templateId+'" class="emailUpdate" title="'+data1[i].templateText+'" style="color:grey;font-size:20px;"><a class="templateTitle" id="'+data1[i].templateId+data1[i].templateId+'">'+data1[i].templateName+'</a></div>';
							$("#viewAddedTemplates").append('<a>'+eventData+'</a>');
							}
						}

			});
		
			
	
}
$('#viewAddedTemplates').on('click','.emailUpdate',function(){
	$('#addTemplateSubmit').attr('style','display:none;');
	$('#updateTemplateSubmit').attr('style','display:inline-block;');
	$('#templateName').val("");
	$('#templateContent').val("");
	$("#selectErrorMsg2").attr('style','display:none;');
	$("#selectErrorMsg1").attr('style','display:none;');
	$(".templateTitle").removeAttr('style','color:grayText');
	templateId=$(this).attr('id');
	var templateText=$(this).attr('title');
	$("#"+templateId+templateId).attr('style','color:grayText');
	var templateName=$(this).text();
			$('#templateName').val(templateName);
			$('#templateContent').val(templateText);
			
			
});
$('#updateTemplateSubmit').click(function(){
	$('#updateTemplateSubmit').attr('disabled','disabled');
	updateTemplateSubmit(templateId);
});
///////////////////////////////add mail templates end////////////////////////////////////////////

///////////////////////////////For add security Questions////////////////////////////////////////
//add security question
$('#securityQuestionSubmit').click(function(){
	if($("#addSecurityQuestion").val()==""){
		$("#selectErrorMsgs").attr('style','display:block;color:red');
	}else{
		$("#selectErrorMsgs").attr('style','display:none;color:red');
		$("#securityQuestionmessages").text("Are you sure to add this security question");
		$('#securityQuestionModalOk').show();
		$('#securityQuestionModalCancel').show();
		$('#securityQuestionModal').modal('show');
	}
});
//conform to add security question
$('#securityQuestionModalOk').click(function(){
	addSecurityQuestionSubmit();
	$('#securityQuestionModal').modal('hide');
});
//cancel operation for security question
$('#securityQuestionModalCancel').click(function(){
	$('#securityQuestionModal').modal('hide');
});
$('#securityQuestionCancel').click(function(){
	$('#addSecurityQuestion').val("");
	$("#selectErrorMsgs").attr('style','display:none;');
});
$('#addSecurityQuestionView').click(function(){
	$('#addSecurityQuestion').val("");
	$('#updatesecurityQuestionSubmit').attr('style','display:none;');
	$('#securityQuestionSubmit').attr('style','display:inline-block;');
});
function addSecurityQuestionSubmit(){
	
		$('#selectErrorMsgs').attr('style','display:none;color:red');
				var url = urlForServer + "admin/adminAddSecurityQuestions";
				var datastr = '{"question":"'+$('#addSecurityQuestion').val()+'"}';
				var params = encodeURIComponent(datastr);
				$.ajax({ headers: { 
					"Mn-Callers" : musicnote,
			    	"Mn-time" :musicnoteIn	
				},
					
							type : 'POST',
							url : url,
							data : params,
							success : function(responseText)
							{
								if(responseText=='success'){
								$("#moveCopyWarning").modal('show');
								$("#move-modal-message1").text("Question Added Successfully");
								$("#addSecurityQuestion").val("");
								fetchsecurityQuestionView();
								}
							},
				error: function(e) {
		    		alert("Please try again later");
		    	}
				});
}

/*$('#addSecurityQuestions').click(function(){
	fetchsecurityQuestionView();
});*/

function fetchsecurityQuestionView(){
	$('#securityQuestionSubmit').attr('style','display:inline-block;');
	$('#updatesecurityQuestionSubmit').attr('style','display:none;');
	$('#deletesecurityQuestionSubmit').attr('style','display:none;');
	$('#newsecurityQuestionSubmit').attr('style','display:none;');
	$('#securityQuestionCancel').attr('style','display:inline-block;');
	$("#addSecurityQuestion").val("");
	$('#updateSecurityQuestionModalOk').hide();
	$('#securityQuestionModalCancel').hide();
	$('#securityQuestionModalInfo').hide();
	$('#securityQuestionModalOk').hide();
	$("#viewSecurityQuestions").empty();
			var url = urlForServer + "admin/fetchsecurityQuestionView";
			var datastr = '{"userId":"'+userId+'"}';
			var params = encodeURIComponent(datastr);
			$.ajax({ headers: { 
				"Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn	
			},
				
						type : 'POST',
						url : url,
						data : params,
						success : function(responseText) 
						{
							data1 = jQuery.parseJSON(responseText);
							for(var i=0;i<data1.length;i++){
							var eventDatas='<div  id="'+data1[i].qId+'" class="securityQuestionView" title="'+data1[i].QuestionId+'" style="color:grey;font-size:20px;"><a class="secureQuestion" id="'+data1[i].qId+data1[i].qId+'">'+data1[i].Question+'</a></div>';
							$("#viewSecurityQuestions").append('<a>'+eventDatas+'</a>');
							}
						}
			});
}
//check SecurityQuestion editable or not  
$('#viewSecurityQuestions').on('click','.securityQuestionView',function(){
	$('#addSecurityQuestion').val("");
	var url = urlForServer + "admin/checkSecurityQuestion";
	QuestionId=$(this).attr('title');
	var Question=$(this).text();
	qId=$(this).attr('id');
	var datastr = '{"questionId":"'+QuestionId+'"}';
	var params = encodeURIComponent(datastr);
	$.ajax({ 
		headers: { 
		"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn	
	},
		type : 'POST',
		url : url,
		data : params,
		success : function(responseText) 
		{
			$(".secureQuestion").removeAttr('style','color:graytext');
			if(responseText=='success')
			{
				$("#"+qId+qId).attr('style','color:graytext');
				$('#securityQuestionSubmit').attr('style','display:none;');
				$('#securityQuestionCancel').attr('style','display:none;');
				$('#selectErrorMsgs').attr('style','display:none;color:red');
				$('#updatesecurityQuestionSubmit').attr('style','display:inline-block;');
				$('#deletesecurityQuestionSubmit').attr('style','display:inline-block;');
				$('#newsecurityQuestionSubmit').attr('style','display:inline-block;');
				$('#addSecurityQuestion').val(Question);
			}
			else
			{
				$('#securityQuestionSubmit').attr('style','display:inline-block;');
				$('#updatesecurityQuestionSubmit').attr('style','display:none;');
				$('#deletesecurityQuestionSubmit').attr('style','display:none;');
				$('#newsecurityQuestionSubmit').attr('style','display:none;');
				$('#securityQuestionCancel').attr('style','display:inline-block;');
				$("#securityQuestionmessages").text("This question can't be modify because this question is used by some one");
				$('#securityQuestionModalInfo').show();
				$('#securityQuestionModal').modal('show');
			}
		}
	});
});
//when hide security question modal panel
$('#securityQuestionModal').on('hidden.bs.modal', function () {
	$('#updateSecurityQuestionModalOk').hide();
	$('#securityQuestionModalCancel').hide();
	$('#securityQuestionModalInfo').hide();
	$('#securityQuestionModalOk').hide();
});
//Insert new question
$("#newsecurityQuestionSubmit").click(function(){
	$("#selectErrorMsgs").attr('style','display:none;color:red');
	$('#updatesecurityQuestionSubmit').attr('style','display:none;');
	$('#deletesecurityQuestionSubmit').attr('style','display:none;');
	$('#newsecurityQuestionSubmit').attr('style','display:none;');
	$('#securityQuestionCancel').attr('style','display:inline-block;');
	$('#securityQuestionSubmit').attr('style','display:inline-block;');
	$('#addSecurityQuestion').val("");
});
//alert for used question
$("#securityQuestionModalInfo").click(function(){
	$('#securityQuestionModal').modal('hide');
});
//update security question
$('#updatesecurityQuestionSubmit').click(function(){
	if($("#addSecurityQuestion").val()==""){
		$("#selectErrorMsgs").attr('style','display:block;color:red');
	}else{
		operationType="update";
		$("#securityQuestionmessages").text("Are you sure to update this security question ");
		$('#updateSecurityQuestionModalOk').show();
		$('#updateSecurityQuestionConfirm').show();
		$('#securityQuestionModalCancel').show();
		$('#securityQuestionModal').modal('show');
	}
});
//delete security question
$('#deletesecurityQuestionSubmit').click(function(){
	if($("#addSecurityQuestion").val()==""){
		$("#selectErrorMsgs").attr('style','display:block;color:red');
	}else{
		operationType="delete";
		$("#securityQuestionmessages").text("Are you sure to delete this security question ");
		$('#updateSecurityQuestionModalOk').show();
		$('#deleteSecurityQuestionConfirm').show();
		$('#securityQuestionModalCancel').show();
		$('#securityQuestionModal').modal('show');
	}
});
//update security question confirmation
$("#updateSecurityQuestionModalOk").click(function(){
	$('#deletesecurityQuestionSubmit').attr('style','display:none;');
	$('#newsecurityQuestionSubmit').attr('style','display:none;');
	$('#updatesecurityQuestionSubmit').attr('style','display:none;');
	$('#securityQuestionSubmit').attr('style','display:inline-block;');
	$('#securityQuestionCancel').attr('style','display:inline-block;');
	$('#selectErrorMsgs').attr('style','display:none;color:red');
	$('#securityQuestionModal').modal('hide');
	updateSecurityQuestion(qId,QuestionId);
});

//function for update and delete security Question
function updateSecurityQuestion(qId,QuestionId){
	var addSecurityQuestion=$("#addSecurityQuestion").val();
		$("#selectErrorMsgs").attr('style','display:none;color:red');
			var url = urlForServer + "admin/updateSecurityQuestions";
			var datastr = '{"question":"'+addSecurityQuestion+'","qId":"'+qId+'","questionId":"'+QuestionId+'","operationType":"'+operationType+'"}';
			var params = encodeURIComponent(datastr);
			$.ajax({ headers: { 
				"Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn	
			},
				
						type : 'POST',
						url : url,
						data : params,
						success : function(responseText) 
						{
							$("#moveCopyWarning").modal('show');
							if(operationType=="update")
								$("#move-modal-message1").text("Question Updated Successfully");
							else if(operationType=="delete")
								$("#move-modal-message1").text("Question Deleted Successfully");	
							$("#addSecurityQuestion").val("");
							fetchsecurityQuestionView();
							$('#updatesecurityQuestionSubmit').removeAttr('disabled');
							$('#updatesecurityQuestionSubmit').attr('style','display:none;');
							$('#securityQuestionSubmit').attr('style','display:inline-block;');
						}

			});
}
//view Blocked user List when Close block Conform Modal
$('#blockConformModal').on('hidden.bs.modal', function () {
	viewBlockedUsers();
});
$('#DeleteFileConfirmModal').on('hidden.bs.modal', function () {
	viewUploadedFile();
});
	//Blocked modal panel conform button	
 $("#complaintReportsOk").click(function(){
	 $('#blockConformModal').modal('hide');
	 var url=urlForServer+"admin/insertBlockedReport";
	 var userName=$("#userName").val();
	 var userId=$("#userId").val();
	 var adminId=$('#adminId').val();
	 var selectBlockStatus=$("#blockStatus").val();
	 var data='{"userName":"'+userName+'","userId":"'+userId+'","selectBlockStatus":"'+selectBlockStatus+'","adminId":"'+adminId+'"}';
	 var params = encodeURIComponent(data);
	
		$.ajax(
		{
			headers:
			{ 
				"Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn					
			},
			data:params,
			url:url,
			type:'post',
			success:function(responseText)
			{
			},
			error: function(e) {
	    		
	    		alert("Please try again later");
	    	}
		});
 });
 //upload success alert
 $("#uploadSuccessOk").click(function()
		 {
	 		$('#uploadSuccessModal').modal('hide');
		 });
 //Blocked modal panel cancel button  
 $("#complaintReportsCancel").click(function()
		 {
	 		$('#blockConformModal').modal('hide');
		 });
 //view blocked List When select dropDown List
 $('#selectBlock').change(function()
		{
	 		viewBlockedUsers();
		});
//cancel confirmation modal
 $("#DeleteFileConfirmCancel").click(function()
		 {
	 		$('#DeleteFileConfirmModal').modal('hide');
		 });
 //okay confirmation modal
$("#DeleteFileConfirmOk").click(function()
		 {
			var url=urlForServer+'admin/deleteUploadedFile/'+$('#fileId').val();
			$.ajax(
					{
						headers:
						{ 
							"Mn-Callers" : musicnote,
					    	"Mn-time" :musicnoteIn					
						},
						url:url,
						type:'post',
						success:function(responseText)
						{
						},
						error: function(e) {
				    		
				    		alert("Please try again later");
				    	}
					});
	 		$('#DeleteFileConfirmModal').modal('hide');
	 		viewUploadedFile();
		 });
//blocked user conform to add user preferences 
$("#addUserPreferenceConformButton").click(function(){
	$('#addUserFlagModal').modal('hide');
	var url = urlForServer + "admin/addUserFlagOnMnUser/"+addPreferenceUserId+"/"+userFlag;
	$.ajax({
		headers: { 
		  "Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn	
	  },
	  	 url: url,
         type: "POST",
         success: function(data)
         {
         },
         error:function(){
				console.log("<-------add column UserFlag in MnUser error-------> ");
				}
	});
});
$("#addUserPreferenceButtonCancel").click(function(){
	$('#addUserFlagModal').modal('hide');
	userAddPreferencesGrid();
});

//Uploading Video files
	var extArray=['mov','mp4','avi','wmv','wma','mp3','wav','amr','pdf'];
	var ext;
	function file_get_ext(filename)
	{
		return typeof filename != "undefined" ? filename.substring(filename.lastIndexOf(".")+1, filename.length).toLowerCase() : false;
	}
	$("#upload").submit(function()
			{
				$("#uploadBtn").attr('disabled','disabled');
				$("#valueNotInserted").hide();
				ext=file_get_ext($("#fileChoose").val());
				$("#upload").removeAttr("action",url);
				if($("#videoDescription").val()!="" && $("#videoTitle").val()!="" && $("#fileChoose").val()!="" && extArray.indexOf(ext)!= -1)
				{
					var url=urlForServer+'admin/uploadFiles/'+adminUploadToken;
					$("#upload").attr("action",url);
				}
			});
			
			var options = 
			{
				beforeSend: function()
				{
					$("#valueNotInserted").hide();
					$("#noFilesSelected").hide();
					$("#errorFileFormat").hide();
					ext=file_get_ext($("#fileChoose").val());
					if($("#videoDescription").val()!="" && $("#videoTitle").val()!="" && $("#fileChoose").val()!="" && extArray.indexOf(ext)!= -1)
					{
						$("#progress").show();
					}
			        //clear everything
			        $("#bar").width('0%');
			        $("#message").html("");
			        $("#percent").html("0%");
			        $('.progress-bar').progressbar();
				},
				uploadProgress: function(event, position, total, percentComplete)
				{
					 $("#bar").width(percentComplete+'%');
				     $("#percent").html(percentComplete+'%');
				},
				success: function(response)
				{
					$("#bar").width('100%');
			        $("#percent").html('100%');
				},
				complete: function(response) 
				{
					if($("#videoDescription").val()!='' && $("#videoTitle").val()!='' )
					{
						if($("#fileChoose").val()!="")
						{
							ext=file_get_ext($("#fileChoose").val());
							if(extArray.indexOf(ext)!= -1)
							{
								var responseValue=response.responseText;
								var responseData = jQuery.parseJSON(responseValue);
								var description=$("#videoDescription").val().trim();
								description=description.replace(/\<br \/\>/g, "\n");
								description=description.replace(/\n\r?/g, '<br />');
								description=description.replace( /[\s\n\r]+/g, ' ' );
								var data1='{"description":"'+escape(description)+'","path":"'+responseData[0].filePath+'","type":"'+responseData[0].fileType+'","name":"'+responseData[0].fileName+'","title":"'+$("#videoTitle").val()+'"}';
								var url=urlForServer+'admin/insertFileDetails';
								$.ajax(
									 	{
									 		headers:
									 		{ 
									 			"Mn-Callers" : musicnote,
									 	    	"Mn-time" :musicnoteIn				
									 		},
									 		url:url,
									 		type:'post',
									 		data:data1,
									 		success:function(responseText)
									 		{
												viewUploadedFile();
												$("#uploadBtn").removeAttr('disabled','disabled');
												$('#uploadSuccessModal').modal('show');
									 		}
									 	});
							}
							else
							{
								$("#errorFileFormat").show();
								$("#uploadBtn").removeAttr('disabled','disabled');
							}
						}
						else
						{
							$("#uploadBtn").removeAttr('disabled','disabled');
							$("#noFilesSelected").show();
						}
					}
					else
					{
						$("#uploadBtn").removeAttr('disabled','disabled');
						$("#valueNotInserted").show();
					}
					
				}
			};		
			$("#upload").ajaxForm(options);
			
/////////////////Blog page update and delete options//////////////////	
			
			
			$('#addNewBlog').click(function(){
				$("#blogTitle").val("");
				$(".note-editable").empty();
				$('.blogTitle').removeAttr('style','color:graytext');
				$('#updateBlog').attr('style','display:none;');
				$('#deleteBlog').attr('style','display:none;');
				$('#addBlog').attr('style','display:inline-block;');
				$("#blogDescriptionMsgs").attr('style','display:none;');
			});	
			
			/*$('#viewBlog').click(function(){
				fetchBlogDetails();
			});*/
			
			
			$('#blogdetails').on('click','.blogUpdate',function(){
				$('#addBlog').attr('style','display:none;');
				$('#updateBlog').attr('style','display:inline-block;');
				$('#deleteBlog').attr('style','display:inline-block;margin-left:10px;');
				$("#blogTitle").val("");
				$(".note-editable").empty();
				$("#blogDescriptionMsgs").attr('style','display:none;');
				blogId=$(this).attr('id');
				var blogTitle=$(this).attr('title');
				var blogDesc=blogArray[blogId];
				$('.blogTitle').removeAttr('style','color:graytext');
				$('#'+blogId+blogId).attr('style','color:graytext');
						$('#blogTitle').val(blogTitle);
						$('.summernote').code(blogDesc);
			});
			
			 $("#BlogSuccessModal").on('click','#BlogOk',function()
					 {
				 		$('#deleteBlog').attr('style','display:none');
				 		$('#updateBlog').attr('style','display:none');
				 		$('#addBlog').removeAttr('style','display:none;');
				 		$('#BlogSuccessModal').modal('hide');
					 });
			 $('.blogEditor').on('click','#deleteBlog',function(){
				 if($('#blogTitle').val()!='' )
				 {
					 $('#addwarningmessages').text("Are you sure to delete this blog ");
					 $('#BlogwarningModal').modal('show');
				 }
				});
			 $('.blogEditor').on('click','#updateBlog',function(){
				 updateBlog(blogId);
				});
			 $('#BlogYes').click(function(){
					deleteBlog(blogId);
					 $('#BlogwarningModal').modal('hide');
				});
			 $('#BlogNo').click(function(){
				 $('#BlogwarningModal').modal('hide');
				});
			 $('.BlogSuccessModal').on('click','#closeblogModel',function(){
				 $('#deleteBlog').attr('style','display:none');
			 		$('#updateBlog').attr('style','display:none');
			 		$('#addBlog').removeAttr('style','display:none;');
			 		$('#BlogSuccessModal').modal('hide');
				});
			 $('.modalHeader').on('click','#closeblogwarningModel',function(){
				 $('#BlogwarningModal').modal('hide');
				});
	function updateBlog(blogId)
	{	
				$("#blogDescriptionMsgs").attr('style','display:none;');
			 	if($('#blogTitle').val()!="" && $('.summernote').code()!="" && $('.summernote').code()!="<br>" )
			 	{
			 		var blogTitle=$("#blogTitle").val();
			 		var blogDescription=$(".summernote").code();
			 		blogDescription = blogDescription.split("&lt;").join("$-$").split("&gt;").join("$*$").split("&").join("$@$").split("+")
			 											.join("$!$").split("%").join("$^$").split("~").join("$@#$");
			 		var data='"Title"~~'+blogTitle+'~~"blogId"~~'+blogId+'~~"adminId"~~'+userId+'~~"blogDescription"~~'+blogDescription;
			 		var params = data;
			 		var url=urlForServer +"admin/updateBlogDetails";
			 		$.ajax({
			 			 headers: { 
			 			"Mn-Callers" : musicnote,
			 	    	"Mn-time" :musicnoteIn	
			 		},
			 		  	 url: url,
			 		  	 data:params,
			 	         type: "POST",
			 	         success: function(responseText)
			 	         {
			 	        	 $("#blogDescriptionMsgs").attr('style','display:none;');
			 	        	 $('#addBlogmessages').text("Blog Updated Successfully");
			 					$('#BlogSuccessModal').modal('show');
			 					$('#deleteBlog').attr('style','display:none');
						 		$('#updateBlog').attr('style','display:none');
						 		$('#addBlog').removeAttr('style','display:none;');
						 		fetchBlogDetails();
			 					setTimeout(function(){
					     			$('#BlogSuccessModal').modal('hide');	
					     		},3000);
			 	         },
			 		  error:function()
			 			{
			 				alert("error");
			 			}
			 		});
			 		$("#blogTitle").val("");
			 		$(".note-editable").empty();
			 	}
			 	else
			 		{
			 			$("#blogDescriptionMsgs").attr('style','display:block;color:red;');
			 		}
			 	
		 }	
	
	function deleteBlog(blogId)
	{
	 		var url=urlForServer +"admin/deleteBlogDetails/"+blogId;
	 		$.ajax({
	 			 headers: { 
	 			"Mn-Callers" : musicnote,
	 	    	"Mn-time" :musicnoteIn	
	 		},
	 		  	 url: url,
	 	         type: "POST",
	 	         success: function(responseText)
	 	         {
	 	        	 $("#blogDescriptionMsgs").attr('style','display:none;');
	 	        	 $('#addBlogmessages').text("Blog Deleted Successfully");
	 					$('#BlogSuccessModal').modal('show');
	 					$('#deleteBlog').attr('style','display:none');
				 		$('#updateBlog').attr('style','display:none');
				 		$('#addBlog').removeAttr('style','display:none;');
				 		fetchBlogDetails();
	 					setTimeout(function(){
			     			$('#BlogSuccessModal').modal('hide');	
			     		},3000);
	 	         },
	 		  error:function()
	 			{
	 				alert("error");
	 			}
	 		});
	 		$("#blogTitle").val("");
	 		$(".note-editable").empty();
	}
	 
	
});
//uploaded file Details
function viewUploadedFile()
{
	$("#valueNotInserted").hide();
	$("#fileChoose").val("");
	$("#upload-file-info").html("No file selected");
	$("#noFilesSelected").hide();
	$("#errorFileFormat").hide();
	$("#progress").hide();
	$('#videoTitle').val("");
	$('#videoDescription').val("");
	$('#videoGrid').empty();
	var url=urlForServer+"admin/getUploadedFiles";
	$.ajax(
		 	{
		 		headers:
		 		{ 
		 			"Mn-Callers" : musicnote,
		 	    	"Mn-time" :musicnoteIn				
		 		},
		 		url:url,
		 		type:'post',
		 		success:function(responseText)
		 		{
		    		if(responseText=='failed')
		 			{
		    			$("#noVideoFound").attr('style','display:block;color:red;margin-left:170px;margin-top:60px;');	
		 			}
		 			else
		 			{
		 				$("#noVideoFound").attr('style','display:none');
		 				var data1 = "";
		 				data1 = jQuery.parseJSON(responseText);
		 				$('#videoGrid').append('<table id="tableId" ></table><div id="tableValues"></div>');
							$("#tableId").jqGrid(
							{
								data:data1,
					       		sortable: true,       		
					       		height:200,	        	
					       	    width:650,
					       	    datatype: "local",
					       	    colNames:['File Name','Title','Description','Status','Option','File Id'],
								colModel:[{	name:'fileName',index:'1',width:150,align:'center'},
								          {name:'fileTitle',index:'2',width:150,align:'center'},
								          {name:'description',index:'3',width:150,align:'center'},
								          {name:'status',index:'4',width:150,align:'center'},
								          {name:'option',index:'3', width:'90',align:'center',edittype:'select',editable: true,formatter:'select',
			 									cellattr: function(cellValue, options, rowdata) 
			 									{
			 										return "><input type='button' value='Delete' id='deleteVideo"+cellValue+"' onclick='javascript:deleteAdminVideo("+cellValue+");'></input"; 
			 									}},
			 									{name: 'fileId',width:'50',index:'1',width:150,align:'center',hidden:true}],
								pager: '#tableValues',
								viewrecords: true,
				   	        	rownumbers:true, 
				   	        	pginput: "false",
				   	        	rowNum:9,
				   	        	loadComplete: function () 
 				   	        	{ 
									var ids = $("#tableId").jqGrid('getDataIDs');
									var l = ids.length;
	 					            for (var i = 1; i <= l; i++) 
	 					            {
	 					            	var statusValue=$("#tableId").jqGrid ('getCell', i, 'status');
	 					            	if(statusValue=="I")
	 					            	{
	 					            		$('#deleteVideo'+i).attr( "disabled","true");
	 					            	}
	 					            }
								
 				   	        	}
							});
							$("#tableId").jqGrid('navGrid',"#tableValues",{edit:false,add:false,del:false,search:false});
		 			}
		 		}
		 	});
}
function deleteAdminVideo(cellValue)
{
	$('#DeleteFileConfirmModal').modal('show');
	$("#fileId").val(jQuery("#tableId").jqGrid('getCell', cellValue, 'fileId'));
}

//view Blocked Users Details
function viewBlockedUsers()
{
	$("#noRecordFound").attr('style','display:none');
	$('#jQGridDiv').empty();
	 	$('#jQGridDiv').attr('style','display:block;height: 600px;');
	 	var lastsel2;
	 	var selectBlockOption=$("#selectBlock").val();
	 	var data='{"selectBlockOption":"'+selectBlockOption+'"}';
	 	var params = encodeURIComponent(data);
	 	var url=urlForServer+"admin/getBlockedReport";
	 	$.ajax(
	 	{
	 		headers:
	 		{ 
	 			"Mn-Callers" : musicnote,
	 	    	"Mn-time" :musicnoteIn				
	 		},
	 		url:url,
	 		data:data,
	 		type:'post',
	 		success:function(responseText)
	 		{
	 			if(responseText=='')
	 			{
	 			 	if(selectBlockOption=='select')
	 			 	{
	 			 		$('#noRecordFound').attr('style','display:none;');
	 			 	}
	 			 	else{
	 			 		$('#noRecordFound').attr('style','display:block;color:red;margin-left:170px;margin-top:60px;');
	 			 	}
	 			}
	 			else
	 			{
	 				$("#noRecordFound").hide();	
	 				var data1 = "";
	 				var dataLength="";
	 				var i=0;
	 				data1 = jQuery.parseJSON(responseText);
	 							$('#jQGridDiv').append('<table id="taskTablesss" ></table><div id="tablePagesss"></div>');
	 							$("#taskTablesss").jqGrid(
	 							{
	 								data:data1,
	 					       		sortable: true,       		
	 					       		height:200,	        	
	 					       	    width:950,
	 					       	    datatype: "local",
	 								colNames:['User Name','Number of Complaints','Crowd Block','Blocking Mode','Blocked Date','UserId','blockUser','blockCrowdShare'],
	 								colModel:[{	name:'userName',index:'1',width:150,align:'center'},
	 								{name:'noOfComplaints',index:'2',width:'150',align:'center'},
	 								{name:'blockCrowd',index:'3', width:'90',align:'center',edittype:'select',editable: true,formatter:'select',/*editoptions:{value:"off:Off;on:On"}*/
	 									cellattr: function(cellValue, options, rowdata) 
	 									{
	 										return "><select id='optionConform"+cellValue+"' onchange='javascript:crowdStatusCheck("+cellValue+");'>" +
	 												"<option id='off' value='off'>off</option>" +
	 												"<option id='on' value='on'>on</option></select";
	 									}},
	 								{name:'fullblockingUser',index:'4',width:'150',align:'center',formatter:'select', 
	 									editable: true,edittype:"select",
	 									/*editoptions:{value:"upTo10days:UpTo 10 days;upTo20days:UpTo 20 days;fullyBlocked:Fully Blocked;off:Off"},*/
	 									cellattr: function(cellValue, options, rowdata) 
	 									{
	 										return "><select id='blockConform"+cellValue+"' disable onchange='javascript:checkBlockedDays("+cellValue+");'>" +
	 												"<option id='off' value='OFF'>off</option>" +
	 												"<option id='upTo10days' value='10'>UpTo 10 days</option>" +
	 												"<option id='upTo20days' value='20'>UpTo 20 days</option>" +
	 												"<option id='fullyBlocked' value='fullyBlocked'>Fully Blocked</option></select";
	 									}
	 									
	 									},
	 									{name:'blockedDate',index:'1',width:150,align:'center'},
	 									{name: 'userId',width:'50',index:'1',width:150,align:'center',hidden:true},
	 									{name: 'blockingUser',width:'50',index:'1',width:150,align:'center',hidden:true},
	 									{name: 'blockCrowdShare',width:'50',index:'1',width:150,align:'center',hidden:true}],
	 									pager: '#tablePagesss',
	 								 viewrecords: true,
	 				   	        	 rownumbers:true, 
	 				   	        	 pginput: "false",
	 				   	        	 rowNum:9,
	 				   	        	loadComplete: function () 
	 				   	        	{ 
		 					            ids = $("#taskTablesss").jqGrid('getDataIDs');
		 					            var l = ids.length;
		 					            for (var i = 1; i <= l; i++) 
		 					            {
		 					            	var blockstatus=$("#taskTablesss").jqGrid ('getCell', i, 'blockingUser');
		 					            	var blockCrowd=$("#taskTablesss").jqGrid ('getCell', i, 'blockCrowdShare');
		 					            	if(blockstatus!="OFF"&&blockstatus!="null")
		 					            	{
		 					            		$('#blockConform'+i).val(blockstatus);
		 					            		$('#optionConform'+i).attr( "disabled", "true");
		 					            	}
			 					            if(blockCrowd!="off" && blockCrowd!="null")
			 					           	{
			 					            	$('#optionConform'+i).val(blockCrowd);
			 					           	}
		 					            }
	 				   	        	}
	 							});
	 							$("#taskTablesss").jqGrid('navGrid',"#tablePagesss",{edit:false,add:false,del:false,search:false}); 
	 						}
	 					},
	 					error:function()
	 					{
	 						alert("error");
	 					}
	 					
	 				});
	 		
}
//update crowd Blocked user
function crowdStatusCheck(cellValue)
{
	$("#userName").val(jQuery("#taskTablesss").jqGrid('getCell', cellValue, 'userName'));
	$("#userId").val(jQuery("#taskTablesss").jqGrid('getCell', cellValue, 'userId'));
	$("#blockStatus").val($('#optionConform'+cellValue).val());
	if($('#optionConform'+cellValue).val()=='off')
	{
		$('#blockConform'+cellValue).removeAttr( "disabled", "disabled");
		$('#block').hide();
		$('#block20').hide();
		$('#block10').hide();
		$('#blockFully').hide();
		$('#unBlockFully').hide();
		$('#unBlock').show();
		$('#blockConformModal').modal('show');
	}
	else if($('#optionConform'+cellValue).val()=='on')
	{
		$('#blockConform'+cellValue).attr( "disabled", "disabled");
		$('#unBlock').hide();
		$('#block20').hide();
		$('#block10').hide();
		$('#blockFully').hide();
		$('#unBlockFully').hide();
		$('#block').show();
		$('#blockConformModal').modal('show');
	}
	else
	{
		$('#blockConform'+cellValue).removeAttr( "disabled", "disabled");		
	}
 }
//update All blocked user 
function checkBlockedDays(cellValue)
{
	$("#userName").val(jQuery("#taskTablesss").jqGrid('getCell', cellValue, 'userName'));
	$("#userId").val(jQuery("#taskTablesss").jqGrid('getCell', cellValue, 'userId'));
	$("#blockStatus").val($('#blockConform'+cellValue).val());
	if($('#blockConform'+cellValue).val()=='10'){
		$('#block').hide();
		$('#unBlock').hide();
		$('#block20').hide();
		$('#blockFully').hide();
		$('#unBlockFully').hide();
		$('#block10').show();
		$('#optionConform'+cellValue).attr( "disabled", "disabled");
	 $('#blockConformModal').modal('show');
	}
	else if($('#blockConform'+cellValue).val()=='20')
	{
		$('#block').hide();
		$('#unBlock').hide();
		$('#block10').hide();
		$('#blockFully').hide();
		$('#unBlockFully').hide();
		$('#block20').show();
		$('#optionConform'+cellValue).attr( "disabled", "disabled");
		$('#blockConformModal').modal('show');
	}
	else if($('#blockConform'+cellValue).val()=='fullyBlocked')
	{
		$('#block').hide();
		$('#unBlock').hide();
		$('#block20').hide();
		$('#block10').hide();
		$('#unBlockFully').hide();
		$('#blockFully').show();
		$('#optionConform'+cellValue).attr( "disabled", "disabled");
		$('#blockConformModal').modal('show');
	}
	else if($('#blockConform'+cellValue).val()=='OFF')
	{
		$('#block').hide();
		$('#unBlock').hide();
		$('#block20').hide();
		$('#block10').hide();
		$('#blockFully').hide();
		$('#unBlockFully').show();
		$('#optionConform'+cellValue).removeAttr( "disabled", "disabled");
		$('#blockConformModal').modal('show');
	}
	else
	{
		$('#optionConform'+cellValue).removeAttr( "disabled", "disabled");
	}
 }
//Add User Preferences jqGrid
function userAddPreferencesGrid()
{
		$('#addPreferencesTableGrid').empty();
		$('#addPreferencesTableGrid').append('<table id="mnUserTable" ></table><div id="addPreferenceTable"></div>');
		var url=urlForServer +"admin/getMnUserDetailsForUserPrefrence";
		$.ajax({
			 headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn	
		},
			url:url,
			type:'post',
			success:function(responseText)
			{
				var data1 = $.parseJSON(responseText);
		    
				$('#mnUserTable').jqGrid({
					data:data1,
					sortable: true,       		
					height:221,	        	
			       	width:1600,
			       	ignoreCase: true,
			       	datatype: "local",
	                multiselect:false,
	                colNames:['Name','UserId','MailId','Level','Status','Role','AddUserFlag','Add User Preference'],
					colModel:[{
						name:'Name',
						index:'Name',
						width:200, sorttype:'string',searchoptions:{sopt:['cn','eq']}					
					},{
						name:'UserId',
						index:'UserId',
						width:250, sorttype:'string',searchoptions:{sopt:['cn']}					
					},{
						name:'MailId',
						index:'Id',
						width:250					
					},{
						name:'Level',
						index:'Level',
						width:200					
					},{
						name:'Status',
						index:'Status',
						width:150, sorttype:'string',searchoptions:{sopt:['cn']}					
					},{
						name:'Role',
						index:'Role',
						width:200,sorttype:'string',searchoptions:{sopt:['cn']}	
					},
					{
						name:'AddUserFlag',
						index:'1',
						width:200
					},{
						name:'AddUserPreference',
						index:'3', width:'150',align:'center',edittype:'select',editable: true,formatter:'select',search:false,cellattr: function (cellValue, options, rowdata)
						{
							return "><select id='addPreferencesId"+cellValue+"' onchange='javascript:userAddPreferences("+cellValue+");'><option id='off' value='off'>Off</option><option id='on' value='on'>On</option ></select";
						}	
					}],
		        	   rowNum:50,
		        	   rowTotal: 200,
		        	   rowList : [10,20,30,50,70,100],
		        	   loadonce:true,
		        	   mtype: "GET", 
		        	   rownumbers: true,
		        	   rownumWidth: 40,
		        	   gridview: true,
		        	   pager: '#addPreferenceTable',
		        	   sortname: 'Name',
		        	   viewrecords: true,
		        	   sortorder: "asc",
		        	   pginput: "false",
		        	//Addpreference Activated in on
		        	   loadComplete: function () 
			   	        { 
				           var ids = $("#mnUserTable").jqGrid('getDataIDs');
				            var l = ids.length;
				            for (var i = 1; i <= l; i++) 
				            {
				            	var status=$("#mnUserTable").jqGrid ('getCell', i, 'AddUserFlag');
				            	var mailId=$("#mnUserTable").jqGrid ('getCell', i, 'MailId');
				            	if(status=="true" && mailId!=null && mailId!="null" && mailId!=='')
				            	{
				            		var value="on";
				            		$('#addPreferencesId'+i).val(value);
				            	}
				            	else 
				            	{
				            		var value="off";
				            		$('#addPreferencesId'+i).val(value);
				            	}
				            }
				        }	   
		        	 });
					$("#mnUserTable").hideCol("UserId");
					$("#mnUserTable").hideCol("MailId");
					$("#mnUserTable").hideCol("Level");
					$("#mnUserTable").hideCol("AddUserFlag");
					$("#mnUserTable").jqGrid('filterToolbar',{searchOperators : true});
					$("#mnUserTable").jqGrid('navGrid',"#addPreferenceTable",{edit:false,add:false,del:false,search:false}); 
				},
				error:function(){
				console.log("<-------Error returned add User preference-------> ");
				}
				});		
}
//add UserFlag column in MnUser for using adduser Button 
function userAddPreferences(cellValue)
{
	var addPreferenceStatus="";
	//mailId
	var mailId= $("#mnUserTable").jqGrid('getCell',cellValue, 'MailId');
	if(mailId!=null && mailId!=="null" && mailId!='')
	{
	addPreferenceUserId= $("#mnUserTable").jqGrid('getCell',cellValue, 'UserId');
	userFlag=$("#addPreferencesId"+cellValue).val();
	var url = urlForServer + "admin/checkBlockedUserForAddPreference/"+addPreferenceUserId;
	$.ajax({
		headers: { 
		  "Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn	
	  },
	  	 url: url,
         type: "POST",
         success: function(responseText)
         {
		  var data=jQuery.parseJSON(responseText);
		  addPreferenceStatus=data[0].status;
		  if(addPreferenceStatus=="A")
			{
			  if(userFlag=="on")
				{
					$('#addUserFlagModalText').text("Add user preference is activated");
					$('#addUserPreferenceConformButton').hide();
					$('#addUserPreferenceButtonCancel').hide();
					$('#addUserFlagModal').modal('show');
				}
				else
				{
					$('#addUserFlagModalText').text("Add user preference is deactivated");
					$('#addUserPreferenceConformButton').hide();
					$('#addUserPreferenceButtonCancel').hide();
					$('#addUserFlagModal').modal('show');
				}
			  var url = urlForServer + "admin/addUserFlagOnMnUser/"+addPreferenceUserId+"/"+userFlag;
				$.ajax({
					headers: { 
					  "Mn-Callers" : musicnote,
				    	"Mn-time" :musicnoteIn	
				  },
				  	 url: url,
			         type: "POST",
			         success: function(data)
			         {
			         },
			         error:function(){
							console.log("<-------add column UserFlag in MnUser error-------> ");
							}
				});
			}
		  else
		  {
			  if(userFlag=="on")
			  {
				  $('#addUserFlagModalText').text("Do you really want to allow add preference for blocked user ?");
			  }
			  else
			  {
				  $('#addUserFlagModalText').text("Do you really want to confirm add preference for blocked user ?");
			  }
			  $('#addUserPreferenceConformButton').attr('style','display:block;margin-left:420px;margin-top:6px;');
			  $('#addUserPreferenceButtonCancel').attr('style','display:block;margin-left:470px;margin-top:-38px;');
			  $('#addUserFlagModal').modal('show');
		  }
         },
         error:function(){
				console.log("<-------add column UserFlag in MnUser error-------> ");
				}
	});
	}
	else
	{
		$('#addUserFlagModalText').text("Add users preference support only for Mail Id users ");
		$('#addUserPreferenceConformButton').hide();
		$('#addUserPreferenceButtonCancel').hide();
		$('#addUserFlagModal').modal('show');
		var value="off";
		$('#addPreferencesId'+cellValue).val(value);
	}
	
}
function keypressnotallowed(event)
{
	return false;
}
//price disable for Enter Character
$(function () {
	$('#amount').keydown(function (e) {
	if (e.shiftKey || e.ctrlKey || e.altKey) {
	e.preventDefault();
	} else {
	var key = e.keyCode;
	if (!((key == 8) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) {
	e.preventDefault();
	}
	}
	});
	});
//Days disable for Enter Character
$(function () {
	$('#days').keydown(function (e) {
	if (e.shiftKey || e.ctrlKey || e.altKey) {
	e.preventDefault();
	} else {
	var key = e.keyCode;
	if (!((key == 8) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) {
	e.preventDefault();
	}
	}
	});
	});
//Default Configuration configAmount disable for Enter Character
$(function () {
	$('#configAmount').keydown(function (e) {
	if (e.shiftKey || e.ctrlKey || e.altKey) {
	e.preventDefault();
	} else {
	var key = e.keyCode;
	if (!((key == 8) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) {
	e.preventDefault();
	}
	}
	});
	});
//Default Configuration configDays disable for Enter Character
$(function () {
	$('#configDays').keydown(function (e) {
	if (e.shiftKey || e.ctrlKey || e.altKey) {
	e.preventDefault();
	} else {
	var key = e.keyCode;
	if (!((key == 8) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) {
	e.preventDefault();
	}
	}
	});
	});
//Default Configuration configAmount1 disable for Enter Character
$(function () {
	$('#configAmount1').keydown(function (e) {
	if (e.shiftKey || e.ctrlKey || e.altKey) {
	e.preventDefault();
	} else {
	var key = e.keyCode;
	if (!((key == 8) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) {
	e.preventDefault();
	}
	}
	});
	});
//Default Configuration configDays1 disable for Enter Character
$(function () {
	$('#configDays1').keydown(function (e) {
	if (e.shiftKey || e.ctrlKey || e.altKey) {
	e.preventDefault();
	} else {
	var key = e.keyCode;
	if (!((key == 8) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) {
	e.preventDefault();
	}
	}
	});
	});
//Default Configuration configAmount2 disable for Enter Character
$(function () {
	$('#configAmount2').keydown(function (e) {
	if (e.shiftKey || e.ctrlKey || e.altKey) {
	e.preventDefault();
	} else {
	var key = e.keyCode;
	if (!((key == 8) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) {
	e.preventDefault();
	}
	}
	});
	});
//Default Configuration configDays2 disable for Enter Character
$(function () {
	$('#configDays2').keydown(function (e) {
	if (e.shiftKey || e.ctrlKey || e.altKey) {
	e.preventDefault();
	} else {
	var key = e.keyCode;
	if (!((key == 8) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) {
	e.preventDefault();
	}
	}
	});
	});
//Default Configuration configAmount3 disable for Enter Character
$(function () {
	$('#configAmount3').keydown(function (e) {
	if (e.shiftKey || e.ctrlKey || e.altKey) {
	e.preventDefault();
	} else {
	var key = e.keyCode;
	if (!((key == 8) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) {
	e.preventDefault();
	}
	}
	});
	});
//Default Configuration configDays disable for Enter Character
$(function () {
	$('#configDays3').keydown(function (e) {
	if (e.shiftKey || e.ctrlKey || e.altKey) {
	e.preventDefault();
	} else {
	var key = e.keyCode;
	if (!((key == 8) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) {
	e.preventDefault();
	}
	}
	});
	});
//Default Configuration configAmount4 disable for Enter Character
$(function () {
	$('#configAmount4').keydown(function (e) {
	if (e.shiftKey || e.ctrlKey || e.altKey) {
	e.preventDefault();
	} else {
	var key = e.keyCode;
	if (!((key == 8) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) {
	e.preventDefault();
	}
	}
	});
	});
//Default Configuration configDays4 disable for Enter Character
$(function () {
	$('#configDays4').keydown(function (e) {
	if (e.shiftKey || e.ctrlKey || e.altKey) {
	e.preventDefault();
	} else {
	var key = e.keyCode;
	if (!((key == 8) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) {
	e.preventDefault();
	}
	}
	});
	});

	
	
function blog()
{
	$("#blogDescriptionMsgs").attr('style','display:none;');
	if($('#blogTitle').val()!="" && $('.summernote').code()!="")
	{
		$("#blogDescriptionMsgs").attr('style','display:none;');
		var blogTitle=$("#blogTitle").val();
		var blogDescription=$(".summernote").code();
		blogDescription = blogDescription.split("&lt;").join("$-$").split("&gt;").join("$*$").split("&").join("$@$")
										.split("+").join("$!$").split("%").join("$^$").split("~").join("$@#$");
		var data='"Title"~~'+blogTitle+'~~"adminId"~~'+userId+'~~"blogDescription"~~'+blogDescription;
		var params = data;
		var url=urlForServer +"admin/insertBlogDetails";
		$.ajax({
			 headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn	
		},
		  	 url: url,
		  	 data:params,
	         type: "POST",
	         success: function(responseText)
	         {
	        	 $('#addBlogmessages').text("Blog Added Successfully");
					$('#BlogSuccessModal').modal('show');
					$('#deleteBlog').attr('style','display:none');
			 		$('#updateBlog').attr('style','display:none');
			 		$('#addBlog').removeAttr('style','display:none;');
			 		fetchBlogDetails();
					setTimeout(function(){
		     			$('#BlogSuccessModal').modal('hide');	
		     		},3000);
					
					
	         },
		  error:function()
			{
				alert("error");
			}
		});
		$("#blogTitle").val("");
		$(".note-editable").empty();
	}
	else
		{
		$("#blogDescriptionMsgs").attr('style','display:block;color:red;');
		}
	
}
function cancelblog()
{
	$("#blogDescriptionMsgs").attr('style','display:none;');
	$("#blogTitle").val("");
	$(".note-editable").empty();
	}

function fetchBlogDetails()
{
$("#blogdetails").empty();
		var url=urlForServer +'admin/getBlogDetails';
		var musicnoteAn=$.base64.encode(url);
		var params='{"status":"A"}';
		$.ajax({ headers: { 
			"Mn-Callers" : musicnoteAn,
	    	"Mn-time" :musicnoteIn	
			},
			
			type: 'POST',
	    	url : url,
	    	cache: false,
	    	data:params,
	    	contentType: "application/json; charset=utf-8",
					success : function(response) 
					{
						if(response!="")
						{
							for(var i=response.length;i<=response.length;i++){
								if(response.indexOf("[@!$[") != -1){
			    					response = response.replace("[@!$[","");
								}
								if(response.indexOf("]@!$]") != -1){
									response = response.replace("]@!$]","");
								}
			    				var split=response.split('~~');
			    				for(var j=0;j<split.length;j++){
			    					if(split[j].indexOf("{@!${") != -1){
			    						split[j] = split[j].replace("{@!${","");
									}
									if(split[j].indexOf("}@!$}") != -1){
										split[j] = split[j].replace("}@!$}","");
									}
									var values=split[j].split('~');
									
									var blogTitle='';
									
									for(var k=0;k<values.length;k++)
									{
										if(values[k].indexOf('*:*')){
										var blogValue=values[k].split('*:*');
										
										if(blogValue[0]=='blogId '){
											blogId=blogValue[1].trim();
										}else if(blogValue[0]=='blogTitle '){
											blogTitle=blogValue[1].trim();
										}else if(blogValue[0]=='blogDate '){
											blogDate=blogValue[1].trim();
										}else if(blogValue[0]=='blogDescription '){
											blogValue[1] = blogValue[1].split("$@$amp;").join("&amp;").split("$@$nbsp;").join("&nbsp;").split("$@$").join("&").split("$!$").join("+")
																.split("$^$").join("%").split("$-$").join("&lt;").split("$*$").join("&gt;").split("$@#$").join("~");
											blogArray[blogId]=blogValue[1].trim();
										}else if(blogValue[0]=='adminId '){
											adminId=blogValue[1].trim();
										}
									}
								}
									var eventData='<div  id="'+blogId+'" class="blogUpdate" title="'+blogTitle+'" style="color:grey;font-size:20px;"><a id="'+blogId+blogId+'" class="blogTitle">'+blogTitle+'</a></div>';
									$("#blogdetails").append('<a>'+eventData+'</a>');
							}
						}
					}
				}
		});
}
